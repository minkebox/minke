const assert = require('assert');

describe('Expand', async function() {

  require('./fixture/system.fixture')();

  describe('MinkeApp.expandPort', async function() {

    require('./fixture/minkeapp.fixture')();

    it('Simple port', async function() {
      const port = await this.app.eval.expandPort({
        type: `Port`,
        name: `SELECTED_PORT`,
        port: `10`,
        protocol: `UDP`,
        nat: true
      });
      assert.equal(port.port, 10);
    });

    it('Math port', async function() {
      const port = await this.app.eval.expandPort({
        type: `Port`,
        name: `SELECTED_PORT`,
        port: `10+1`,
        protocol: `UDP`,
        nat: true
      });
      assert.equal(port.port, 11);
    });

    it('Variable port', async function() {
      this.app.eval.setAllVariables({ SELECTED_PORT: { type: 'String', value: '10' }});
      const port = await this.app.eval.expandPort({
        type: `Port`,
        name: `SELECTED_PORT`,
        port: `{{SELECTED_PORT}}`,
        protocol: `UDP`,
        nat: true
      });
      assert.equal(port.port, 10);
    });

    it('Variable math port', async function() {
      this.app.eval.setAllVariables({ SELECTED_PORT: { type: 'String', value: '10' }});
      const port = await this.app.eval.expandPort({
        type: `Port`,
        name: `SELECTED_PORT`,
        port: `{{SELECTED_PORT}}+1`,
        protocol: `UDP`,
        nat: true
      });
      assert.equal(port.port, 11);
    });

  });

});

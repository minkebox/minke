const assert = require('assert');
const { O_TRUNC } = require('constants');

describe('Variables', async function() {

  require('./fixture/system.fixture')();

  function commonTest() {

    it('setVariable simple', function() {
      this.app.eval.setVariable('key', 'value');
    });

    it('expandVariable simple', async function() {
      assert.equal(await this.app.eval.expandVariable('key'), null);
    });

    it('expandString simple', async function() {
      assert.equal(await this.app.eval.expandString('testing'), 'testing');
    });

    it('variables are not auto-created', async function() {
      this.app.eval.setVariable('key', 'value');
      assert.equal(await this.app.eval.expandVariable('key'), null);
    });

  }

  describe('MinkeSetup', function() {
    require('./fixture/minkesetup.fixture')();
    commonTest();
  });

  describe('MinkeApp', function() {
    require('./fixture/minkeapp.fixture')();

    commonTest();

    describe('Type conversions', function() {

      it('expandString: string to number', async function() {
        this.app.eval.setAllVariables({ reallyanumber: { type: 'String', value: '10' }});
        assert.strictEqual(await this.app.eval.expandVariable('reallyanumber'), 10);
      });

      it('expandString: number to number', async function() {
        this.app.eval.setAllVariables({ reallyanumber: { type: 'String', value: 10 }});
        assert.strictEqual(await this.app.eval.expandVariable('reallyanumber'), 10);
      });

      it('expandString: string to bool (true)', async function() {
        this.app.eval.setAllVariables({ reallyanumber: { type: 'String', value: 'true' }});
        assert.strictEqual(await this.app.eval.expandVariable('reallyanumber'), true);
      });

      it('expandString: string to bool (false)', async function() {
        this.app.eval.setAllVariables({ reallyanumber: { type: 'String', value: 'false' }});
        assert.strictEqual(await this.app.eval.expandVariable('reallyanumber'), false);
      });

    });

    describe('Default values', function() {

      it('expandString with simple default', async function() {
        this.app.eval.setAllVariables({ astring: { type: 'String', defaultValue: 'a string' }});
        assert.equal(await this.app.eval.expandVariable('astring'), 'a string');
      });

      it('expandString with variable default', async function() {
        this.app.eval.setAllVariables({
          anotherstring: { type: 'String', defaultValue: '{{astring}}' },
          astring: { type: 'String', defaultValue: 'a string' }
        });
        assert.equal(await this.app.eval.expandVariable('anotherstring'), 'a string');
      });

    });

    describe('Numbers', function() {

      it('expandNumber: simple', async function() {
        this.app.eval.setAllVariables({ astring: { type: 'String', value: '100' }});
        assert.equal(await this.app.eval.expandNumber('astring'), 100);
      });

      it('expandNumber: math', async function() {
        this.app.eval.setAllVariables({ astring: { type: 'String', value: '100' }});
        assert.equal(await this.app.eval.expandNumber('{{astring}}+1'), 101);
      });

    });

    describe('Patterns', function() {

      beforeEach(function() {
        this.app.eval.setAllVariables({
          testing: { type: 'Array', value: [ [ '1', '2' ], [ '3', '4' ] ] }
        });
      });

      it('default pattern', async function() {
        assert.equal(await this.app.eval.expandVariable('testing'), '1\n3');
      })

      it('simple pattern', async function() {
        this.app.eval._vars.testing.encoding = { pattern: '{{V[0]}}{{V[1]}}', join: '' };
        assert.equal(await this.app.eval.expandVariable('testing'), '1234');
      });

      it('reverse pattern', async function() {
        this.app.eval._vars.testing.encoding = { pattern: '{{V[1]}}{{V[0]}}', join: '' };
        assert.equal(await this.app.eval.expandVariable('testing'), '2143');
      });

      it('simple JS pattern with numbers', async function() {
        this.app.eval._vars.testing.encoding = { pattern: '{{V[0]+V[1]}}', join: '' };
        assert.equal(await this.app.eval.expandVariable('testing'), '37');
      });

      it('pattern with newline', async function() {
        this.app.eval._vars.testing.encoding = { pattern: '{{V[0]}}{{V[1]}}\n', join: '' };
        assert.equal(await this.app.eval.expandVariable('testing'), '12\n34\n');
      });

      it('pattern with newline join', async function() {
        this.app.eval._vars.testing.encoding = { pattern: '{{V[0]}}{{V[1]}}', join: '\n' };
        assert.equal(await this.app.eval.expandVariable('testing'), '12\n34');
      });

      it('pattern with word join', async function() {
        this.app.eval._vars.testing.encoding = { pattern: '{{V[0]}}{{V[1]}}', join: '-hello-' };
        assert.equal(await this.app.eval.expandVariable('testing'), '12-hello-34');
      });

      it('pattern with JS function', async function() {
        this.app.eval._vars.testing.encoding = { pattern: '{{parseInt(V[0]*10+V[1],16)}}', join: '\n' };
        assert.equal(await this.app.eval.expandVariable('testing'), '18\n52');
      });

    });

    describe('Nested', function() {

      it('2 levels', async function() {
        this.app.eval.setAllVariables({
          anotherstring: { type: 'String', defaultValue: '{{astring}}' },
          astring: { type: 'String', defaultValue: 'a string' }
        });
        assert.equal(await this.app.eval.expandVariable('anotherstring'), 'a string');
      });

      it('3 levels', async function() {
        this.app.eval.setAllVariables({
          yetanotherstring:{ type: 'String', defaultValue: '{{anotherstring}}' },
          anotherstring: { type: 'String', defaultValue: '{{astring}}' },
          astring: { type: 'String', defaultValue: 'a string' }
        });
        assert.equal(await this.app.eval.expandVariable('yetanotherstring'), 'a string');
      });

      it('4 levels', async function() {
        this.app.eval.setAllVariables({
            andnow: { type: 'String', defaultValue: '{{yetanotherstring}}' },
            yetanotherstring: { type: 'String', defaultValue: '{{anotherstring}}' },
            anotherstring: { type: 'String', defaultValue: '{{astring}}' },
            astring: { type: 'String', defaultValue: 'a string' }
        });
        assert.equal(await this.app.eval.expandVariable('andnow'), 'a string');
      });

    });

    describe('Quotes', function() {

      it('string', async function() {
        this.app.eval.setAllVariables({
          API_KEY: {
            type: 'String',
            value: '91f0f9da-abfa-47fe-a599-32c183345305'
          }
        });
        assert.equal(await this.app.eval.expandVariable('API_KEY'), '91f0f9da-abfa-47fe-a599-32c183345305');
      });

      it('" string', async function() {
        this.app.eval.setAllVariables({
          API_KEY: {
            type: 'String',
            value: '91f0f9da-abfa-47fe-a599-32c183345305'
          },
          QUOTE: {
            type: 'String',
            defaultValue: `{{'"' + API_KEY + '"'}}`
          }
        });
        assert.equal(await this.app.eval.expandVariable('QUOTE'), '"91f0f9da-abfa-47fe-a599-32c183345305"');
      });

      it('"x2 string', async function() {
        this.app.eval.setAllVariables({
          API_KEY: {
            type: 'String',
            value: '91f0f9da-abfa-47fe-a599-32c183345305'
          },
          QUOTE: {
            type: 'String',
            defaultValue: `"api": {{'"' + API_KEY + '"'}}`
          }
        });
        assert.equal(await this.app.eval.expandVariable('QUOTE'), '"api": "91f0f9da-abfa-47fe-a599-32c183345305"');
      });

      it('complex string', async function() {
        this.app.eval.setAllVariables({
          API_KEY: {
            type: 'String',
            value: '91f0f9da-abfa-47fe-a599-32c183345305'
          },
          __HOSTIP: {
            type: 'String',
            value: '1.2.3.4'
          },
          serverjson: {
            type: 'String',
            defaultValue: `[\n` +
              `  {\n` +
              `    "name": "Local",\n` +
              `    "url": {{'"http://' + __HOSTIP + ':8080/"'}},\n` +
              `    "apiKey": {{'"' + API_KEY + '"'}}\n` +
              `  }\n` +
              `]\n`
          }
        });
        assert.equal(await this.app.eval.expandVariable('serverjson'), `[
  {
    "name": "Local",
    "url": "http://1.2.3.4:8080/",
    "apiKey": "91f0f9da-abfa-47fe-a599-32c183345305"
  }
]
`);
      });

    });

  });


});

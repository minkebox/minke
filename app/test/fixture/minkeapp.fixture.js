const sinon = require('sinon');
const mock = require('mock-require');

module.exports = function() {

beforeEach(async function() {
  mock('fs', {
    mkdirSync: sinon.fake(),
    readdirSync: sinon.fake.returns([]),
    existsSync: sinon.fake.returns(false)
  });
  const MinkeApp = mock.reRequire('../../app/MinkeApp');
  const MinkeEval = mock.reRequire('../../app/MinkeEval');
  mock.stop('fs');

  MinkeApp._network = { network: {} };
  const app = new MinkeApp();
  await app.createFromJSON({ binds: [], vars: '', networks: { primary: { name: 'home'} , secondary: { name: 'none' } } });
  app._globalId = '1FB44E7C-7E63-4739-A1F6-569220469E8B';
  app.eval = await MinkeEval.create(app);
  this.app = app;
  this.MinkeEval = MinkeEval;
});

afterEach(function() {
  delete this.app;
  delete this.MinkeEval;
});

}

const FS = require('fs');
const Config = require('../Config');

const OVERRIDES = '/minke/image.overrides';

const Images = {

  _toverrides: Config.REGISTRY_TAG_OVERRIDES || {},
  _roverrides: Config.REGISTRY_OVERRIDES || {},

  withTag: function(name) {
    // Add tag
    if (name.indexOf(':') === -1) {
      name = `${name}:${this._toverrides[name] || Config.REGISTRY_DEFAULT_TAG}`;
    }
    // Handle registry overrides
    const sname = name.split('/', 1)[0];
    const oreg = this._roverrides[sname];
    if (oreg) {
      name = `${oreg}${name.substring(sname.length)}`;
    }
    return name;
  }

};

try {
  const overrides = JSON.parse(FS.readFileSync(OVERRIDES, { encoding: 'utf8' }));
  if (overrides.tags) {
    Images._toverrides = overrides.tags;
  }
  if (overrides.registries) {
    Images._roverrides = overrides.registries;
  }
  console.log(`Installing image overrides from ${OVERRIDES}`);
}
catch (_) {
}

Images.MINKE         = Images.withTag(`${Config.REGISTRY_HOST}/minkebox/minke`);
Images.MINKE_HELPER  = Images.withTag(`${Config.REGISTRY_HOST}/minkebox/minke-helper`);
Images.MINKE_UPDATER = Images.withTag(`${Config.REGISTRY_HOST}/minkebox/minke-updater`);

module.exports = Images;

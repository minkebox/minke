const FS = require('fs');
const Config = require('../Config');
const Images = require('./Images');
const Pull = require('./Pull');
const Applications = require('../app/Applications');
const Skeletons = require('./Skeletons');
const MinkeSetup = require('../app/MinkeSetup');
const Log = require('debug')('updater');

const TRACER_OUTPUT = '/tmp/tracer.out';
const DEFAULT_TIME = { hour: 3, minute: 0 }; // 3am

const Updater = {

  _updateTimeOfDay: Object.assign({}, DEFAULT_TIME),
  _tick: null,

  start: function() {
    if (!this._tick) {
      this._tick = setTimeout(() => this.updateAll(), this._calcTimeToNextUpdate());
    }
  },

  stop: function() {
    if (this._tick) {
      clearTimeout(this._tick);
      this._tick = null;
    }
  },

  restart: function(config) {
    this._updateTimeOfDay.hour = typeof config.hour === 'number' ? config.hour : DEFAULT_TIME.hour;
    this._updateTimeOfDay.minute = typeof config.minute === 'number' ? config.minute : DEFAULT_TIME.minute;
    this.stop();
    this.start();
  },

  //
  // Make sure every app is installed (not necessarily up-to-date)
  //
  checkAll: async function() {
    const order = Applications.getLocal();
    const images = {};
    for (let i = 0; i < order.length; i++) {
      const app = order[i];
      images[order[i]._image] = app;
      order[i]._secondary.forEach(secondary => images[secondary._image] = app);
    }
    const apps = {};
    for (let img in images) {
      const app = images[img];
      try {
        await app._docker.getImage(img).inspect();
      }
      catch (_) {
        apps[app._id] = app;
      }
    }
    for (let id in apps) {
      if (!apps[id]._mainMinkeApp) {
        try {
          await this.updateApp(apps[id]);
        }
        catch (e) {
          Log(e);
        }
      }
    }
    try {
      await Pull.updateImage(Images.MINKE_HELPER);
    }
    catch (e) {
      Log(e);
    }
  },

  updateAll: async function() {
    this.stop();
    try {
      await this._pruneNetworks();
      await this._pruneImages();
      const updated = await this._updateImages();

      let updateMinke = null;
      const order = Applications.getStartupOrder();
      for (let i = 0; i < order.length; i++) {
        const app = order[i];
        if (updated.indexOf(app) !== -1) {
          if (!app._mainMinkeApp) {
            try {
              const skel = await Skeletons.loadSkeleton(app.skeletonId(), false);
              if (skel && skel.type != 'local') {
                app.updateFromSkeleton(skel.skeleton, app.toJSON());
              }
              await app.restart('update');
            }
            catch (e) {
              console.error(e);
            }
          }
          else {
            // Leave to the end
            updateMinke = app;
          }
        }
      }

      if (this._checkNativeUpdates()) {
        MinkeSetup.getSingleton().restart('update-native');
      }
      else if (updateMinke) {
        updateMinke.restart('update');
      }
    }
    catch (e) {
      console.error(e);
    }
    this.start();
  },

  updateApp: async function(app) {
    let updated = false;
    try {
      const appimage = app._image;
      updated = await Pull.updateImage(appimage);
      if (updated) {
        await Skeletons.updateInternalSkeleton(appimage);
      }
      updated |= await Pull.updateImage(Images.MINKE_HELPER);
      await Promise.all(app._secondary.map(async secondary => {
        updated |= await Pull.updateImage(secondary._image);
      }));
    }
    catch (e) {
      console.error(e);
    }

    if (updated) {
      const skel = await Skeletons.loadSkeleton(app.skeletonId(), false);
      if (skel && skel.type != 'local') {
        app.updateFromSkeleton(skel.skeleton, app.toJSON());
      }
    }

    return updated;
  },

  updateSkeleton: async function(app) {
    const cid = app.skeletonId();
    const sid = Config.APP_MIGRATIONS[cid] || cid;
    const skel = Skeletons.loadSkeleton(sid, false);
    if (!skel) {
      return false;
    }
    // If we're migrating the app to another skeleton, we update and restart it regardless
    if (cid !== sid) {
      Log(`Migrating app from ${cid} to ${sid}`);
      await app.updateFromSkeleton(skel.skeleton, app.toJSON());
    }
    else {
      if (!skel || skel.type !== 'builtin') {
        return false;
      }
      const before = app.toJSON();
      await app.updateFromSkeleton(skel.skeleton, before);
      if (JSON.stringify(before) == JSON.stringify(app.toJSON())) {
        return false;
      }
    }
    await app.save();
    return true;
  },

  _updateImages: async function() {
    const apps = Applications.getLocal();
    const updates = [];
    const helper = await Pull.updateImage(Images.MINKE_HELPER);
    // Build a set of updated images and whether we've updated them or not
    const images = {};
    for (let i = 0; i < apps.length; i++) {
      try {
        const pimage = apps[i]._image;
        if (apps[i]._mainMinkeApp) {
          images[pimage] = await Pull.updateImage(Images.MINKE);
        }
        else {
          if (!(pimage in images)) {
            images[pimage] = await Pull.updateImage(pimage);
            await Skeletons.updateInternalSkeleton(pimage);
          }
          await Promise.all(apps[i]._secondary.map(async secondary => {
            const simage = secondary._image;
            if (!(simage in images)) {
              images[simage] = await Pull.updateImage(simage);
            }
          }));
        }
      }
      catch (e) {
        console.error(e);
      }
    }
    for (let i = 0; i < apps.length; i++) {
      if (apps[i].isRunning()) {
        if (helper && apps[i]._helperContainer) {
          // Helper updated
          updates.push(apps[i]);
        }
        else if (images[apps[i]._image]) {
          // Image updated
          updates.push(apps[i]);
        }
        else if (apps[i]._secondary.reduce((r, secondary) => {
          return r || images[secondary._image];
        }, false)) {
          // Secondary updated
          updates.push(apps[i]);
        }
      }
    }
    return updates;
  },

  _pruneImages: async function() {
    try {
      // Simple prune - anything not being used and without a tag
      await docker.pruneImages({});
      // Extra prune - anything not being used, even with a tag, but not a system component
      await docker.pruneImages({
        filters: {
          'label!': [ 'net.minkebox.system' ],
          'dangling': [ 'false' ]
        }
      });
    }
    catch (e) {
      console.error(e);
    }
  },

  _pruneNetworks: async function() {
    try {
      await docker.pruneNetworks({});
    }
    catch (e) {
      Log(e);
    }
  },

  _calcTimeToNextUpdate: function() {
    const date = new Date();
    date.setMilliseconds(0);
    date.setSeconds(0);
    date.setMinutes(this._updateTimeOfDay.minute);
    date.setHours(this._updateTimeOfDay.hour);
    const millis = date.getTime();
    const now = Date.now();
    if (millis > now) {
      return millis - now;
    }
    else {
      date.setDate(date.getDate() + 1);
      return date.getTime() - now;
    }
  },

  _checkNativeUpdates: function() {
    try {
      const info = FS.readFileSync(TRACER_OUTPUT, { encoding: 'utf8' });
      if (info.trim() != '') {
        return true;
      }
    }
    catch (_) {
    }
    return false;
  },

  updateSelfForRestart: async function() {
    // Without a system, we have to restart ourselves to apply the update. We do this by launching an
    // update helper which will wait for us to terminate and then relaunch us.
    const Network = require('../sys/Network');
    const MinkeSetup = require('../app/MinkeSetup');
    const Filesystem = require('../sys/Filesystem');
    const e = (t) => t.replace(/(\s)/g, '\\ ');
    const img = Images.MINKE_UPDATER;
    await Pull.updateImage(img);
    const maps = Filesystem.getNativeMappings();
    const vols = Object.keys(maps).map(dest => `--mount type=bind,source=${e(maps[dest].src)},target=${e(dest)},bind-propagation=${maps[dest].propagation}`).join(' ');
    const net = await Network.getHomeNetwork();
    const info = await MinkeSetup._container.inspect();
    const cmdline = `-d --name ${e(info.Name || 'minke')} --privileged -e TZ=${MinkeSetup.getTimezone()} -e DEBUG=${DEBUG} --network=${e(net.id)} --ip=${MinkeSetup.getIPAddress()} ${vols} ${info.Config.Image}`;
    const id = MinkeSetup._container.id.substring(0, 12);
    return new Promise(resolve => {
      docker.run(
        img,
        [ '/bin/sh', '-c', '/startup.sh' ],
        process.out,
        {
          Env: [ `ID=${id}`, `CMD=${cmdline}` ],
          HostConfig: {
            AutoRemove: true,
            Binds: [
              '/var/run/docker.sock:/var/run/docker.sock'
            ]
          }
        },
        () => {}
      ).on('start', resolve);
    });
  }

}

module.exports = Updater;

const Path = require('path');
const UUID = require('uuid/v4');
const Log = require('debug')('app');
const Config = require('../Config');
const WebProxy = require('./WebProxy');
const DNS = require('../dns/DNS');
const DynamicDNS = require('../dns/DynamicDNS');
const MulticastDNS = require('../dns/MulticastDNS');
const Network = require('../sys/Network');
const Filesystem = require('../sys/Filesystem');
const Database = require('../config/Database');
const Monitor = require('./Monitor');
const Images = require('../images/Images');
const MinkeSetup = require('./MinkeSetup');
const Applications = require('./Applications');
const Skeletons = require('../images/Skeletons');
const MinkeEval = require('./MinkeEval');

const GLOBALDOMAIN = Config.GLOBALDOMAIN;

const HELPER_STARTUP_TIMEOUT = (30 * 1000); // 30 seconds

class MinkeApp {

  constructor() {
    this._docker = docker;
  }

  async createFromJSON(app) {

    this._id = app._id;
    this._globalId = app.globalId;
    this._name = app.name;
    this._description = app.description;
    this._image = app.image;
    this._skeletonId = app.skeletonId;
    this._args = app.args;
    this._env = app.env;
    this._features = app.features,
    this._ports = app.ports;
    this._binds = app.binds;
    this._files = app.files;
    this._networks = app.networks;
    this._bootcount = app.bootcount;
    this._position = app.position || { tab: 0, widget: 0 };
    this._secondary = (app.secondary || []).map(secondary => {
      return {
        _image: secondary.image,
        _skeletonId: secondary.skeletonId,
        _args: secondary.args,
        _env: secondary.env,
        _features: secondary.features,
        _ports: secondary.ports,
        _binds: secondary.binds,
        _files: secondary.files
      };
    });
    this._cluster = {};
    this._fs = Filesystem.create(this);
    this.eval = MinkeEval.create(this, app.vars);

    const skel = Skeletons.loadSkeleton(this.skeletonId(), false);
    if (skel) {
      this._skeleton = skel.skeleton;
      this._monitor = skel.skeleton.monitor || {};
      this._delay = skel.skeleton.delay || 0;
      this._tags = (skel.skeleton.tags || []).concat([ 'All' ]);
    }
    else {
      this._skeleton = null;
      this._monitor = {};
      this._delay = 0;
      this._tags = [ 'All' ];
    }

    this._setStatus('stopped');
  }

  toJSON() {
    return {
      _id: this._id,
      globalId: this._globalId,
      name: this._name,
      description: this._description,
      skeletonId: this._skeletonId,
      image: this._image,
      args: this._args,
      env: this._env,
      features: this._features,
      ports: this._ports,
      binds: this._binds,
      files: this._files,
      // Cannot store raw key in DB due to '.' in keynames
      vars: this.eval.serialize(),
      networks: this._networks,
      bootcount: this._bootcount,
      position: this._position,
      secondary: this._secondary.map(secondary => {
        return {
          image: secondary._image,
          skeletonId: secondary._skeletonId,
          args: secondary._args,
          env: secondary._env,
          features: secondary._features,
          ports: secondary._ports,
          binds: secondary._binds,
          files: secondary._files,
        };
      })
    }
  }

  async createFromSkeleton(skel) {
    this._name = Applications.getUniqueName(skel.name);
    this._id = Database.newAppId();
    this._image = Images.withTag(skel.image),
    this._globalId = UUID().toLowerCase();
    this._bootcount = 0;
    this._position = { tab: 0, widget: 0 };
    this._cluster = {};
    this._fs = Filesystem.create(this);
    this.eval = MinkeEval.create(this);

    await this.updateFromSkeleton(skel, {});

    this._setStatus('stopped');
  }

  async _updateVariables(skeleton, cvars) {
    const vars = {};
    for (let i = 0; i < skeleton.actions.length; i++) {
      const action = skeleton.actions[i];
      const ovalue = cvars[action.name] && cvars[action.name].value;
      switch (action.type) {
        case 'EditEnvironment':
          vars[action.name] = {
            type: 'String',
            value: ovalue || await this.eval.expandString(action.initValue),
            defaultValue: action.defaultValue
          };
          break;
        case 'SetEnvironment':
          vars[action.name] = {
            type: 'String',
            value: ovalue || await this.eval.expandString(action.initValue) || await this.eval.expandString(action.value),
          };
          break;
        case 'EditEnvironmentAsCheckbox':
          vars[action.name] = {
            type: 'Bool',
            value: ovalue || await this.eval.expandBool(action.initValue),
            defaultValue: action.defaultValue
          };
          break;
        case 'EditEnvironmentAsTable':
        case 'SelectWebsites':
        case 'EditFileAsTable':
          vars[action.name] = {
            type: 'Array',
            value: ovalue || (action.initValue && JSON.parse(await this.eval.expandString(action.initValue)))
          };
          if (action.pattern) {
            vars[action.name].encoding = {
              pattern: action.pattern,
              join: 'join' in action ? action.join : '\n'
            };
          }
          break;
        case 'SelectBackups':
          vars[action.name] = {
            type: 'BackupSet',
            value: ovalue || []
          };
          break;
        case 'SelectDirectory':
        {
          vars[action.name] = {
            type: 'Path',
            value: ovalue || await this.eval.expandPath(action.initValue)
          };
          break;
        }
        case 'SelectShares':
        {
          vars[action.name] = {
            type: 'PathSet',
            value: ovalue || []
          };
          break;
        }
        case 'EditFile':
          vars[action.name] = {
            type: 'String',
            value: ovalue || await this.eval.expandString(action.initValue), // ovalue may be valid in some cases (restart)
            persist: false // Don't persist value in DB
          };
          break;
        case 'ShowFile':
        case 'DownloadFile':
          vars[action.name] = {
            type: 'String',
            value: undefined,
            persist: false // Don't persist value in DB
          };
          break;
        case 'ShowFileAsTable':
          vars[action.name] = {
            type: 'Array',
            value: undefined,
            persist: false // Don't persist value in DB
          };
          break;
        default:
          break;
      }
    }
    if (skeleton.constants) {
      for (let i = 0; i < skeleton.constants.length; i++) {
        const constant = skeleton.constants[i];
        vars[constant.name] = {
          type: `String`,
          value: null, // No value, which forces defaultValue to be expanded when used
          defaultValue: constant.value
        };
      }
    }
    this.eval.setAllVariables(vars);
  }

  async updateFromSkeleton(skel, defs) {

    this._skeleton = skel;
    this._skeletonId = skel.uuid;

    await this._updateVariables(skel, this.eval.allVariables());

    this._description = skel.description;

    this._networks = {
      primary: { name: 'home' },
      secondary: { name: 'none' }
    };
    skel.properties.forEach(prop => {
      if (prop.type === 'Network') {
        const pvalue = prop.value || prop.defaultValue;
        const dvalue = defs.networks && defs.networks[prop.name];
        if (pvalue === '__create') {
          this._networks[prop.name].name = this._id;
        }
        else if (dvalue) {
          // Allows template to switch of default from home to host
          if ((dvalue.name === 'host' && pvalue === 'home') || (dvalue.name === 'home' && pvalue === 'host')) {
            this._networks[prop.name] = { name: pvalue };
          }
          else {
            this._networks[prop.name] = dvalue;
          }
        }
        else if (pvalue) {
          this._networks[prop.name].name = pvalue;
        }
        if (prop.bandwidth) {
          this._networks[prop.name].bandwidth = prop.bandwidth;
        }
        if (prop.create) {
          this._networks[prop.name].canCreate = true;
        }
      }
    });
    // Any created network must be secondary
    if (this._networks.primary.name === this._id) {
      this._networks.primary = this._networks.secondary;
      this._networks.secondary = { name: this._id };
    }
    // If we only have one network, must be primary
    if (this._networks.primary.name === 'none') {
      this._networks.primary = this._networks.secondary;
      this._networks.secondary = { name: 'none' };
    }

    await this._parseProperties(this, '', skel.properties, defs.binds || []);
    if (skel.secondary) {
      this._secondary = await Promise.all(skel.secondary.map(async (secondary, idx) => {
        const secondaryApp = {
          _image: Images.withTag(secondary.image),
          _delay: secondary.delay || 0
        };
        await this._parseProperties(secondaryApp, `${idx}`, secondary.properties, []);
        return secondaryApp;
      }));
    }
    else {
      this._secondary = [];
    }
    this._skeleton = skel;
    this._monitor = skel.monitor || {};
    this._delay = skel.delay || 0;
    this._tags = (skel.tags || []).concat([ 'All' ]);
  }

  _setStatus(status) {
    const old = this._status;
    if (old !== status) {
      this._status = status;
      Root.emit('app.status.update', { app: this, status: status, oldStatus: old });
    }
    return old;
  }

  skeletonId() {
    return this._skeletonId || this._image;
  }

  isRunning() {
    return this._status === 'running';
  }

  isStarting() {
    return this._status === 'starting' || this._status === 'downloading';
  }

  _safeName() {
    return this._name.replace(/[^a-zA-Z0-9]/g, '');
  }

  _fullSafeName() {
    const domainname = MinkeSetup.getLocalDomainName();
    return this._safeName() + (domainname ? '.' + domainname : '');
  }

  _primaryMacAddress() {
    const r = this._globalId.split('-')[4];
    return `${r[0]}a:${r[2]}${r[3]}:${r[4]}${r[5]}:${r[6]}${r[7]}:${r[8]}${r[9]}:${r[10]}${r[11]}`;
  }

  getSLAACAddress() {
    return Network.generateSLAACAddress(this._primaryMacAddress());
  }

  async _parseProperties(target, ext, properties, oldbinds) {
    target._env = {};
    target._features = {};
    target._ports = [];
    target._binds = [];
    target._files = [];
    target._args = [];
    await Promise.all(properties.map(async prop => {
      switch (prop.type) {
        case 'Environment':
        {
          target._env[prop.name] = {};
          let value;
          if ('value' in prop) {
            value = prop.value;
          }
          else if ('defaultValue' in prop) {
            value = prop.defaultValue;
          }
          if (value !== null && value !== undefined) {
            target._env[prop.name].value = this.eval.expression2JS(value);
          }
          break;
        }
        case 'Directory':
        {
          let src = null;
          if (prop.use) {
            src = Filesystem.getNativePath(this._id, 'store', `/vol/${prop.use}`);
          }
          else {
            src = Filesystem.getNativePath(this._id, prop.style, `/dir${ext}/${prop.name}`);
          }
          let shares = prop.shares || [];
          if (shares.length === 0) {
            const bind = oldbinds.find(p => p.target === prop.name);
            if (bind && bind.shares) {
              shares = bind.shares;
            }
          }
          target._binds.push({
            dir: prop.use || prop.name,
            src: src,
            target: prop.name,
            description: prop.description || prop.name,
            backup: prop.backup,
            value: prop.value || prop.defaultValue,
            shares: shares
          });
          break;
        }
        case 'File':
        {
          const targetname = Path.normalize(prop.name);
          target._files.push({
            src: Filesystem.getNativePath(this._id, prop.style, `/file${ext}/${prop.name.replace(/\//g, '_')}`),
            target: targetname,
            mode: prop.mode || 0o666,
            backup: prop.backup,
            value: prop.value || prop.defaultValue,
            readonly: prop.readonly
          });
          break;
        }
        case 'Feature':
        {
          if ('value' in prop) {
            target._features[prop.name] = prop.value;
          }
          else {
            target._features[prop.name] = true;
          }
          break;
        }
        case 'Port':
        {
          const port = {
            target: prop.name,
            port: prop.port,
            protocol: prop.protocol,
          };
          [ 'web', 'dns', 'nat', 'mdns' ].forEach(type => prop[type] && (port[type] = prop[type]));
          target._ports.push(port);
          break;
        }
        case 'Arguments':
          target._args = prop.value;
          break;
        default:
          break;
      }
    }));
  }

  async start(inherit) {
    try {
      this._setStatus('starting');

      if (this._willCreateNetwork()) {
        this._createdNetwork = true;
        Root.emit('net.create', { app: this });
      }

      this._bootcount++;

      inherit = inherit || {};

      const config = {
        name: `${this._safeName()}__${this._id}`,
        Hostname: this._safeName(),
        Image: this._image,
        HostConfig: {
          Mounts: [],
          Devices: [],
          CapAdd: [],
          CapDrop: [],
          LogConfig: {
            Type: 'json-file',
            Config: {
              'max-file': '1',
              'max-size': '100k'
            }
          },
          Sysctls: {}
        }
      };

      const configEnv = [];

      // Create network environment
      const netEnv = {};
      const pNetwork = this._networks.primary.name;
      // Remove secondary network during app creation. Some apps might have to option of using one or two networks.
      const sNetwork = this._networks.secondary.name !== pNetwork ? this._networks.secondary.name : 'none';
      if (pNetwork !== 'none') {
        switch (pNetwork) {
          case 'home':
            netEnv.DHCP_INTERFACE = 0;
            netEnv.NAT_INTERFACE = 0;
            netEnv.DEFAULT_INTERFACE = 0;
            break;
          case 'host':
            netEnv.DEFAULT_INTERFACE = 0;
            break;
          default:
            netEnv.NAT_INTERFACE = 0;
            netEnv.INTERNAL_INTERFACE = 0;
            netEnv.DEFAULT_INTERFACE = 0;
            break;
        }
        if (sNetwork !== 'none') {
          switch (sNetwork) {
            case 'home':
              netEnv.SECONDARY_INTERFACE = 1;
              netEnv.DHCP_INTERFACE = 1;
              if (!('NAT_INTERFACE' in netEnv)) {
                netEnv.NAT_INTERFACE = 1;
              }
              break;
            case 'dns':
              netEnv.SECONDARY_INTERFACE = 1;
              netEnv.DNS_INTERFACE = 1;
              break;
            default:
              netEnv.SECONDARY_INTERFACE = 1;
              netEnv.INTERNAL_INTERFACE = 1;
              break;
          }
        }
      }
      for (let eth in netEnv) {
        configEnv.push(`__${eth}=eth${netEnv[eth]}`);
      }

      switch (pNetwork) {
        case 'none':
          config.HostConfig.NetworkMode = 'none';
          break;
        case 'home':
        {
          const homenet = await Network.getHomeNetwork();
          config.HostConfig.NetworkMode = homenet.id;
          config.MacAddress = this._primaryMacAddress();
          configEnv.push(`__DNSSERVER=${MinkeApp._network.network.ip_address}`);
          configEnv.push(`__GATEWAY=${MinkeApp._network.network.gateway_ip}`);
          configEnv.push(`__HOSTIP=${MinkeApp._network.network.ip_address}`);
          configEnv.push(`__DOMAINNAME=${MinkeSetup.getLocalDomainName()}`);
          config.HostConfig.Dns = [ MinkeApp._network.network.ip_address ];
          config.HostConfig.DnsSearch = [ 'local' ];
          config.HostConfig.DnsOptions = [ 'ndots:1', 'timeout:2', 'attempts:1' ];
          if (MinkeSetup.isIP6Enabled()) {
            config.HostConfig.Sysctls["net.ipv6.conf.all.disable_ipv6"] = "0";
          }
          break;
        }
        case 'host':
        {
          config.HostConfig.NetworkMode = `container:${MinkeSetup._container.id}`;
          config.Hostname = null;
          this._homeIP = MinkeApp._network.network.ip_address;
          this._defaultIP = this._homeIP;
          configEnv.push(`__DNSSERVER=${this._homeIP}`);
          configEnv.push(`__GATEWAY=${MinkeApp._network.network.gateway_ip}`);
          configEnv.push(`__HOSTIP=${this._homeIP}`);
          configEnv.push(`__DOMAINNAME=${MinkeSetup.getLocalDomainName()}`);
          break;
        }
        default:
        {
          const vpnapp = Applications.getById(pNetwork);
          if (!vpnapp) {
            // VPN app isn't running - we can't startup.
            throw new Error('Cannot start application using network which isnt active');
          }
          const vpn = await Network.getPrivateNetwork(pNetwork);
          config.HostConfig.NetworkMode = vpn.id;
          configEnv.push(`__DNSSERVER=${vpnapp._secondaryIP}`);
          configEnv.push(`__GATEWAY=${vpnapp._secondaryIP}`);
          if (sNetwork === 'home') {
            configEnv.push(`__HOSTIP=${MinkeApp._network.network.ip_address}`);
            configEnv.push(`__DOMAINNAME=${MinkeSetup.getLocalDomainName()}`);
            configEnv.push(`__DHCP_INTERFACE_MAC=${this._primaryMacAddress()}`);
          }
          config.HostConfig.Dns = [ vpnapp._secondaryIP ];
          config.HostConfig.DnsSearch = [ 'local' ];
          config.HostConfig.DnsOptions = [ 'ndots:1', 'timeout:2', 'attempts:1' ];
          break;
        }
      }

      configEnv.push(`__GLOBALID=${this._globalId}`);

      if (this._features.tuntap) {
        config.HostConfig.Devices.push({
          PathOnHost: '/dev/net/tun',
          PathInContainer: '/dev/net/tun',
          CgroupPermissions: 'rwm'
        });
      }

      if (this._features.privileged) {
        config.HostConfig.Privileged = true;
      }

      // Add supported capabilities
      [ '+SYS_MODULE','+SYS_RAWIO','+SYS_PACCT','+SYS_ADMIN','+SYS_NICE','+SYS_RESOURCE','+SYS_TIME','+SYS_TTY_CONFIG',
        '+AUDIT_CONTROL','+MAC_ADMIN','+MAC_OVERRIDE','+NET_ADMIN','+SYSLOG','+DAC_READ_SEARCH','+LINUX_IMMUTABLE',
        '+NET_BROADCAST','+IPC_LOCK','+IPC_OWNER','+SYS_PTRACE','+SYS_BOOT','+LEASE','+WAKE_ALARM','+BLOCK_SUSPEND' ].forEach(cap => {
        if (this._features[cap]) {
          config.HostConfig.CapAdd.push(cap.substring(1));
        }
      });

      [ '-SETPCAP','-MKNOD','-AUDIT_WRITE','-CHOWN','-NET_RAW','-DAC_OVERRIDE','-FOWNER','-FSETID','-KILL','-SETGID',
        '-SETUID','-NET_BIND_SERVICE','-SYS_CHROOT','-SETFCAP' ].forEach(cap => {
        if (this._features[cap]) {
          config.HostConfig.CapDrop.push(cap.substring(1));
        }
      });

      const extraHosts = [];
      function extractHost(cf) {
        const img = cf._image.split(/[/:]/);
        extraHosts.push(`${img[img.length - 2]}:127.0.0.1`);
      }
      this._secondary.forEach(extractHost);
      config.HostConfig.ExtraHosts = extraHosts;

      if (pNetwork !== 'host') {

        const helperConfig = {
          name: `helper-${this._safeName()}__${this._id}`,
          Hostname: config.Hostname,
          Image: Images.MINKE_HELPER,
          HostConfig: {
            NetworkMode: config.HostConfig.NetworkMode,
            CapAdd: [ 'NET_ADMIN' ],
            ExtraHosts: config.HostConfig.ExtraHosts,
            Dns: config.HostConfig.Dns,
            DnsSearch: config.HostConfig.DnsSearch,
            DnsOptions: config.HostConfig.DnsOptions,
            Sysctls: {}
          },
          MacAddress: config.MacAddress,
          Env: [].concat(configEnv) // Helper doesn't get any of the application environment
        };
        helperConfig.Env.push(`__MINKENAME=${MinkeSetup.safeName()}`);

        if (pNetwork === 'home' || sNetwork === 'home') {
          const ip6 = this.getSLAACAddress();
          if (ip6) {
            helperConfig.Env.push(`__HOSTIP6=${ip6}`);
          }
        }

        if (this._willCreateNetwork()) {
          helperConfig.HostConfig.Sysctls["net.ipv4.ip_forward"] = "1";
        }

        // Network shaping
        // Note. We store values in Mb/s, but the wondershaper in the helper takes kbps.
        if (this._networks.primary.bandwidth) {
          helperConfig.Env.push(`__DEFAULT_INTERFACE_BANDWIDTH=${1024 * await this.eval.expandNumber(this._networks.primary.bandwidth)}`);
        }
        if (this._networks.secondary.bandwidth) {
          helperConfig.Env.push(`__SECONDARY_INTERFACE_BANDWIDTH=${1024 * await this.eval.expandNumber(this._networks.secondary.bandwidth)}`);
        }

        this._ddns = this._features.ddns || false;
        const nat = [];
        for (let p = 0; p < this._ports.length; p++) {
          const port = await this.eval.expandPort(this._ports[p]);
          if (port.nat) {
            nat.push(`${port.port}:${port.protocol}`);
          }
        }
        if (nat.length) {
          helperConfig.Env.push(`ENABLE_NAT=${nat.join(' ')}`);
          this._ddns = true;

          // If we're opening the NAT, and we're not on the home network, we need to find
          // the actual remote endpoint so we can create global network addresses.
          if ((netEnv.NAT_INTERFACE === 0 && pNetwork != 'home') ||
              (netEnv.NAT_INTERFACE === 1 && sNetwork != 'home')) {
            helperConfig.Env.push(`FETCH_REMOTE_IP=true`);
          }
        }

        if (inherit.helperContainer) {
          this._helperContainer = inherit.helperContainer;
        }
        else {
          try {
            this._helperContainer = await this._runConfig(helperConfig);
          }
          catch (e) {
            Log('Error starting helper');
            Log(e);
            throw e;
          }

          // Attach new helper to secondary network if necessary
          if (pNetwork !== 'none') {
            switch (sNetwork) {
              case 'none':
                break;
              case 'home':
              {
                try {
                  const homenet = await Network.getHomeNetwork();
                  await homenet.connect({
                    Container: this._helperContainer.id
                  });
                }
                catch (e) {
                  // Sometimes we get an error setting up the gateway, but we don't want it to set the gateway anyway so it's safe
                  // to ignore.
                  Log('Error connecting home network');
                  Log(e);
                }
                break;
              }
              case 'dns':
              {
                try {
                  const dnsnet = await Network.getDNSNetwork();
                  await dnsnet.connect({
                    Container: this._helperContainer.id
                  });
                }
                catch (e) {
                  Log('Error connecting dns network');
                  Log(e);
                }
                break;
              }
              default:
              {
                const vpn = await Network.getPrivateNetwork(sNetwork);
                try {
                  await vpn.connect({
                    Container: this._helperContainer.id
                  });
                }
                catch (e) {
                  // Sometimes we get an error setting up the gateway, but we don't want it to set the gateway anyway so it's safe
                  // to ignore.
                  Log('Error connecting private network');
                  Log(e);
                }
                break;
              }
            }
          }
        }

        // Wait while the helper configures everything.
        if (!await this._monitorHelper()) {
          // Helper failed to startup cleanly - abort
          this.stop();
          return;
        }

        if (this._homeIP) {
          const homeip6 = this.getSLAACAddress();
          Network.registerIP(this._homeIP);
          DNS.registerHost(this._safeName(), `${this._globalId}${GLOBALDOMAIN}`, this._homeIP, homeip6);
          // If we need to be accessed remotely, register with DDNS
          if (this._ddns) {
            DynamicDNS.register(this);
          }
        }
        else if (this._defaultIP) {
          DNS.registerHost(this._safeName(), null, this._defaultIP, null);
        }

        // Remove options we inherit from the helper container
        config.Hostname = null;
        config.HostConfig.ExtraHosts = null;
        config.HostConfig.Dns = null;
        config.HostConfig.DnsSearch = null;
        config.HostConfig.DnsOptions = null;
        config.HostConfig.NetworkMode = `container:${this._helperContainer.id}`;
        config.MacAddress = null;
      }

      config.Cmd = await this.eval.expandArguments(this._args);
      const mainEnv = await this.eval.expandEnvironment(this._env);
      config.Env = Object.keys(mainEnv).map(key => `${key}=${mainEnv[key].value}`).concat(configEnv);

      this._mounts = await this._fs.getAllMounts(this);
      config.HostConfig.Mounts = this._mounts;

      const ports = [];
      for (let p = 0; p < this._ports.length; p++) {
        ports.push(await this.eval.expandPort(this._ports[p]));
      }
      if (this._defaultIP) {
        const webport = ports.find(port => port.web);
        if (webport) {
          let web = webport.web;
          const urlip = `${webport.port === 443 ? 'https' : 'http'}://${this._defaultIP}${webport.port ? ':' + webport.port : ''}`;
          const url = web.url || `${webport.port === 443 ? 'https' : 'http'}://${this._homeIP}${webport.port ? ':' + webport.port : ''}`;
          switch (web.widget || 'none') {
            case 'newtab':
              if (this._homeIP) {
                this._widgetOpen = WebProxy.createNewTab(this, `/a/w${this._id}`, web.path, url);
              }
              else {
                this._widgetOpen = WebProxy.createNewTabProxy(this, `/a/w${this._id}`, web.path, urlip);
              }
              break;
            case 'inline':
              this._widgetOpen = WebProxy.createProxy(this, `/a/w${this._id}`, web.path, urlip);
              break;
            case 'config':
              this._widgetOpen = WebProxy.createUrl(`/configure/${this._id}/`);
              break;
            case 'none':
            default:
              this._widgetOpen = null;
              break;
          }
          switch (web.tab || 'none') {
            case 'newtab':
              if (this._homeIP) {
                this._tabOpen = WebProxy.createNewTab(this, `/a/t${this._id}`, web.path, url);
              }
              else {
                this._tabOpen = WebProxy.createNewTabProxy(this, `/a/t${this._id}`, web.path, urlip);
              }
              break;
            case 'inline':
              this._tabOpen = WebProxy.createProxy(this, `/a/t${this._id}`, web.path, urlip);
              break;
            case 'none':
            default:
              this._tabOpen = null;
              break;
          }

          if (this._widgetOpen && this._widgetOpen.http) {
            koaApp.use(this._widgetOpen.http);
          }
          if (this._tabOpen && this._tabOpen.http) {
            koaApp.use(this._tabOpen.http);
          }
        }
      }

      if (this._homeIP) {
        const dnsport = ports.find(port => port.dns);
        if (dnsport) {
          this._dns = DNS.addDNSServer(this, { port: dnsport.port, dnsNetwork: sNetwork === 'dns' });
        }
      }

      this._mdnsRecords = [];
      this._netRecords = [];
      if (this._homeIP) {
        await Promise.all(ports.map(async (port) => {
          if (port.mdns && port.mdns.type && port.mdns.type.split('.')[0]) {
            this._mdnsRecords.push(await MulticastDNS.addRecord({
              hostname: this._safeName(),
              domainname: 'local',
              ip: this._homeIP,
              port: port.port,
              service: port.mdns.type,
              txt: !port.mdns.txt ? [] : Object.keys(port.mdns.txt).map((key) => {
                return `${key}=${port.mdns.txt[key]}`;
              })
            }));
          }
        }));
      }

      // Setup timezone
      if (this._features.localtime) {
        config.HostConfig.Mounts.push({
          Type: 'bind',
          Source: '/usr/share/zoneinfo',
          Target: '/usr/share/zoneinfo',
          BindOptions: {
            Propagation: 'private'
          },
          ReadOnly: true
        });
        config.Env.push(`TZ=${MinkeSetup.getTimezone()}`);
      }

      // Shared memory
      if (this._features.shm) {
        config.HostConfig.ShmSize = this._features.shm * 1024 * 1024;
      }

      // All devices
      if (this._features.dev) {
        config.HostConfig.Mounts.push({
          Type: 'bind',
          Source: '/dev',
          Target: '/dev',
          BindOptions: {
            Propagation: 'private'
          }
        });
      }

      if (inherit.container) {
        this._container = inherit.container;
        if (inherit.secondary.length) {
          this._secondaryContainers = inherit.secondary;
        }
      }
      else {
        const startup = [];

        Log('primary', JSON.stringify(config, null, 2));
        startup.push({ delay: this._delay, fn: async () => this._container = await this._runConfig(config) });

        // Setup secondary containers
        if (this._secondary.length) {
          this._secondaryContainers = [];
          for (let c = 0; c < this._secondary.length; c++) {
            const secondary = this._secondary[c];
            secondary._mounts = await this._fs.getAllMounts(secondary);
            const secondaryEnv = await this.eval.expandEnvironment(secondary._env);
            const sconfig = {
              name: `${this._safeName()}__${this._id}__${c}`,
              Image: secondary._image,
              Cmd: await this.eval.expandArguments(secondary._args),
              HostConfig: {
                Mounts: secondary._mounts,
                Devices: [],
                CapAdd: [],
                CapDrop: [],
                LogConfig: config.HostConfig.LogConfig,
                NetworkMode: `container:${this._helperContainer.id}`
              },
              Env: Object.keys(secondaryEnv).map(key => `${key}=${secondaryEnv[key].value}`)
            };
            Log(`secondary${c}`, JSON.stringify(sconfig, null, 2));
            startup.push({ delay: secondary._delay, fn: async () => this._secondaryContainers[c] = await this._runConfig(sconfig) });
          }
        }

        // Start everything up in delay order
        startup.sort((a, b) => a.delay - b.delay);
        for (let i = 0; i < startup.length; i++) {
          const start = Date.now();
          try {
            await startup[i].fn();
          }
          catch (e) {
            Log(e);
          }
          if (i + 1 < startup.length) {
            const startuptime = Date.now() - start;
            const delaytime = (startup[i + 1].delay - startup[i].delay) * 1000;
            if (startuptime < delaytime) {
              await new Promise(resolve => setTimeout(resolve, delaytime - startuptime));
            }
          }
        }
      }

      if (this._monitor.cmd) {
        this._statusMonitor = Monitor.create({
          app: this,
          target: this._monitor.target,
          cmd: this._monitor.cmd,
          init: this._monitor.init
        });
      }

      this._startTime = inherit.container ? 0 : Date.now();

      this._setStatus('running');
    }
    catch (e) {
      // Startup failed for some reason, so attempt to shutdown and cleanup.
      Log(e);
      this.stop();
    }
  }

  async stop() {

    this._setStatus('stopping');

    this._statusMonitor = null;

    if (this._mdns) {
      await Promise.all(this._mdnsRecords.map(rec => MulticastDNS.removeRecord(rec)));
      await Promise.all(this._netRecords.map(rec => MulticastDNS.removeRecord(rec)));
      this._mdns = null;
      this._mdnsRecords = null;
    }

    if (this._dns) {
      DNS.removeDNSServer(this._dns);
      this._dns = null;
    }

    function removeMiddleware(m) {
      if (m.http) {
        const idx = koaApp.middleware.indexOf(m.http);
        if (idx !== -1) {
          koaApp.middleware.splice(idx, 1);
        }
      }
    }

    if (this._widgetOpen) {
      removeMiddleware(this._widgetOpen);
      this._widgetOpen = null;
    }
    if (this._tabOpen) {
      removeMiddleware(this._tabOpen);
      this._tabOpen = null;
    }
    if (this._webProxy) {
      this._webProxy.close();
      this._webProxy = null;
    }

    DNS.unregisterHost(this._safeName());
    if (this._homeIP) {
      if (this._ddns) {
        DynamicDNS.unregister(this);
      }
      Network.unregisterIP(this._homeIP);
      this._homeIP = null;
    }

    // Stop everything
    if (this._secondaryContainers) {
      await Promise.all(this._secondaryContainers.map(async c => {
        try {
          await c.stop();
        }
        catch (e) {
          Log(e);
        }
      }));
    }
    if (this._container) {
      try {
        await this._container.stop();
      }
      catch (e) {
        Log(e);
      }
    }
    if (this._helperContainer) {
      try {
        await this._helperContainer.stop();
      }
      catch (e) {
        Log(e);
      }
    }

    // Log everything
    const getLogs = async (container) => {
      try {
        const log = await container.logs({
          follow: true,
          stdout: true,
          stderr: true
        });
        let outlog = '';
        let errlog = '';
        return new Promise(resolve => {
          this._docker.modem.demuxStream(log,
            {
              write: (chunk) => {
                outlog += chunk.toString('utf8');
              }
            },
            {
              write: (chunk) => {
                errlog += chunk.toString('utf8');
              }
            }
          );
          log.on('end', () => {
            resolve({ out: outlog, err: errlog });
          });
        });
      }
      catch (_) {
        return { out: '', err: '' };
      }
    }

    if (this._container) {
      const logs = await getLogs(this._container);
      this._fs.saveLogs(logs.out, logs.err, '');
    }
    if (this._helperContainer) {
      const logs = await getLogs(this._helperContainer);
      this._fs.saveLogs(logs.out, logs.err, '_helper');
    }
    if (this._secondaryContainers) {
      await Promise.all(this._secondaryContainers.map(async (container, idx) => {
        const logs = await getLogs(container);
        this._fs.saveLogs(logs.out, logs.err, `_${idx}`);
      }));
    }

    this._unmonitorHelper();

    // Remove everything
    const removing = [];
    if (this._secondaryContainers) {
      for (let c = 0; c < this._secondaryContainers.length; c++) {
        removing.push(this._secondaryContainers[c].remove());
      }
      this._secondaryContainers = null;
    }
    if (this._container) {
      removing.push(this._container.remove());
      this._container = null;
    }
    if (this._helperContainer) {
      removing.push(this._helperContainer.remove());
      this._helperContainer = null;
    }
    await Promise.all(removing.map(rm => rm.catch(e => console.log(e)))); // Ignore exceptions

    await this._fs.unmountAll(this, this._mounts);
    for (let i = 0; i < this._secondary.length; i++) {
      await this._fs.unmountAll(this._secondary[i], this._secondary[i]._mounts);
    }

    this._setStatus('stopped');

    if (this._createdNetwork) {
      Root.emit('net.remove', { app: this });
      this._createdNetwork = false;
    }
  }

  async restart(reason) {
    if (this.isRunning()) {
      await this.stop();
    }
    await this.save();
    await this.start();
  }

  async save() {
    await Database.saveApp(this.toJSON());
  }

  getWebLink(type) {
    switch (type) {
      case 'config':
      default:
        if (this._tabOpen || this._widgetOpen) {
          return {
            url: (this._tabOpen || this._widgetOpen).url,
            target: '_blank'
          };
        }
      case 'tab':
        if (this._tabOpen) {
          return {
            url: this._tabOpen.url,
            target: this._tabOpen.target
          };
        }
        break;
      case 'widget':
        if (this._widgetOpen) {
          return {
            url: this._widgetOpen.url,
            target: this._widgetOpen.target
          };
        }
        break;
    }
    return {};
  }

  async _runConfig(config) {
    const container = await this._docker.createContainer(config);
    await container.start();
    return container;
  }

  async _monitorHelper() {
    this._helperLog = await this._helperContainer.logs({
      follow: true,
      stdout: true,
      stderr: false
    });
    return new Promise(resolve => {
      const timeout = setTimeout(() => {
        this._helperLog.destroy();
        this._helperLog = null;
        resolve(false);
      }, HELPER_STARTUP_TIMEOUT);
      this._docker.modem.demuxStream(this._helperLog, {
        write: (data) => {
          data = data.toString('utf8');
          let idx = data.indexOf('MINKE:DHCP:IP ');
          if (idx !== -1) {
            this._homeIP = data.replace(/.*MINKE:DHCP:IP (.*)\n.*/, '$1');
            this.eval.updateJSProperty('__HOMEIP', this._homeIP);
          }
          idx = data.indexOf('MINKE:DEFAULT:IP ');
          if (idx !== -1) {
            this._defaultIP = data.replace(/.*MINKE:DEFAULT:IP (.*)\n.*/, '$1');
          }
          idx = data.indexOf('MINKE:SECONDARY:IP ');
          if (idx !== -1) {
            this._secondaryIP = data.replace(/.*MINKE:SECONDARY:IP (.*)\n.*/, '$1');
          }
          idx = data.indexOf('MINKE:REMOTE:IP ');
          if (idx !== -1) {
            this._remoteIP = data.replace(/.*MINKE:REMOTE:IP (.*)\n.*/, '$1');
          }
          idx = data.indexOf('MINKE:DEFAULT:FLAGS 0x');
          if (idx !== -1) {
            let flags = parseInt(data.replace(/.*MINKE:DEFAULT:FLAGS 0x(.*)\n.*/, '$1'), 16);
            if (!this._features.promiscuous) {
              flags &= 0xfeff;
            }
            Network.updateBridge(this._defaultIP, flags);
          }
          if (data.indexOf('MINKE:UP') !== -1) {
            clearTimeout(timeout);
            resolve(true);
          }
        }
      }, null);
    });
  }

  _unmonitorHelper() {
    if (this._helperLog) {
      this._helperLog.destroy();
      this._helperLog = null;
    }
    if (this._defaultIP) {
      Network.updateBridge(this._defaultIP, 0);
    }
  }

  _willCreateNetwork() {
    return (this._networks.primary.name === this._id || this._networks.secondary.name === this._id);
  }

}

module.exports = MinkeApp;

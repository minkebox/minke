const Log = require('debug')('apps');

const Applications = {

  hosts: {},

  //
  // Generate a unique app name based on a basname.
  //
  getUniqueName: function(basename) {
    for (let i = 0; ; i++) {
      const name = basename + (i === 0 ? '' : ` ${i}`);
      for (let h in this.hosts) {
        if (!this.hosts[h].find(app => name === app._name)) {
          return name;
        }
      }
    }
  },

  //
  // Add an app to the app list.
  //
  add: function(app, host) {
    if (!app) {
      throw new Error('missing app');
    }
    host = host || 'local';
    if (!this.hosts[host]) {
      this.hosts[host] = [];
    }
    this.hosts[host].push(app);
  },

  //
  // Remove an app (if found).
  //
  remove: function(app) {
    for (let h in this.hosts) {
      const idx = this.hosts[h].indexOf(app);
      if (idx !== -1) {
        this.hosts[h].splice(idx, 1);
        return true;
      }
    }
    return false;
  },

  //
  // Get specific host apps
  //
  getForHost(host) {
    const apps = [];
    apps.push.apply(apps, this.hosts[host]);
    return apps;
  },

  //
  // Get local apps
  //
  getLocal() {
    return this.getForHost('local');
  },

  //
  // Get a list of all apps.
  //
  getAll() {
    const list = [];
    for (let h in this.hosts) {
      list.push.apply(list, this.hosts[h]);
    }
    return list;
  },

  //
  // Find app by id.
  //
  getById(id) {
    for (let h in this.hosts) {
      const app = this.hosts[h].find(a => a._id === id);
      if (app) {
        return app;
      }
    }
    return null;
  },

  addHost(host) {
    if (!this.hosts[host]) {
      this.hosts[host] = [];
      return true;
    }
    return false;
  },

  getHosts() {
    return Object.keys(this.hosts);
  },

  removeHostIfEmpty(host) {
    if (this.hosts[host] && this.hosts[host].length === 0) {
      delete this.hosts[host];
      return true;
    }
    return false;
  },

  getByContainerId(id) {
    for (let h in this.hosts) {
      const app = this.hosts[h].find(app => {
        if (app._container && app._container.id === id) {
          return true;
        }
        else if (app._helperContainer && app._helperContainer.id === id) {
          return true;
        }
        else if (app._secondaryContainers && app._secondaryContainers.find(c => c.id === id)) {
          return true;
        }
        else {
          return false;
        }
      });
      if (app) {
        return app;
      }
    }
    return null;
  },

  //
  // Calculate the order in which applications should be started to take into account
  // dependencies between them.
  //
  getStartupOrder: function() {
    const order = [];
    const list = this.getLocal();

    // First the main Minke app
    order.push(list[0]);
    list.splice(0, 1);

    // Next all the 'host' applications.
    for (let i = 0; i < list.length; ) {
      if (list[i]._networks.primary.name === 'host' && list[i]._networks.secondary.name === 'none') {
        order.push(list[i]);
        list.splice(i, 1);
      }
      else {
        i++;
      }
    }

    // Now with the base networks (or none), add applications which depend on already existing networks.
    // If apps create networks, they are added after any neworks they depend on.
    // Exclude proxy apps so we let things they will proxy start first.
    const networks = {
      none: true, host: true, home: true, dns: true
    };
    let len;
    do {
      len = list.length;
      let i = 0;
      while (i < list.length) {
        const app = list[i];
        if (!app._features.proxy &&
            (networks[app._networks.primary.name] || app._networks.primary.name === app._id) &&
            (networks[app._networks.secondary.name] || app._networks.secondary.name === app._id)) {
          networks[app._networks.primary.name] = true;
          networks[app._networks.secondary.name] = true;
          order.push(app);
          list.splice(i, 1);
        }
        else {
          i++;
        }
      }
    } while (list.length < len);
    // Now include proxy apps
    do {
      len = list.length;
      let i = 0;
      while (i < list.length) {
        const app = list[i];
        if ((networks[app._networks.primary.name] || app._networks.primary.name === app._id) &&
            (networks[app._networks.secondary.name] || app._networks.secondary.name === app._id)) {
          networks[app._networks.primary.name] = true;
          networks[app._networks.secondary.name] = true;
          order.push(app);
          list.splice(i, 1);
        }
        else {
          i++;
        }
      }
    } while (list.length < len);
    // Repeat as long as the list gets shorted. Once it stops we either have ordered everything, or somehow have applications
    // which are dependent on things that don't exist. Let's not start those.
    if (list.length) {
      Log(`Failed to order apps: ${list.map(app => app._name)}`);
    }

    return order;
  },

  //
  // Get a list of all networks defined by the apps.
  //
  getNetworks: function(includeRemote) {
    const networks = [ { _id: 'home', name: 'home' } ];
    const apps = includeRemote ? this.getAll() : this.getLocal();
    apps.forEach(app => {
      if (app._willCreateNetwork()) {
        networks.push({ _id: app._id, name: app._name });
      }
    });
    return networks;
  },

  //
  // Get a list of all websites accessible from a specific network.
  //
  getWebsites: async function(network, includeRemote) {
    const acc = [];
    const applications = includeRemote ? this.getAll() : this.getLocal();
    for (let a = 0; a < applications.length; a++) {
      const app = applications[a];
      if (app !== this && (network === app._networks.primary.name || network === app._networks.secondary.name)) {
        for (let p = 0; p < app._ports.length; p++) {
          const port = await app.eval.expandPort(app._ports[p]);
          if (port.web && !port.web.private) {
            acc.push({
              app: app,
              port: port
            });
          }
        }
      }
    }
    return acc;
  },

  //
  // Get a list of all apps and their shareables. We can exclude an app (probaby the caller).
  //
  getShareables: async function(exclude) {
    Log('getShareables:');
    async function update(app, shares) {
      for (let i = 0; i < app._binds.length; i++) {
        const bind = app._binds[i];
        Log(bind);
        if (bind.value) {
          const avalue = app.eval.getVariable(bind.value).value;
          const pattern = bind.shares[0];
          const vshares = [];
          for (let j = 0; j < avalue.length; j++) {
            vshares.push({
              name: await app.eval.expandString(pattern.name, { V: avalue[j] }),
              description: await app.eval.expandString(pattern.description, { V: avalue[j] })
            });
          }
          shares.push({
            dir: bind.dir,
            src: bind.src,
            target: bind.target,
            description: bind.description,
            backup: bind.backup,
            shares: vshares
          });
        }
        else {
          // Include bindings with shares which aren't bound to other things
          if (bind.shares && bind.shares.length && !bind.shares.find(share => !!share.src)) {
            shares.push(bind);
          }
        }
      }
    }

    const acc = [];
    const apps = this.getLocal();
    for (let i = 0; i < apps.length; i++) {
      const app = apps[i];
      if (app !== exclude) {
        const shares = [];
        await update(app, shares);
        for (let i = 0; i < app._secondary.length; i++) {
          await update(app._secondary[i], shares);
        }
        if (shares.length) {
          acc.push({
            app: app,
            shares: shares
          });
        }
      }
    }
    Log(acc.map(a => [ a.app._name, JSON.stringify(a.shares) ]));
    return acc;
  },

  //
  // Get a list of applications requesting backups.
  //
  getBackups: function() {
    const acc = [];
    this.getLocal().forEach(app => {
      let backups = false;
      function backup(src) {
        src._binds.forEach(bind => backups |= bind.backup);
        src._files.forEach(bind => backups |= bind.backup);
      }
      backup(app);
      app._secondary.forEach(secondary => backup(secondary));

      if (backups) {
        acc.push({ app: app });
      }
    });
    return acc;
  },

  //
  // Get an ordered list of all tags defined by applications.
  //
  getTags: function() {
    const tags = [];
    for (let h in this.hosts) {
      this.hosts[h].forEach(app => {
        app._tags.forEach(tag => {
          if (tag !== 'All' && tags.indexOf(tag) === -1) {
            tags.push(tag);
          }
        });
      });
    }
    tags.sort((a, b) => a.localeCompare(b));
    return [ 'All' ].concat(tags);
  }

};

module.exports = Applications;

const Log = require('debug')('executor');
const LogMonitor = Log.extend('monitor');
const Applications = require('./Applications');
const Updater = require('../images/Updater');

const CRASH_TIMEOUT = (2 * 60 * 1000); // 2 minutes

const Executor = {

  init: function() {
    this._monitor();
  },

  startup: async function(config) {

    const running = await docker.listContainers({ all: true });
    const runningNames = running.map(container => container.Names[0]);

    // Stop or inherit apps if they're still running
    const inheritables = {};
    await Promise.all(Applications.getLocal().map(async (app) => {
      const aidx = runningNames.indexOf(`/${app._safeName()}__${app._id}`);
      const hidx = runningNames.indexOf(`/helper-${app._safeName()}__${app._id}`);
      const inherit = {
        container: aidx === -1 ? null : docker.getContainer(running[aidx].Id),
        helperContainer: hidx === -1 ? null : docker.getContainer(running[hidx].Id),
        secondary: []
      };
      for (let s = 0; s < app._secondary.length; s++) {
        const sidx = runningNames.indexOf(`/${app._safeName()}__${app._id}__${s}`);
        if (sidx === -1) {
          break;
        }
        inherit.secondary.push(docker.getContainer(running[sidx].Id));
      }

      // If we want to inherit, make sure everything is still alive
      let alive = config.inherit;
      async function isAlive(container) {
        if (alive && container && !(await container.inspect()).State.Running) {
          alive = false;
        }
      }
      await isAlive(inherit.container);
      await isAlive(inherit.helperContainer);
      await Promise.all(inherit.secondary.map(async secondary => await isAlive(secondary)));

      // We need a helper unless we're using the host network
      if (app._networks.primary.name !== 'host' && !inherit.helperContainer) {
        alive = false;
      }

      // Make sure we have all the secondaries
      if (inherit.secondary.length !== app._secondary.length) {
        alive = false;
      }

      // Update the app from the current skeleton if necessary
      if (await Updater.updateSkeleton(app)) {
        alive = false;
      }

      if (alive) {
        Log(`Inheriting ${app._name}`);
        inheritables[app._id] = inherit;
      }
      else {
        if (inherit.container) {
          Log(`Stopping ${app._safeName()}`);
          try {
            await inherit.container.remove({ force: true });
          }
          catch (e) {
            console.error(e);
          }
        }
        if (inherit.helperContainer) {
          Log(`Stopping helper-${app._safeName()}`);
          try {
            await inherit.helperContainer.remove({ force: true });
          }
          catch (e) {
            console.error(e);
          }
        }
        await Promise.all(inherit.secondary.map(async (sec, idx) => {
          Log(`Stopping ${app._safeName()}__${idx}`);
          try {
            await sec.remove({ force: true });
          }
          catch (e) {
            console.error(e);
          }
        }));
      }
    }));

    // Startup the main app here so things like DNS are running for update to use.
    const MinkeSetup = require('./MinkeSetup');
    await MinkeSetup.getSingleton().start();

    // Make sure everything is installed
    await Updater.checkAll();

    // Startup apps in order
    const order = Applications.getStartupOrder();
    for (let i = 0; i < order.length; i++) {
      try {
        const app = order[i];
        if (app._status === 'stopped') {
          await app.start(inheritables[app._id]);
        }
      }
      catch (e) {
        console.error(e);
      }
    }
  },

  shutdown: async function(config) {
    const apps = Applications.getAll();
    await Promise.all(apps.map(async (app) => {
      if (app._type !== 'remote') {
        // If app is starting up, wait for it to finish ... then we can shut it down.
        // If the app is stopping, then no need to stop it again.
        while (app.isStarting()) {
          await new Promise(resolve => setTimeout(resolve, 1000));
        }
        if (app.isRunning()) {
          // If we shutdown with 'inherit' set, we leave the children running so we
          // can inherit them when on a restart.
          if (!config.inherit && !app._mainMinkeApp) {
            await app.stop();
          }
        }
      }
      await app.save();
      const MinkeSetup = require('./MinkeSetup');
      await MinkeSetup.getSingleton().stop();
    }));
  },

  _monitor: function() {
    setTimeout(async () => {
      const stream = await docker.getEvents({});
      stream.on('readable', () => {
        const lines = stream.read().toString('utf8').split('\n');
        lines.forEach((line) => {
          if (!line) {
            return;
          }
          try {
            const event = JSON.parse(line);
            switch (event.Type) {
              case 'container':
                LogMonitor('monitor:', event.Action, event.id);
                switch (event.Action) {
                  case 'health_status: unhealthy':
                  case 'die':
                  {
                    const app = Applications.getByContainerId(event.id);
                    if (app && app.isRunning()) {
                      // If the app is running we stop it (so we stop all the pieces). We will auto-restart
                      // as long as it has been running longer than CRASH_TIMEOUT.
                      LogMonitor('monitor: stopping');
                      app.stop().then(() => {
                        LogMonitor('monitor: stopped');
                        LogMonitor('monitor: runtime:', Date.now() - app._startTime);
                        if (Date.now() - app._startTime > CRASH_TIMEOUT) {
                          LogMonitor('monitor: restarting');
                          app.start();
                        }
                      });
                    }
                    break;
                  }
                  case 'create':
                  case 'start':
                  case 'stop':
                  case 'destroy':
                  default:
                    break;
                }
                break;
              case 'network':
                break;
              case 'volume':
                break;
              case 'image':
                break;
              default:
                break;
            }
          }
          catch (_) {
          }
        });
      });
    }, 0);
  }

};

module.exports = Executor;

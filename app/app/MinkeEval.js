const Crypto = require('crypto');
const JSInterpreter = require('js-interpreter');
const Log = require('debug')('vars');
const Config = require('../Config');
const UPNP = require('../sys/UPNP');
const DNS = require('../dns/DNS');

const JSINTERPRETER_STEPS = 500;
const JSINTERPRETER_EXPAND_ATTEMPTS = 10;

let MinkeApp;
let MinkeSetup;

const MinkeEval = function(app, vars) {
  if (!MinkeApp) {
    MinkeApp = require('./MinkeApp');
  }
  if (!MinkeSetup) {
    MinkeSetup = require('./MinkeSetup');
  }
  this._app = app;
  this._vars = vars || {};
  this._js = null;
}

MinkeEval.prototype = {

  expandString: async function(txt, extras) {
    if (!txt) {
      return txt;
    }
    // Simple strings
    if (typeof txt === 'string' && txt.indexOf('{{') === -1) {
      return txt;
    }
    // Convert to evaluatable form and eval
    else {
      try {
        txt = await this._eval(this.expression2JS(txt), extras);
      }
      catch (e) {
        Log(e);
      }
    }
    return txt;
  },

  expression2JS: function(str) {
    if (typeof str === 'function') {
      return `(${str.toString()})()`;
    }
    else {
      str = String(str);
    }
    const p = str.replace(/\n/g, '\\n').split(/({{|}}|"|')/);
    let js = '""';
    let expr = false;
    let q = [];
    for (let i = 0; i < p.length; i++) {
      const t = p[i];
      if (t === "'" || t === '"') {
        if (q[0] === t) {
          q.shift();
        }
        else {
          q.unshift(t);
        }
        if (expr) {
          js += t;
        }
        else if (t === "'") {
          js += `+"'"`;
        }
        else {
          js += `+'"'`;
        }
      }
      else if (q.length) {
        js += expr ? t : `+"${t}"`;
      }
      else if (t === '{{') {
        js += `+(`;
        expr = true;
      }
      else if (t === '}}') {
        js += `)`;
        expr = false;
      }
      else {
        js += expr ? t : `+"${t}"`;
      }
    }
    return js;
  },

  expandPath: async function(path) {
    const v = this._vars[path];
    if (v && v.type === 'Path') {
      return v.value;
    }
    return path;
  },

  expandPathSet: async function(path) {
    const v = this._vars[path];
    if (v && v.type === 'PathSet') {
      return v.value;
    }
    return [];
  },

  expandBackupSet: async function(path) {
    const v = this._vars[path];
    if (v && v.type === 'BackupSet') {
      return v.value;
    }
    return [];
  },

  expandPort: async function(port) {
    let web;
    if (typeof port.web === 'object' && port.web !== null) {
      web = {
        tab: port.web.tab || 'newtab',
        widget: port.web.widget || port.web.type
      };
      if (port.web.path) {
        web.path = await this.expandPath(port.web.path);
      }
      if (port.web.url) {
        web.url = await this.expandString(port.web.url);
      }
      if (port.web.private) {
        web.private = true;
      }
    }
    else {
      web = null;
    }

    let dns;
    if (typeof port.dns === 'object') {
      dns = port.dns;
    }
    else if (typeof port.dns === 'string') {
      dns = await this.expandBool(port.dns);
    }
    else {
      dns = !!port.dns ? {} : null;
    }

    const nport = {
      target: port.name || port.target,
      port: await this.expandNumber(port.port, port.defaultPort || 0),
      protocol: port.protocol,
      web: web,
      dns: dns,
      nat: await this.expandBool(port.nat || false),
      mdns: port.mdns || null
    };
    Log(port, nport);
    return nport;
  },

  expandNumber: async function(val, alt) {
    if (typeof val === 'number') {
      return val;
    }
    if (typeof val === 'string') {
      try {
        val = await this._eval(val.replace(/({{|}})/g, ''));
        if (!isNaN(val) && !isNaN(parseFloat(val))) {
          return Number(val);
        }
      }
      catch (_) {
      }
    }
    return alt;
  },

  expandBool: async function(val) {
    if (typeof val !== 'string') {
      return !!val;
    }
    try {
      val = await this._eval(val.replace(/({{|}})/g, ''));
      if (!val) {
        return false;
      }
    }
    catch (_) {
    }
    return true;
  },

  setAllVariables: function(vars) {
    this._vars = vars;
    this._js = null;
  },

  allVariables: function() {
    return this._vars;
  },

  getVariable: function(name) {
    return this._vars[name];
  },

  setVariable: function(name, value) {
    const v = this._vars[name];
    if (v) {
      switch (v.type) {
        case 'Array':
          if (JSON.stringify(v.value) !== value) {
            try {
              v.value = JSON.parse(value);
              this._js = null;
              return true;
            }
            catch (_) {
            }
          }
          break;
        case 'PathSet':
          if (JSON.stringify(v.value) !== JSON.stringify(value)) {
            v.value = value;
            this._js = null;
            return true;
          }
          break;
        default:
          if (v.value !== value) {
            v.value = value;
            this._js = null;
            return true;
          }
          break;
      }
    }
    return false;
  },

  expandVariable: async function(name, encoding) {
    const v = this._vars[name];
    if (!v) {
      return null;
    }
    let value = v.value;
    if ((value === undefined || value === null || value === '') && v.defaultValue) {
      value = await this.expandString(v.defaultValue);
    }
    switch (v.type) {
      case 'String':
      case 'Bool':
        // Reduce values to Booleans or Numbers if possible
        if (String(value).toLowerCase() === 'true') {
          value = true;
        }
        else if (String(value).toLowerCase() === 'false') {
          value = false;
        }
        else if (!isNaN(value) && !isNaN(parseFloat(value))) {
          value = Number(value);
        }
        return value;
      case 'Path':
      {
        return value === undefined || value === null ? value : String(value);
      }
      case 'Array':
      {
        const sencoding = encoding || v.encoding || { pattern: '{{V[0]}}', join: '\n' };
        const value = v.value || [];
        const nvalue = [];
        for (let r = 0; r < value.length; r++) {
          const V = [];
          for (let i = 0; i < value[r].length; i++) {
            let entry = value[r][i];
            if (String(entry).toLowerCase() === 'true') {
              entry = true;
            }
            else if (String(entry).toLowerCase() === 'false') {
              entry = false;
            }
            else if (!isNaN(entry) && !isNaN(parseFloat(entry))) {
              entry = Number(entry);
            }
            V[i] = entry;
          }
          nvalue.push(await this.expandString(sencoding.pattern, { V: V }));
        }
        return nvalue.join(sencoding.join);
      }
      case 'PathSet':
      {
        const sencoding = encoding || v.encoding || { join: '\n' };
        return v.value.join(sencoding.join);
      }
      default:
        return ''
    }
  },

  isVariableConstant: function(name) {
    const v = this._vars[name];
    if (v && v.value && v.type !== 'Array') {
      return true;
    }
    return false;
  },

  expandEnvironment: async function(env) {
    const fullEnv = {};
    // Evaluate the environment
    for (let key in env) {
      let value = null;
      if (env[key] && env[key].value) {
        value = await this._eval(env[key].value);
      }
      else if (this._vars[key]) {
        value = await this.expandVariable(key);
      }
      if (value === undefined || value === null) {
        fullEnv[key] = { value: '' };
      }
      else {
        fullEnv[key] = { value: value };
      }
    }
    Log('expandEnv', env, '->', fullEnv);
    return fullEnv;
  },

  expandArguments: async function(args) {
    const results = [];
    if (args) {
      for (let i = 0; i < args.length; i++) {
        results.push(await this.expandString(args[i]));
      }
    }
    return results;
  },

  _eval: async function(code, extras) {
    try {
      const result = await this.execJS(code, extras);
      Log('eval', code, result);
      return result;
    }
    catch (e) {
      console.error('eval fail', code, this._vars, extras, e);
      throw e;
    }
  },

  _createJS: async function() {
    // Create new interpreter
    const js = new JSInterpreter('');
    this._js = js;
    const glb = this._js.globalObject;

    const asyncWrap = (fn) => {
      return js.createAsyncFunction(async (a,b,c,d,e,f,g,h,i, callback) => {
        let result = null;
        js._inAsyncFn = true;
        try {
          result = await fn(a,b,c,d,e,f,g,h,i);
        }
        catch (_) {
        }
        js._inAsyncFn = false;
        callback(result);
      });
    };

    // Set various app values
    js.setProperty(glb, '__GLOBALNAME', `${this._app._globalId}${Config.GLOBALDOMAIN}`);
    js.setProperty(glb, '__DOMAINNAME', MinkeSetup.getLocalDomainName());
    js.setProperty(glb, '__HOSTIP', MinkeApp._network.network.ip_address);
    js.setProperty(glb, '__HOMEIP', this._app._homeIP);
    js.setProperty(glb, '__HOMEIP6', (this._app.getSLAACAddress && this._app.getSLAACAddress()) || '');
    js.setProperty(glb, '__DNSSERVER',  MinkeApp._network.network.ip_address);
    js.setProperty(glb, '__MACADDRESS', (this._app._primaryMacAddress && this._app._primaryMacAddress().toUpperCase()) || '');
    // And app functions
    js.setProperty(glb, '__RANDOMHEX', js.createNativeFunction(len => {
      return Crypto.randomBytes(len / 2).toString('hex').toUpperCase();
    }));
    js.setProperty(glb, '__RANDOMPORTS', asyncWrap(async count => {
      const minPort = 40000;
      const nrPorts = 1024;
      const active = await UPNP.getActivePorts();
      let port = Math.floor(Math.random() * nrPorts);
      for (;;) {
        let i;
        for (i = 0; i < count; i++) {
          if (active.indexOf(minPort + port + i) !== -1) {
            break;
          }
        }
        if (i === count) {
          break;
        }
        port = (port + count) % nrPorts;
      }
      return minPort + port;
    }));
    js.setProperty(glb, '__LOOKUPIP', js.createNativeFunction(host => {
      return DNS.lookupLocalnameIP(host);
    }));

    // Start by setting properties for all constant variables. These are the ones which
    // don't require the interpreter to be evaluated.
    const undef = '<<UNDEFINED>>';
    const todo = {};
    for (let name in this._vars) {
      if (this.isVariableConstant(name)) {
        this._js.setProperty(glb, name, await this.expandVariable(name));
      }
      else {
        this._js.setProperty(glb, name, undef);
        todo[name] = undef;
      }
    }
    // Now we evaluate non-constants until nothing changes
    for (let attempts = JSINTERPRETER_EXPAND_ATTEMPTS; attempts > 0 && Object.keys(todo).length; attempts--) {
      for (let name in todo) {
        const value = await this.expandVariable(name);
        if (value !== todo[name]) {
          this._js.setProperty(glb, name, value);
          todo[name] = value;
        }
        if (String(value).indexOf(undef) === -1) {
          delete todo[name];
        }
      }
    }
    if (Object.keys(todo).length) {
      Log('Variable evaluation failed for: ' + Object.keys(todo).join(' '));
    }
    return this._js;
  },

  updateJSProperty: function(name, value) {
    if (this._js) {
      this._js.setProperty(this._js.globalObject, name, this._js.nativeToPseudo(value));
    }
  },

  execJS: async function(code, extras) {
    const js = this._js || await this._createJS();
    try {
      if (extras) {
        js.setProperty(js.globalObject, '__extras', js.nativeToPseudo(extras));
        code = `with(__extras){${code}}`;
      }
      js.appendCode(code);
      let i;
      for (i = 0; i < JSINTERPRETER_STEPS && (js.step() || js._inAsyncFn); i++) {
        if (js._inAsyncFn) {
          await new Promise(done => setTimeout(done, 10));
        }
      }
      if (i >= JSINTERPRETER_STEPS) {
        throw new Error('Interpreter overrun');
      }
    }
    finally {
      if (extras) {
        delete js.globalObject.properties.__extras;
      }
    }
    return js.pseudoToNative(js.value);
  },

  serialize: function() {
    return JSON.stringify(Object.keys(this._vars).reduce((obj, key) => {
      obj[key] = Object.assign({}, this._vars[key]);
      if (this._vars[key].persist === false) {
        delete obj[key].value;
      }
      return obj;
    }, {}));
  }
};

module.exports = {
  create: function(app, json) {
    return new MinkeEval(app, JSON.parse(json || '{}'));
  }
}

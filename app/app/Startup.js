const Moment = require('moment-timezone');
const UUID = require('uuid/v4');
const Config = require('../Config');
const Database = require('../config/Database');
const MinkeApp = require('./MinkeApp');
const Filesystem = require('../sys/Filesystem');
const Network = require('../sys/Network');
const DNSNetwork = require('../dns/DNSNetwork');
const Executor = require('./Executor');
const System = require('../sys/System');
const Applications = require('./Applications');
const ClusterApplications = require('../cluster/Applications');
const MinkeSetup = require('./MinkeSetup');
const ConfigBackup = require('../config/ConfigBackup');
const Disks = require('../sys/Disks');

const Startup = {

  start: async function(config) {

    // Start DB
    await Database.init();

    // Find ourself
    const clist = await docker.listContainers({});
    for (let i = 0; i < clist.length; i++) {
      try {
        const container = docker.getContainer(clist[i].Id);
        const info = await container.inspect();
        if (info.Config.Labels['net.minkebox.system.main'] === 'true') {
          MinkeSetup._container = container;
          break;
        }
      }
      catch (_) {
      }
    }
    if (!MinkeSetup._container) {
      throw new Error('Failed to find ourself on startup');
    }

    // Setup the filesystem
    await Filesystem.init();

    // Get our IP
    MinkeApp._network = Network.getActiveInterface();

    // Startup home network early (in background)
    Network.getHomeNetwork();

    // Startup the DNS network
    await DNSNetwork.start();

    // See if we have wifi (in background)
    Network.wifiAvailable();

    // Monitor applications
    ClusterApplications.start();
    Executor.init();

    // Monitor host system
    System.start();

    // Setup at the top. We load this now rather than at the top of the file because it will attempt to load us
    // recursively (which won't work).
    Applications.add(new MinkeSetup(
      await Database.getConfig('minke'),
      {
        LOCALDOMAIN: 'home',
        IP6: false,
        NATIP6: false,
        WIFIENABLED: false,
        DNSSERVER1: Config.DEFAULT_FALLBACK_RESOLVER,
        DNSSERVER2: '',
        TIMEZONE: Moment.tz.guess(),
        ADMINMODE: 'DISABLED',
        MASTERMODE: 'DISABLED',
        GLOBALID: UUID().toLowerCase(),
        POSITION: 0,
        DARKMODEENABLED: 'auto',
        HUMAN: 'unknown'
      }, {
        HOSTNAME: 'MinkeBox',
        LOCALDOMAIN: '',
        DHCP: MinkeApp._network.dhcp,
        PORT: config.port || 80,
        IPADDRESS: MinkeApp._network.network.ip_address,
        GATEWAY: MinkeApp._network.network.gateway_ip,
        NETMASK: MinkeApp._network.netmask.mask,
        WIFINAME: '',
        WIFIPASSWORD: '',
        DNSSERVER1: '',
        DNSSERVER2: '',
        UPDATETIME: '03:00'
      }
    ));

    // Load all the apps
    const jsonapps = await Database.getApps();
    for (let i = 0; i < jsonapps.length; i++) {
      if (jsonapps[i].type !== 'remote') {
        const app = new MinkeApp();
        await app.createFromJSON(jsonapps[i]);
        Applications.add(app);
      }
    }

    // Safe to start listening - only on the home network.
    koaApp.listen({
      host: MinkeApp._network.network.ip_address,
      port: config.port || 80
    });

    // Save current config
    await ConfigBackup.save();

    // Setup Disks
    await Disks.init();

    // Start the apps up
    await Executor.startup(config);
  }
};

module.exports = Startup;

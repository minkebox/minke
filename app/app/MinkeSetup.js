const FS = require('fs');
const Config = require('../Config');
const Images = require('../images/Images');
const DNS = require('../dns/DNS');
const Network = require('../sys/Network');
const Database = require('../config/Database');
const MulticastDNS = require('../dns/MulticastDNS');
const UPNP = require('../sys/UPNP');
const Updater = require('../images/Updater');
const DynamicDNS = require('../dns/DynamicDNS');
const Human = require('../sys/Human');
const Executor = require('./Executor');
const ClusterManager = require('../cluster/Manager');

const RESTART_REASON = `${Config.ROOT}/minke-restart-reason`;
const DEFAULT_SSH_PORT = 22;

let singleton;

function MinkeSetup(savedConfig, firstUseConfig, defaultConfig) {

  singleton = this;

  // Make the main app easy to identify elsewhere
  this._mainMinkeApp = true;

  if (savedConfig) {
    this._bootcount = 1;
  }
  else {
    this._bootcount = 0;
    savedConfig = Object.assign({}, firstUseConfig);
  }

  this.eval._vars = {};
  const makeVar = (name) => {
    this.eval._vars[name] = { type: 'String', value: savedConfig[name] || defaultConfig[name] };
  }

  this._id = 'minke';
  this._docker = docker;
  this._image = 'minke';
  this._status = 'stopped';
  this._features = {};
  this._binds = [];
  this._files = [{
    src: `${Config.ROOT}/minkebox.config`,
    target: '/minkebox.config',
    data: '',
    mode: 0o600,
    backup: true
  }];
  makeVar('LOCALDOMAIN');
  makeVar('DHCP');
  makeVar('PORT');
  makeVar('IPADDRESS');
  makeVar('NETMASK');
  makeVar('GATEWAY');
  makeVar('IP6');
  makeVar('NATIP6');
  makeVar('WIFIENABLED');
  makeVar('WIFINAME');
  makeVar('WIFIPASSWORD');
  makeVar('DNSSERVER1');
  makeVar('DNSSERVER2');
  makeVar('TIMEZONE');
  makeVar('ADMINMODE');
  makeVar('MASTERMODE');
  makeVar('GLOBALID');
  makeVar('UPDATETIME');
  makeVar('HUMAN');
  makeVar('HOSTNAME');
  makeVar('POSITION');
  makeVar('DARKMODEENABLED');
  this._secondary = [];
  this._ports = [
    { port: this.eval.expandVariable('PORT'), protocol: 'TCP', mdns: { type: '_minkebox._tcp' } },
    { port: this.eval.expandVariable('PORT') + 1, protocol: 'TCP', mdns: { type: '_ssh._tcp' } }
  ];
  this._networks = {
    primary: { name: 'host' },
    secondary: { name: 'none' }
  };
  this._monitor = {};
  this._name = this.eval.expandVariable('HOSTNAME');
  this._container = MinkeSetup._container;
  this._homeIP = this.eval.expandVariable('IPADDRESS');
  this._defaultIP = this._homeIP;
  this._globalId = this.eval.expandVariable('GLOBALID');
  this._tags = [ 'All' ];
  this._position = { tab: parseInt(this.eval.expandVariable('POSITION')), widget: 0 };
}

MinkeSetup.prototype = {

  start: async function() {

    this.setTimezone();
    this.setUpdateTime();

    DNS.start({
      hostname: this._name,
      domainname: MinkeSetup.getLocalDomainName(),
      ip: this._homeIP,
      port: 53,
      resolvers: [ this.eval.expandVariable('DNSSERVER1'), this.eval.expandVariable('DNSSERVER2') ]
    });

    Human.start(this._globalId, this.eval._vars.HUMAN);

    DynamicDNS.start();

    await UPNP.start({
      uuid: this._globalId,
      hostname: this._name,
      ipaddress: this._homeIP,
      port: this.eval.expandVariable('PORT')
    });

    await MulticastDNS.start({
      ipaddress: this._homeIP
    });

    this._hostMdns = await MulticastDNS.addRecord({
      hostname: this._name,
      domainname: 'local',
      ip: this._homeIP,
      service: '_http._tcp',
      port: this.eval.expandVariable('PORT'),
      txt: []
    });

    this._minkeMdns = await MulticastDNS.addRecord({
      hostname: this._name,
      domainname: 'local',
      ip: this._homeIP,
      service: '_minkebox._tcp',
      port: this.eval.expandVariable('PORT'),
      txt: []
    });

    this._sshdMdns = await MulticastDNS.addRecord({
      hostname: this._name,
      domainname: 'local',
      ip: this._homeIP,
      service: '_ssh._tcp',
      port: DEFAULT_SSH_PORT,
      txt: []
    });

    ClusterManager.enableMaster(MinkeSetup.getMasterMode());

    this._setStatus('running');
  },

  stop: async function() {
    this._setStatus('shutting down');
    await MulticastDNS.stop();
    await UPNP.stop();
    Human.stop();
    DynamicDNS.stop();
    await DNS.stop();
  },

  updateAll: async function() {
    this._setStatus('updating');
    await Updater.updateAll();
    this._setStatus('running');
  },

  restart: async function(reason) {
    this._setStatus('restarting');

    this._bootcount = 1;
    this._homeIP = this.eval.expandVariable('IPADDRESS');

    if (this._hostMdns) {
      await MulticastDNS.removeRecord(this._hostMdns);
    }
    this._hostMdns = await MulticastDNS.addRecord({
      hostname: this._name,
      domainname: 'local',
      ip: this._homeIP,
      service: '_http._tcp',
      port: this.eval.expandVariable('PORT'),
      txt: []
    });

    await MulticastDNS.removeRecord(this._minkeMdns);
    this._minkeMdns = await MulticastDNS.addRecord({
      hostname: this._name,
      domainname: 'local',
      ip: this._homeIP,
      service: '_minkebox._tcp',
      port: this.eval.expandVariable('PORT'),
      txt: []
    });

    await MulticastDNS.removeRecord(this._sshdMdns);
    this._sshdMdns = await MulticastDNS.addRecord({
      hostname: this._name,
      domainname: 'local',
      ip: this._homeIP,
      service: '_ssh._tcp',
      port: DEFAULT_SSH_PORT,
      txt: []
    });

    UPNP.update({ hostname: this._name });

    DNS.setHostname(this._name);
    DNS.setDefaultResolver(
      this.eval.expandVariable('DNSSERVER1'),
      this.eval.expandVariable('DNSSERVER2')
    );
    DNS.setDomainName(MinkeSetup.getLocalDomainName());

    Network.setHomeNetwork({
      enable: !this.eval.expandVariable('WIFIENABLED'),
      address: this.eval.expandVariable('DHCP') ? 'dhcp' : this._homeIP,
      netmask: this.eval.expandVariable('NETMASK'),
      gateway: this.eval.expandVariable('GATEWAY')
    });
    Network.setWiredNetwork({
      enable: !this.eval.expandVariable('WIFIENABLED'),
    });
    Network.setWiFiNetwork({
      enable: this.eval.expandVariable('WIFIENABLED'),
      network: this.eval.expandVariable('WIFINAME'),
      password: this.eval.expandVariable('WIFIPASSWORD'),
      address: this.eval.expandVariable('DHCP') ? 'dhcp' : this._homeIP,
      netmask: this.eval.expandVariable('NETMASK'),
      gateway: this.eval.expandVariable('GATEWAY')
    });

    this.setTimezone();
    this.setUpdateTime();
    ClusterManager.enableMaster(MinkeSetup.getMasterMode());

    await this.save();

    Root.emit('app.status.update', { app: this, status: this._status });
    if (reason) {
      this._setStatus('restarting');
      this.systemRestart(reason);
    }
    else {
      this._setStatus('running');
    }
  },

  save: async function() {
    const config = {
      LOCALDOMAIN: null,
      IP6: null,
      NATIP6: null,
      WIFIENABLED: null,
      WIFINAME: null,
      WIFIPASSWORD: null,
      DNSSERVER1: null,
      DNSSERVER2: null,
      ADMINMODE: null,
      MASTERMODE: null,
      GLOBALID: null,
      UPDATETIME: null,
      TIMEZONE: null,
      DARKMODEENABLED: null,
      HUMAN: null
    };
    for (let key in config) {
      config[key] = this.eval.expandVariable(key);
    }
    config.HOSTNAME = this._name;
    config.POSITION = this._position.tab;
    config._id = this._id;
    await Database.saveConfig(config);
  },

  eval: {

    expandString: function(str) {
      return str;
    },

    expandVariable: function(key) {
      if (key in this._vars) {
        return this._vars[key].value;
      }
      return null;
    },

    setVariable: function(key, value) {
      if (key in this._vars) {
        this._vars[key].value = value;
        return true;
      }
      return false;
    },

    allVariables() {
      return this._vars;
    }

  },

  toJSON: function() {
    return {};
  },

  updateFromSkeleton: function() {
  },

  getWebLink: function() {
    return {};
  },

  _safeName: function() {
    return this._name;
  },

  _willCreateNetwork: function() {
    return false;
  },

  isRunning: function() {
    return true;
  },

  isStarting: function() {
    return false;
  },

  _setStatus: function(status) {
    const old = this._status;
    if (old !== status) {
      this._status = status;
      Root.emit('app.status.update', { app: this, status: status, oldStatus: old });
    }
    return old;
  },

  setTimezone: function() {
    if (DEBUG) {
      return false;
    }
    try {
      const timezone = this.eval.expandVariable('TIMEZONE');
      const oldtimezone = FS.readFileSync('/etc/timezone', { encoding: 'utf8' });
      const zonefile = `/usr/share/zoneinfo/${timezone}`;
      if (oldtimezone != timezone && FS.existsSync(zonefile)) {
        FS.copyFileSync(zonefile, '/etc/localtime');
        FS.writeFileSync('/etc/timezone', timezone);
        return true;
      }
    }
    catch (_) {
    }
    return false;
  },

  setUpdateTime: function() {
    try {
      const time = this.eval.expandVariable('UPDATETIME').split(':');
      const config = {
        hour: parseInt(time[0]),
        minute: parseInt(time[1])
      };
      if (config.hour >= 0 && config.hour <= 23 && config.minute >= 0 && config.minute <= 59) {
        Updater.restart(config);
      }
    }
    catch (_) {
    }
  },

  skeletonId: function() {
    return '00000000-0000-0000-0000-000000000000';
  },

  systemRestart: async function(reason) {
    try {
      FS.writeFileSync(RESTART_REASON, reason);
    }
    catch (_) {
    }
    switch (reason) {
      case 'restart':
      case 'update':
      case 'update-native':
        await Executor.shutdown({ inherit: true });
        if (!SYSTEM) {
          await Updater.updateSelfForRestart();
        }
        process.exit();
        break;

      case 'restore':
        process.exit();
        break;

      case 'reboot':
      case 'halt':
      case 'exit':
      default:
        await Executor.shutdown({});
        process.exit();
        break;
    }
  }

}

MinkeSetup.getSingleton = function() {
  return singleton;
}

MinkeSetup.getLocalDomainName = function() {
  return singleton.eval.expandVariable('LOCALDOMAIN');
}

MinkeSetup.getAdvancedMode = function() {
  return singleton.eval.expandVariable('ADMINMODE') === 'ENABLED';
}

MinkeSetup.getMasterMode = function() {
  return singleton.eval.expandVariable('MASTERMODE') === 'ENABLED';
}

MinkeSetup.getDarkMode = function() {
  return singleton.eval.expandVariable('DARKMODEENABLED');
}

MinkeSetup.getTimezone = function() {
  return singleton.eval.expandVariable('TIMEZONE') || 'UTC';
}

MinkeSetup.safeName = function() {
  return singleton._safeName() || 'minkebox';
}

MinkeSetup.getIPAddress = function() {
  return singleton._homeIP;
}

MinkeSetup.isIP6Enabled = function() {
  if (!Network.getSLAACAddress()) {
    return false;
  }
  else {
    return !!singleton.eval.expandVariable('IP6');
  }
}

MinkeSetup.isIP6NATEnabled = function() {
  return MinkeSetup.isIP6Enabled() && !!singleton.eval.expandVariable('NATIP6');
}

module.exports = MinkeSetup;

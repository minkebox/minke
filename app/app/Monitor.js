const TIMEOUT = 2; // 2 seconds

async function runCmd(app, container, cmd, runtime) {
  const exec = await container.exec({
    AttachStdin: false,
    AttachStdout: true,
    AttachStderr: false,
    Tty: false,
    Cmd: [ 'sh', '-c', cmd ]
  });
  const stream = await exec.start();
  let buffer = '';
  app._docker.modem.demuxStream(stream, {
    write: (data) => {
      buffer += data.toString('utf8');
    }
  }, null);
  return new Promise(resolve => {
    let timeout = setTimeout(() => {
      if (!timeout) {
        timeout = null;
        try {
          stream.destroy();
        }
        catch (_) {
        }
        resolve('');
      }
    }, (runtime || TIMEOUT) * 1000);
    stream.on('close', () => {
      if (!timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      resolve(buffer);
    });
  });
}

const Monitor = {

  create: function(args) {
    const container = args.target === 'helper' ? args.app._helperContainer : args.app._container;
    const cmd = args.cmd;
    return {
      init: args.init.replace(/{{ID}}/g, args.app._id),
      update: async (timeout) => await runCmd(args.app, container, cmd, timeout)
    }
  }

};

module.exports = Monitor;

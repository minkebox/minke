const FS = require('fs');
const Koa = require('koa');
const KoaRouter = require('koa-router');
const KoaProxies = require('koa-proxies');

const TEMPORARY_REDIRECT = 307;

function makeProxy(to) {
  const app = new Koa();
  app.use(async (ctx, next) => {
    try {
      // We transform location redirects ourselves because the proxy doesn't handle this. The proxy will
      // do redirect internally but if we let that happen then redirects from /aaa to /aaa/ aren't seen
      // by the browser which can break some apps (e.g. PiHole).
      const requestOrigin = ctx.request.origin;
      await next();
      const location = ctx.response.get('location');
      if (location && location.indexOf(to) === 0) {
        ctx.response.set('Location', `${requestOrigin}${location.substring(to.length)}`);
      }
    }
    catch (e) {
      //console.log(e);
      ctx.type = 'text/html';
      ctx.body = FS.readFileSync(`${__dirname}/../pages/html/ProxyFail.html`);
    }
  });
  app.use(KoaProxies('/', {
    //logs: true,
    target: to,
    ws: true,
    autoRewrite: false,
    events: {
      proxyReq: (proxyReq, req, res) => {
        proxyReq.removeHeader('origin');
      },
      proxyRes: (proxyRes, req, res) => {
        proxyRes.headers['X-MinkeBox-Proxy'] = 'true';
        delete proxyRes.headers['content-security-policy'];
        delete proxyRes.headers['x-frame-options'];
      }
    }
  }));
  const server = app.listen();
  return new Promise(resolve => {
    server.on('listening', () => {
      resolve({
        port: server.address().port,
        close: () => {
          server.close();
        }
      });
    });
  });
}

function Proxy(app, from, to) {
  this._router = KoaRouter({
    prefix: from
  });
  this._router.all('(.*)', async (ctx) => {
    if (!app._webProxy) {
      app._webProxy = await makeProxy(to);
    }
    ctx.redirect(`http://${ctx.request.header.host}:${app._webProxy.port}${ctx.params[0] || ''}`);
    ctx.status = TEMPORARY_REDIRECT;
  });
}

function Redirect(from, url) {
  this._router = KoaRouter({
    prefix: from
  });
  this._router.all('/', async (ctx) => {
    ctx.redirect(url);
    ctx.status = TEMPORARY_REDIRECT;
  });
}

const WebProxy = {

  createProxy: function(app, from, path, to) {
    const f = new Proxy(app, from, to);
    return {
      url: `${from}${path || ''}`.replace(/\/\//g,'/'),
      http: f._router.middleware()
    };
  },

  createNewTab: function(app, from, path, to) {
    const f = new Redirect(from, `${to}${path || ''}`);
    return {
      url: from,
      target: '_blank',
      http: f._router.middleware()
    };
  },

  createNewTabProxy: function(app, from, path, to) {
    const f = new Proxy(app, from, to);
    return {
      url: `${from}${path || ''}`.replace(/\/\//g,'/'),
      target: '_blank',
      http: f._router.middleware()
    };
  },

  createUrl: function(url) {
    return {
      url: url
    };
  }

}

module.exports = WebProxy;

const UDP = require('dgram');
const Net = require('net');
const ChildProcess = require('child_process');
const FS = require('fs');
const DnsPkt = require('dns-packet');
const Network = require('../sys/Network');
const Database = require('../config/Database');
const Native = require('../native/native');
const Log = require('debug')('dns');
const LogTiming = Log.extend('timing');
const LogError = Log.extend('error');

const ETC = (DEBUG ? '/tmp/' : '/etc/');
const HOSTNAME_FILE = `${ETC}hostname`;
const HOSTNAME = '/bin/hostname';
const DNS_NETWORK = 'dns0';
const GLOBAL1 = { _name: 'global1', _position: { tab: Number.MAX_SAFE_INTEGER - 1 }, isRunning: () => true };
const GLOBAL2 = { _name: 'global2', _position: { tab: Number.MAX_SAFE_INTEGER }, isRunning: () => true };
const CLUSTER = { _name: 'cluster', _position: { tab: -5 }, isRunning: () => true };
const MAX_SAMPLES = 128;
const STDEV_QUERY = 4;
const STDEV_FAIL = 2;

const NOERROR = 0;
const SERVFAIL = 2;
const NOTFOUND = 3;

const PARALLEL_QUERY = 0;
const ENABLE_NEGATIVE_CACHING = 0; // Buggy - disabled

const PrivateDNS = require('./PrivateDNS');
const CachingDNS = require('./CachingDNS');
const MulticastDNS = require('./MulticastDNS');
const GlobalDNS = require('./GlobalDNS');


const LocalDNSSingleton = {

  _qHighWater: 50,
  _qLowWater: 20,
  _forwardCache: {},
  _backwardCache: {},
  _macCache: {},
  _pending: {},

  start: async function() {
    const home = await Network.getHomeNetwork();
    const homecidr = home.info.IPAM.Config[0].Subnet.split('/');
    let homebits = parseInt(homecidr[1]);

    this._network = await Network.getDNSNetwork();
    const cidr = this._network.info.IPAM.Config[0].Subnet.split('/');
    const base = cidr[0].split('.');
    const basebits = parseInt(cidr[1]);

    // Ideally we need one more bit in the DNS network compared to HOME. If we don't get that we might get some
    // address duplications.
    if (basebits >= homebits) {
      homebits = basebits + 1;
    }

    // Simple mapping using mask
    this._mask = [ 0, 0, 0, 0 ];
    for (let i = homebits; i < 32; i++) {
      this._mask[Math.floor(i / 8)] |= 128 >> (i % 8);
    }
    this._base = [ parseInt(base[0]), parseInt(base[1]), parseInt(base[2]), parseInt(base[3]) ];
    this._base[Math.floor(basebits / 8)] |= 128 >> (basebits % 8);

    this._broadcast = `${base[0]}.${base[1]}.255.255`;

    // Manage how ARP-replies are sent
    // http://kb.linuxvirtualserver.org/wiki/Using_arp_announce/arp_ignore_to_disable_ARP
    FS.writeFileSync('/proc/sys/net/ipv4/conf/all/arp_announce', '1', { encoding: 'utf8' });
    FS.writeFileSync('/proc/sys/net/ipv4/conf/all/arp_ignore', '2', { encoding: 'utf8' });

    // Remove my address from main interface, we don't use it.
    // And although we don't use it, the ip address still keeps turning up on the network for unknown reasons unless
    // we explicitly remove it.
    ChildProcess.spawnSync('/sbin/ip', [ 'addr', 'del', `${this._network.info.IPAM.Config[0].Gateway}/16`, 'dev', DNS_NETWORK ]);
  },

  stop: async function() {
    const state = {
      _id: 'localdns',
      dnsBase: JSON.stringify(this._base),
      map: [],
    };
    await Database.saveConfig(state);
  },

  _allocAddress: function(address) {
    // Find an active entry
    const entry = this._forwardCache[address];
    if (entry) {
      return entry;
    }

    const saddress = address.split('.');
    const daddress = `${this._base[0] | (this._mask[0] & saddress[0])}.${this._base[1] | (this._mask[1] & saddress[1])}.${this._base[2] | (this._mask[2] & saddress[2])}.${this._base[3] | (this._mask[3] & saddress[3])}`;
    const maddress = this._allocMacAddress(address);

    // If we have more source addresses than dns address, we may get duplicates. So look for a duplicate entry and remove it
    // so we can reuse it.
    const oldEntry = this._backwardCache[daddress];
    if (oldEntry) {
      if (oldEntry.address === address) {
        LogError('_allocAddress: forward and backward cache inconsistency');
      }
      delete this._forwardCache[oldEntry.address];
      delete this._backwardCache[daddress];
      delete this._macCache[oldEntry.mac];
      this._destroyInterface(oldEntry);
      if (oldEntry.socket) {
        oldEntry.socket.close();
      }
    }

    // If the mac address is in use (on another entry), remove that entry
    const macEntry = this._macCache[maddress];
    if (macEntry) {
      delete this._forwardCache[macEntry.address];
      delete this._backwardCache[macEntry.dnsAddress];
      delete this._macCache[maddress];
      this._destroyInterface(macEntry);
      if (macEntry.socket) {
        macEntry.socket.close();
      }
    }

    const newEntry = {
      socket: null,
      address: address,
      dnsAddress: daddress,
      mac: maddress,
      iface: maddress.replace(/:/g,'')
    };
    this._forwardCache[address] = newEntry;
    this._backwardCache[daddress] = newEntry;
    this._macCache[maddress] = newEntry;

    this._createInterface(newEntry);

    return newEntry;
  },

  _allocMacAddress: function(address) {
    const sa = address.split('.');
    function f(p) {
      return ('0'+parseInt(sa[p]).toString(16)).slice(-2);
    }
    return `da:00:${f(0)}:${f(1)}:${f(2)}:${f(3)}`;
  },

  // We give each client on the DNS network a unique IP and mac address (the mac is derived from the IP). It's not enough to just
  // give clients IP addesses as some DNS applications care about the mac addresses being unique too. We do this by creating
  // macvlan links connected to the dns bridge. We bind the socket we use to the specific endpoint so the requests go out with
  // the correct IP and mac address.
  _createInterface: function(entry) {
    const iface = entry.iface;
    ChildProcess.spawnSync('/sbin/ip', [ 'link', 'add', `d${iface}`, 'link', DNS_NETWORK, 'type', 'macvlan', 'mode', 'bridge' ]);
    ChildProcess.spawnSync('/sbin/ip', [ 'link', 'set', `d${iface}`, 'up', 'address', entry.mac ]);
    ChildProcess.spawnSync('/sbin/ip', [ 'addr', 'add', `${entry.dnsAddress}/16`, 'broadcast', this._broadcast, 'dev', `d${iface}` ]);
  },

  _destroyInterface: function(entry) {
    ChildProcess.spawnSync('/sbin/ip', [ 'link', 'del', 'dev', `d${entry.iface}` ]);
  },

  getSocket: async function(rinfo, tinfo) {
    const start = Date.now();
    if (rinfo.tcp) {
      return (incomingRequest, callback) => {
        const request = DNS._copyDNSPacket(incomingRequest, {
          flags: incomingRequest.flags & ~DnsPkt.RECURSION_DESIRED,
          additionals: [{
            type: 'OPT', name: '.', options: [{
              code: 'CLIENT_SUBNET',
              family: 1,
              sourcePrefixLength: 32,
              scopePrefixLength: 0,
              ip: rinfo.originalAddress,
            }]
          }]
        });
        const message = DnsPkt.encode(request);
        const msgout = Buffer.alloc(message.length + 2);
        msgout.writeUInt16BE(message.length);
        message.copy(msgout, 2);
        let timeout = setTimeout(() => {
          if (timeout) {
            this._addTimingFailure(tinfo, Date.now() - start);
            timeout = null;
            callback(null);
          }
        }, this._getTimeout(tinfo));
        const entry = this._allocAddress(rinfo.originalAddress);
        const socket = Native.getTCPSocketOnInterface(`d${entry.iface}`, entry.dnsAddress, 0);
        socket.connect({ port: tinfo._port, host: tinfo._address }, () => {
          socket.on('error', (e) => {
            console.error(e);
            if (timeout) {
              this._addTimingFailure(tinfo, Date.now() - start);
              clearTimeout(timeout);
              timeout = null;
              callback(null);
            }
            socket.destroy();
          });
          socket.on('data', (buffer) => {
            if (buffer.length >= 2) {
              const len = buffer.readUInt16BE();
              if (timeout && buffer.length >= 2 + len) {
                this._addTimingSuccess(tinfo, Date.now() - start);
                clearTimeout(timeout);
                timeout = null;
                callback(DnsPkt.decode(buffer.subarray(2, 2 + len)));
              }
            }
            socket.end();
          });
          socket.write(msgout);
        });
      }
    }
    else {
      const socket = await new Promise(async (resolve, reject) => {
        const entry = this._allocAddress(rinfo.originalAddress);
        if (entry.socket) {
          try {
            entry.socket.address(); // Will throw exception if socket not yet bound
            resolve(entry.socket);
          }
          catch {
            entry.socket.once('listening', () => resolve(entry.socket));
          }
        }
        else {
          entry.socket = Native.getUDPSocketOnInterface(`d${entry.iface}`, entry.dnsAddress, 0);
          entry.socket.once('error', e => {
            entry.socket = null;
            console.error(e);
            reject(e);
          });
          entry.socket.once('listening', () => {
            entry.socket.on('message', (message, { port, address }) => {
              if (message.length < 2) {
                return;
              }
              const id = message.readUInt16BE(0);
              this._pending[id] && this._pending[id](message);
            });
            resolve(entry.socket);
          });
        }
      });
      return (incomingRequest, callback) => {
        const request = DNS._copyDNSPacket(incomingRequest, {
          flags: incomingRequest.flags & ~DnsPkt.RECURSION_DESIRED,
          additionals: [{
            type: 'OPT', name: '.', options: [{
              code: 'CLIENT_SUBNET',
              family: 1,
              sourcePrefixLength: 32,
              scopePrefixLength: 0,
              ip: rinfo.originalAddress,
            }]
          }]
        });
        while (this._pending[request.id]) {
          request.id = Math.floor(Math.random() * 65536);
        }
        const id = request.id;
        const timeout = setTimeout(() => {
          if (this._pending[id]) {
            this._addTimingFailure(tinfo, Date.now() - start);
            delete this._pending[id];
            callback(null);
          }
        }, this._getTimeout(tinfo));
        this._pending[id] = (message) => {
          this._addTimingSuccess(tinfo, Date.now() - start);
          clearTimeout(timeout);
          delete this._pending[id];
          callback(DnsPkt.decode(message));
        };
        socket.send(DnsPkt.encode(request), tinfo._port, tinfo._address);
      }
    }
  },

  query: async function(request, response, rinfo, tinfo) {
    return new Promise(async (resolve, reject) => {
      try {
        (await this.getSocket(rinfo, tinfo))(request, (pkt) => {
          if (pkt) {
            if (pkt.rcode === 'NOERROR') {
              response.flags = pkt.flags;
              response.answers = pkt.answers;
              response.additionals = pkt.additionals;
              response.authorities = pkt.authorities;
              return resolve(NOERROR);
            }
            return resolve(NOTFOUND);
          }
          resolve(SERVFAIL);
        });
      }
      catch (e) {
        reject(e);
      }
    });
  },

  translateDNSNetworkAddress: function(address) {
    const entry = this._backwardCache[address];
    if (entry) {
      return entry.address;
    }
    return null;
  },

  _addTimingFailure: function(tinfo, time) {
    const dev = this._stddev(tinfo);
    this._addTimingSuccess(tinfo, time + STDEV_FAIL * dev.deviation);
  },

  _addTimingSuccess: function(tinfo, time) {
    tinfo._samples.shift();
    tinfo._samples.push(Math.min(time, tinfo._maxTimeout));
  },

  _getTimeout: function(tinfo) {
    const dev = this._stddev(tinfo);
    return Math.max(1, Math.min(dev.mean + STDEV_QUERY * dev.deviation, tinfo._maxTimeout));
  },

  _stddev: function(tinfo) {
    const mean = tinfo._samples.reduce((total, value) => total + value, 0) / tinfo._samples.length;
    const variance = tinfo._samples.reduce((total, value) => total + Math.pow(value - mean, 2), 0) / (tinfo._samples.length - 1);
    return {
      mean: mean,
      deviation: Math.sqrt(variance)
    };
  }
}

//
// LocalDNS proxies DNS servers on the DNS network.
//
const LocalDNS = function(addresses, port, timeout) {
  this._address = addresses[0];
  this._addresses = addresses
  this._port = port;
  this._maxTimeout = timeout;
  this._samples = Array(MAX_SAMPLES).fill(this._maxTimeout);
}

LocalDNS.prototype = {

  start: async function() {
  },

  stop: function() {
  },

  query: async function(request, response, rinfo) {
    // Dont send a query back to a server it came from.
    if (this._addresses.indexOf(rinfo.address) !== -1 || this._addresses.indexOf(rinfo.originalAddress) !== -1) {
      return NOTFOUND;
    }
    return await LocalDNSSingleton.query(request, response, rinfo, this);
  }
};

//
// MapDNS maps addresses which are from the DNS network back to their original values, does the lookup,
// and then send the answers back to the original caller.
//
const MapDNS = {

  query: async function(request, response, rinfo) {
    if (request.questions[0].type !== 'PTR') {
      return NOTFOUND;
    }
    const qname = request.questions[0].name;
    if (!qname.endsWith('.in-addr.arpa')) {
      return NOTFOUND;
    }
    const m4 = qname.split('.');
    const address = LocalDNSSingleton.translateDNSNetworkAddress(`${m4[3]}.${m4[2]}.${m4[1]}.${m4[0]}`);
    if (!address) {
      return NOTFOUND;
    }
    const i4 = address.split('.');
    if (i4.length !== 4) {
      return NOTFOUND;
    }
    const mname = `${i4[3]}.${i4[2]}.${i4[1]}.${i4[0]}.in-addr.arpa`;

    const mrequest = DNS._copyDNSPacket(request, {
      questions: [{ name: mname, type: 'PTR', class: 'IN' }],
    });
    const mresponse = DNS._copyDNSPacket(response);
    const success = await DNS.query(mrequest, mresponse, rinfo);
    if (!success) {
      return mresponse.flags & 0xF;
    }

    response.flags = mresponse.flags;
    response.answers = mresponse.answers.map(answer => {
      if (answer.name === mname) {
        answer.name = qname;
      }
      return answer;
    });

    return NOERROR;
  }

}

//
// DNS
// The main DNS system. This fields request and then tries to answer them by walking though a prioritized list of DNS servers.
// By default these handle local names, mulitcast names, address maps, and caching. We can also add global dns servers (which are
// references to DNS services on the physical network) as well as local dns servers (which are dns servers on our internal DNS network).
//
const DNS = { // { app: app, srv: proxy, cache: cache }

  _proxies: [
    { app: { _name: 'private', _position: { tab: -9 }, isRunning: () => true }, srv: PrivateDNS,   cache: false, local: true },
    { app: { _name: 'mdns',    _position: { tab: -8 }, isRunning: () => true }, srv: MulticastDNS, cache: false, local: true },
    { app: { _name: 'map',     _position: { tab: -7 }, isRunning: () => true }, srv: MapDNS,       cache: false, local: true },
    { app: { _name: 'cache',   _position: { tab: -6 }, isRunning: () => true }, srv: CachingDNS,   cache: false, local: true }
  ],

  start: async function(config) {
    this.setDomainName(config.domainname);
    this.setHostname(config.hostname, config.ip);
    this.setDefaultResolver(config.resolvers[0], config.resolvers[1]);

    const onMessage = async (msgin, rinfo) => {
      const start = Date.now();
      const response = {
        id: 0,
        type: 'response',
        flags: 0,
        questions: [],
        answers: [],
        authorities: [],
        additionals: []
      };
      try {
        if (msgin.length < 2) {
          throw Error('Bad length');
        }
        const request = DnsPkt.decode(msgin);
        Log('request', rinfo, JSON.stringify(request, null, 2));
        response.id = request.id;
        response.flags = request.flags;
        if ((response.flags & DnsPkt.RECURSION_DESIRED) !== 0) {
          response.flags |= DnsPkt.RECURSION_AVAILABLE;
        }
        response.questions = request.questions;
        const opt = request.additionals.find(additional => additional.type === 'OPT');
        const client = opt && opt.options.find(option => option.type === 'CLIENT_SUBNET'); // 'type' in decode, 'code' in encode
        if (client) {
          rinfo.originalAddress = client.ip; // Even as a partial address is more useful than the local address.
        }
        else {
          rinfo.originalAddress = rinfo.address;
        }
        await this.query(request, response, rinfo);
        if (response.answers.length === 0) {
          // If we got no answers, and no error code set, we set notfound
          if ((response.flags & 0x0F) === 0) {
            response.flags = (response.flags & 0xFFF0) | NOTFOUND;
          }
        }
        else {
          // If we got an answer, clear the error flag
          response.flags = (response.flags & 0xFFF0) | NOERROR;
        }
      }
      catch (e) {
        LogError(e);
        response.flags = (response.flags & 0xFFF0) | SERVFAIL;
      }
      Log('response', rinfo.tcp ? 'tcp' : 'udp', rinfo, JSON.stringify(DnsPkt.decode(DnsPkt.encode(response)), null, 2));
      LogTiming(`Query time ${Date.now() - start}ms: ${response.questions[0].name} ${response.questions[0].type}`);
      return DnsPkt.encode(response);
    }

    await new Promise(resolve => {
      const run = (callback) => {
        this._udp = UDP.createSocket({
          type: 'udp4',
          reuseAddr: true
        });
        this._udp.on('message', async (msgin, rinfo) => {
          const msgout = await onMessage(msgin, { tcp: false, address: rinfo.address, port: rinfo.port });
          try {
            this._udp.send(msgout, rinfo.port, rinfo.address, err => {
              if (err) {
                LogError(err);
              }
            });
          }
          catch (e) {
            this._udp.emit('error', e);
          }
        });
        this._udp.once('error', e => {
          LogError('DNS socket error - reopening', e);
          try {
            this._udp.close();
          }
          catch (_) {
          }
          this._udp = null;
          // Wait a moment before reopening
          setTimeout(() => run(() => {}), 1000);
        });
        this._udp.bind(config.port, callback);
      }
      run(resolve);
    });

    // Super primitive DNS over TCP handler
    await new Promise(resolve => {
      this._tcp = Net.createServer({
        allowHalfOpen: true
      }, socket => {
        socket.on('error', (e) => {
          console.error(e);
          socket.destroy();
        });
        socket.on('data', async (buffer) => {
          try {
            if (buffer.length >= 2) {
              const len = buffer.readUInt16BE();
              if (buffer.length >= 2 + len) {
                const msgin = buffer.subarray(2, 2 + len);
                const msgout = await onMessage(msgin, { tcp: true, address: socket.remoteAddress.replace(/^::ffff:/, ''), port: socket.remotePort });
                const reply = Buffer.alloc(msgout.length + 2);
                reply.writeUInt16BE(msgout.length, 0);
                msgout.copy(reply, 2);
                socket.end(reply);
              }
              else {
                socket.end();
              }
            }
          }
          catch (e) {
            console.error(e);
            socket.destroy();
          }
        });
      });
      this._tcp.on('error', (e) => {
        // If we fail to open the dns/tcp socket, report and move on.
        console.error(e);
        resolve();
      });
      this._tcp.listen(config.port, resolve);
    });

    await LocalDNSSingleton.start();

    // DNS order determined by app order in the tabs. If that changes we re-order DNS.
    // We flush the cache if the reordering is material to the DNS.
    Root.on('apps.tabs.reorder', () => {
      const order = this._proxies.reduce((t, a) => `${t},${a.app._name}`, '');
      this._proxies.sort((a, b) => a.app._position.tab - b.app._position.tab);
      const norder = this._proxies.reduce((t, a) => `${t},${a.app._name}`, '');
      if (order !== norder) {
        CachingDNS.flush();
      }
    });
  },

  stop: async function() {
    try {
      this._udp.close();
    }
    catch (_) {
    }
    try {
      this._tcp.close();
    }
    catch (_) {
    }
    await LocalDNSSingleton.stop();
  },

  setDefaultResolver: function(resolver1, resolver2) {
    this.removeDNSServer({ app: GLOBAL1 });
    this.removeDNSServer({ app: GLOBAL2 });
    if (resolver1) {
      this._addDNSProxy(GLOBAL1, new GlobalDNS(resolver1, 53, 5000, !resolver2), true, false);
    }
    if (resolver2) {
      this._addDNSProxy(GLOBAL2, new GlobalDNS(resolver2, 53, 5000, true), true, false);
    }
  },

  setClusterDNSAddress: function(ip) {
    this.removeDNSServer({ app: CLUSTER });
    if (ip) {
      this._addDNSProxy(CLUSTER, new GlobalDNS(ip, 53, 5000, true), true, false);
    }
  },

  addDNSServer: function(app, args) {
    const proxy = args.dnsNetwork ?
      new LocalDNS([ app._secondaryIP, app._homeIP ], args.port || 53, args.timeout || 5000) :
      new GlobalDNS(app._homeIP, args.port || 53, args.timeout || 5000, false);
    this._addDNSProxy(app, proxy, true, false);
    return { app: app, local: args.dnsNetwork, port: args.port || 53 };
  },

  _addDNSProxy: function(app, proxy, cache, local) {
    proxy.start().then(() => {
      this._proxies.push({ app: app, srv: proxy, cache: cache, local: local });
      this._proxies.sort((a, b) => a.app._position.tab - b.app._position.tab);
    });
    CachingDNS.flush();
  },

  _copyDNSPacket: function(pkt, changes) {
    const npkt = DnsPkt.decode(DnsPkt.encode(pkt));
    if (changes) {
      Object.assign(npkt, changes);
    }
    return npkt;
  },

  removeDNSServer: function(dns) {
    for (let i = 0; i < this._proxies.length; i++) {
      if (this._proxies[i].app === dns.app) {
        this._proxies.splice(i, 1)[0].srv.stop();
        CachingDNS.flush();
        break;
      }
    }
  },

  setHostname: function(hostname, ip) {
    hostname = hostname || 'MinkeBox';
    if (!DEBUG) {
      FS.writeFileSync(HOSTNAME_FILE, `${hostname}\n`);
      ChildProcess.spawnSync(HOSTNAME, [ '-F', HOSTNAME_FILE ]);
    }
    this.registerHost(hostname, null, ip, Network.getSLAACAddress());
  },

  setDomainName: function(domain) {
    PrivateDNS.setDomainName(domain);
  },

  registerHost: function(localname, globalname, ip, ip6) {
    PrivateDNS.registerHost(localname, globalname, ip, ip6);
  },

  unregisterHost: function(localname) {
    PrivateDNS.unregisterHost(localname);
  },

  lookupLocalnameIP: function(localname) {
    return PrivateDNS.lookupLocalnameIP(localname);
  },

  squery: async function(request, response, rinfo) {
    const question = request.questions[0];
    if (!question) {
      throw new Error('Missing question');
    }
    let error = SERVFAIL;
    for (let i = 0; i < this._proxies.length; i++) {
      const proxy = this._proxies[i];
      Log(`Trying ${proxy.app._name}`);
      if (proxy.app.isRunning()) {
        error = await proxy.srv.query(request, response, rinfo);
        if (error === NOERROR) {
          Log('Found');
          if (proxy.cache) {
            CachingDNS.add(response, NOERROR);
          }
          return true;
        }
      }
    }
    Log('Not found', error);
    if (ENABLE_NEGATIVE_CACHING) {
      if (response.authorities.length) {
        CachingDNS.add(response, error);
      }
    }
    response.flags = (response.flags & 0xFFF0) | error;
    return false;
  },

  pquery: async function(request, response, rinfo) {
    const question = request.questions[0];
    if (!question) {
      throw new Error('Missing question');
    }
    const done = [];
    let i = 0;
    for (; i < this._proxies.length; i++) {
      const proxy = this._proxies[i];
      if (!proxy.local) {
        break;
      }
      Log(`Trying local ${proxy.app._name}`);
      const lerror = await proxy.srv.query(request, response, rinfo);
      if (lerror === NOERROR) {
        Log('Found');
        if (proxy.cache) {
          CachingDNS.add(response, NOERROR);
        }
        return true;
      }
      done[i] = 'fail';
    }
    if (i >= this._proxies.length) {
      response.flags = (response.flags & 0xFFF0) | NOTFOUND;
      return false;
    }
    const vresponse = await new Promise(resolve => {
      let replied = false;
      for(; i < this._proxies.length; i++) {
        const proxy = this._proxies[i];
        Log(`Trying ${proxy.app._name}`);
        const presponse = DNS._copyDNSPacket(response);
        const idx = i;
        const start = Date.now();
        proxy.srv.query(Object.assign({}, request), presponse, rinfo).then(error => {
          Log(`Reply ${this._proxies[idx].app._name}`, error);
          LogTiming(`Query time ${Date.now() - start}ms ${this._proxies[idx].app._name}: ${question.name} ${question.type}`);
          if (!replied) {
            done[idx] = error === NOERROR ? presponse : 'fail';
            for (let k = 0; k < this._proxies.length; k++) {
              if (!done[k]) {
                // Query pending before we find an answer - need to wait for it to complete
                return;
              }
              else if (done[k] !== 'fail') {
                // Found an answer after earlier queries failed, go with this.
                replied = true;
                Log(`Success ${this._proxies[idx].app._name}`);
                if (this._proxies[k].cache) {
                  CachingDNS.add(done[k], NOERROR);
                }
                return resolve(done[k]);
              }
            }
            // Everything failed
            replied = true;
            Log('Not found');
            return resolve(null);
          }
        });
      }
    });
    if (!vresponse) {
      response.flags = (response.flags & 0xFFF0) | NOTFOUND;
      return false;
    }
    Object.assign(response, vresponse);
    return true;
  }

};

DNS.query = PARALLEL_QUERY ? DNS.pquery : DNS.squery;

module.exports = DNS;

const DnsPkt = require('dns-packet');
const Log = require('debug')('dns:caching');

const NOERROR = 0;
const SERVFAIL = 2;
const NOTFOUND = 3;

//
// CachingDNS caches lookups from external DNS servers to speed things up.
//
const CachingDNS = {

  _defaultTTL: 600, // 10 minutes
  _maxTTL: 3600, // 1 hour
  _defaultNegTTL: 30, // 30 seconds
  _qHighWater: 1000,
  _qLowWater: 900,

  _q: [],
  _qTrim: null,

  _cache: {
    A: {},
    AAAA: {},
    CNAME: {},
    SOA: {}
  },

  add: function(response, error) {
    // Dont cache truncated response
    if ((response.flags & DnsPkt.TRUNCATED_RESPONSE) !== 0) {
      return;
    }
    response.authorities.forEach(authority => this._addSOA(authority));
    response.answers.forEach(answer => this._addAnswer(answer));
    response.additionals.forEach(answer => this._addAnswer(answer));
    // If we didn't answer the question, create a negative entry
    const question = response.questions[0];
    if (!response.answers.find(answer => answer.type === question.type)) {
      this._addNegative(question, error)
    }
  },

  _addAnswer: function(answer) {
    switch (answer.type) {
      case 'A':
      case 'AAAA':
      case 'CNAME':
      {
        const name = answer.name.toLowerCase();
        const R = this._cache[answer.type][name] || (this._cache[answer.type][name] = {});
        const key = answer.data.toLowerCase();
        const expires = Math.floor(Date.now() / 1000 + Math.min(this._maxTTL, (answer.ttl || this._defaultTTL)));
        if (R[key]) {
          R[key].expires = expires;
        }
        else {
          if (R.negative) {
            R.negative.expires = 0;
          }
          const rec = { key: key, name: answer.name, type: answer.type, expires: expires, data: answer.data };
          R[key] = rec;
          this._q.push(rec);
        }
        break;
      }
      default:
        break;
    }

    if (this._q.length > this._qHighWater && !this._qTrim) {
      this._qTrim = setTimeout(() => this._trimAnswers(), 0);
    }
  },

  _addNegative: function(question, error) {
    switch (question.type) {
      case 'A':
      case 'AAAA':
      case 'CNAME':
        // Need SOA information to set the TTL of the negative cache entry. If we don't
        // have it we'll use a default (which should be quite short).
        const soa = this._findSOA(question.name);
        const ttl = error === SERVFAIL ? this._defaultNegTTL : Math.min(this._maxTTL, (soa && soa.data.minimum ? soa.data.minimum : this._defaultNegTTL));
        const name = question.name.toLowerCase();
        const R = this._cache[question.type][name] || (this._cache[question.type][name] = {});
        const expires = Math.floor(Date.now() / 1000 + ttl);
        if (R.negative) {
          // Only update the negative expires time if it's expired. Otherwise every failed lookup which
          // matches a negative record will update it again, so it will never expire.
          if (R.negative.expires < Math.floor(Date.now() / 1000)) {
            R.negative.expires = expires;
          }
        }
        else {
          const rec = { key: 'negative', name: question.name, type: question.type, expires: expires };
          R.negative = rec;
          this._q.push(rec);
        }
        break;
      default:
        break;
    }
  },

  _findAnswer: function(type, name) {
    const answers = [];
    switch (type) {
      case 'A':
      case 'AAAA':
      case 'CNAME':
      {
        const R = this._cache[type][name.toLowerCase()];
        if (R) {
          const now = Math.floor(Date.now() / 1000);
          if (R.negative && R.negative.expires > now) {
            break;
          }
          for (let key in R) {
            const rec = R[key];
            if (rec.expires > now) {
              answers.push({ name: rec.name, type: rec.type, ttl: rec.expires - now, data: rec.data });
            }
          }
          break;
        }
        break;
      }
      default:
        break;
    }
    return answers.length ? answers : null;
  },

  _findNegative: function(type, name) {
    switch (type) {
      case 'A':
      case 'AAAA':
      case 'CNAME':
      {
        const R = this._cache[type][name.toLowerCase()];
        if (R && R.negative && R.negative.expires > Math.floor(Date.now() / 1000)) {
          return true;
        }
        break;
      }
    }
    return false;
  },

  _addSOA: function(soa) {
    const key = soa.name.toLowerCase();
    const entry = this._cache.SOA[key];
    if (!entry) {
      const rec = { key: 'soa', name: soa.name, type: 'SOA', class: 'IN', expires: Math.floor(Date.now() / 1000 + Math.min(this._maxTTL, (soa.ttl || this._defaultTTL))), data: soa.data };
      this._cache.SOA[key] = { soa: rec };
      this._q.push(rec);
    }
  },

  _findSOA: function(name) {
    const sname = name.toLowerCase().split('.');
    for (let i = 0; i < sname.length; i++) {
      const soa = this._cache.SOA[sname.slice(i).join('.')];
      if (soa) {
        return soa.soa;
      }
    }
    return null;
  },

  _soa: function(name, response) {
    const soa = this._findSOA(name);
    if (soa) {
      response.authorities.push(soa);
      return true;
    }
    return false;
  },

  _trimAnswers: function() {
    const diff = this._q.length - this._qLowWater;
    Log(`Flushing ${diff}`)
    if (diff > 0) {
      this._q.sort((a, b) => a.expires - b.expires);
      const candidates = this._q.splice(0, diff);
      candidates.forEach(candidate => {
        const name = candidate.name.toLowerCase();
        const R = this._cache[candidate.type][name];
        if (R) {
          const key = candidate.key;
          if (R[key]) {
            delete R[key];
          }
          else {
            console.error('Missing trim entry', candidate);
          }
          if (Object.keys(R).length === 0) {
            delete this._cache[candidate.type][name];
          }
        }
        else {
          console.error('Missing trim list', candidate);
        }
      });
    }
    this._qTrim = null;
  },

  flush: function() {
    if (this._qTrim) {
      clearTimeout(this._qTrim);
      this._qTrim = null;
    }
    this._q = [];
    for (let key in this._cache) {
      this._cache[key] = {};
    }
  },

  query: async function(request, response, rinfo) {
    const question = request.questions[0];
    switch (question.type) {
      case 'A':
      case 'AAAA':
      {
        // Look for a cached A/AAAA record
        const a = this._findAnswer(question.type, question.name);
        if (a) {
          response.answers.push.apply(response.answers, a);
          return NOERROR;
        }
        const cname = this._findAnswer('CNAME', question.name);
        if (cname) {
          const ac = this._findAnswer(question.type, cname[0].data);
          if (ac) {
            response.answers.push.apply(response.answers, cname);
            response.answers.push.apply(response.answers, ac);
            return NOERROR;
          }
        }

        if (!this._findNegative(question.type, question.name)) {
          return NOTFOUND;
        }

        const soa = this._findSOA(question.name);
        if (soa) {
          response.answers.push.apply(response.answers, cname);
          response.authorities.push(soa);
          return NOERROR;
        }

        return NOTFOUND;
      }
      case 'CNAME':
      {
        const cname = this._findAnswer('CNAME', question.name);
        if (cname) {
          response.answers.push.apply(response.answers, cname);
          // See if we have a cached A/AAAA for the CNAME
          const a = this._findAnswer('A', cname[0].data);
          if (a) {
            response.additionals.push.apply(response.additionals, a);
          }
          const aaaa = this._findAnswer('AAAA', cname[0].data);
          if (aaaa) {
            response.additionals.push.apply(response.additionals, aaaa);
          }
          return NOERROR;
        }
        return NOTFOUND;
      }
      default:
        return NOTFOUND;
    }
  }

};

module.exports = CachingDNS;

const UDP = require('dgram');
const Net = require('net');
const DnsPkt = require('dns-packet');

const PrivateDNS = require('./PrivateDNS');

const NOERROR = 0;
const SERVFAIL = 2;
const NOTFOUND = 3;

const MAX_SAMPLES = 128;
const STDEV_QUERY = 4;
const STDEV_FAIL = 2;

const REGEXP_LOCAL_IP = /(^127\.)|(^192\.168\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^::1$)|(^[fF][cCdD])/;

//
// GlobalDNS proxies DNS servers on the Internet.
//
const GlobalDNS = function(address, port, timeout, last) {
  this._address = address;
  this._port = port;
  this._maxTimeout = timeout;
  this._samples = Array(MAX_SAMPLES).fill(this._maxTimeout);
  this._pending = {};
  this._active = false;
  // Identify local or global forwarding addresses. We don't forward local domain lookups to global addresses.
  if (address.match(REGEXP_LOCAL_IP)) {
    this._global = false;
  }
  else {
    this._global = true;
  }
  this._lastDNS = last;
}

GlobalDNS.prototype = {

  start: async function() {
    this._active = true;
    const reopen = (done) => {
      if (!this._active) {
        return;
      }
      this._socket = UDP.createSocket('udp4');
      this._socket.bind();
      this._socket.once('error', e => {
        Log(e);
        try {
          this._socket.close();
        }
        catch (_) {
        }
      });
      this._socket.once('close', () => {
        reopen(() => {});
      });
      this._socket.once('listening', () => {
        this._socket.on('message', (message, { port, address }) => {
          if (message.length < 2 || address !== this._address || port !== this._port) {
            return;
          }
          const id = message.readUInt16BE(0);
          this._pending[id] && this._pending[id](message);
        });
        done();
      });
    }
    return new Promise(resolve => reopen(resolve));
  },

  stop: function() {
    this._active = false;
    this._socket.close();
  },

  getSocket: function(rinfo) {
    const start = Date.now();
    if (rinfo.tcp) {
      return (incomingRequest, callback) => {
        const request = this._copyDNSPacket(incomingRequest, {
          additionals: []
        });
        const message = DnsPkt.encode(request);
        const msgout = Buffer.alloc(message.length + 2);
        msgout.writeUInt16BE(message.length);
        message.copy(msgout, 2);
        let timeout = setTimeout(() => {
          if (timeout) {
            this._addTimingFailure(Date.now() - start);
            timeout = null;
            callback(null);
          }
        }, this._getTimeout());
        const socket = Net.createConnection({ port: this._port, host: this._address }, () => {
          socket.on('error', (e) => {
            console.error(e);
            if (timeout) {
              this._addTimingFailure(Date.now() - start);
              clearTimeout(timeout);
              timeout = null;
              callback(null);
            }
            socket.destroy();
          });
          socket.on('data', (buffer) => {
            if (buffer.length >= 2) {
              const len = buffer.readUInt16BE();
              if (timeout && buffer.length >= 2 + len) {
                this._addTimingSuccess(Date.now() - start);
                clearTimeout(timeout);
                timeout = null;
                callback(DnsPkt.decode(buffer.subarray(2, 2 + len)));
              }
            }
            socket.end();
          });
          socket.write(msgout);
        });
      }
    }
    else {
      return (incomingRequest, callback) => {
        const request = this._copyDNSPacket(incomingRequest, {
          additionals: []
        });
        while (this._pending[request.id]) {
          request.id = Math.floor(Math.random() * 65536);
        }
        const id = request.id;
        const timeout = setTimeout(() => {
          try {
            this._socket.close();
          }
          catch (_) {
          }
          if (this._pending[id]) {
            this._addTimingFailure(Date.now() - start);
            delete this._pending[id];
            callback(null);
          }
        }, this._getTimeout());
        this._pending[id] = (message) => {
          this._addTimingSuccess(Date.now() - start);
          clearTimeout(timeout);
          delete this._pending[id];
          callback(DnsPkt.decode(message));
        };
        this._socket.send(DnsPkt.encode(request), this._port, this._address);
      }
    }
  },

  query: async function(request, response, rinfo) {
    // Dont send a query back to a server it came from.
    if (rinfo.address === this._address || rinfo.originalAddress === this._address) {
      return NOTFOUND;
    }

    // Check we're not trying to looking up local addresses globally
    if (this._global && request.questions[0].type === 'A' || request.questions[0].type === 'AAAA') {
      const name = request.questions[0].name.split('.');
      const domain = name[name.length - 1].toLowerCase();
      if (domain === 'local' || domain === PrivateDNS._domainName) {
        return NOTFOUND;
      }
    }

    return new Promise((resolve, reject) => {
      try {
        this.getSocket(rinfo)(request, (pkt) => {
          if (pkt) {
            if (pkt.rcode === 'NOERROR') {
              response.flags = pkt.flags;
              response.answers = pkt.answers;
              response.additionals = pkt.additionals;
              response.authorities = pkt.authorities;
              return resolve(NOERROR);
            }
            return resolve(NOTFOUND);
          }
          resolve(SERVFAIL);
        });
      }
      catch (e) {
        reject(e);
      }
    });
  },

  _copyDNSPacket: function(pkt, changes) {
    const npkt = DnsPkt.decode(DnsPkt.encode(pkt));
    if (changes) {
      Object.assign(npkt, changes);
    }
    return npkt;
  },

  _addTimingSuccess: function(time) {
    this._samples.shift();
    this._samples.push(Math.min(time, this._maxTimeout));
  },

  _addTimingFailure: function(time) {
    const dev = this._stddev();
    this._addTimingSuccess(time + STDEV_FAIL * dev.deviation);
  },

  _getTimeout: function() {
    // The last dns uses the maxTimeout because it's the final attempt at an answer and there's
    // no reason to terminate it early.
    if (this._lastDNS) {
      return this._maxTimeout;
    }
    else {
      const dev = this._stddev();
      return Math.max(1, Math.min(dev.mean + STDEV_QUERY * dev.deviation, this._maxTimeout));
    }
  },

  _stddev: function() {
    const mean = this._samples.reduce((total, value) => total + value, 0) / this._samples.length;
    const variance = this._samples.reduce((total, value) => total + Math.pow(value - mean, 2), 0) / (this._samples.length - 1);
    return {
      mean: mean,
      deviation: Math.sqrt(variance)
    };
  }

};

module.exports = GlobalDNS;

const Config = require('../Config');
const DnsPkt = require('dns-packet');
const LogError = require('debug')('dns:error');

const NOERROR = 0;
const NOTFOUND = 3;

//
// PrivateDNS provides mappings for locally hosted services.
//
const PrivateDNS = {

  _ttl: 600, // 10 minutes
  _hostname2ip4: {},
  _hostname2ip6: {},
  _ip2localname: {},
  _ip2globalname: {},
  _domainName: '',
  _soa: null,

  query: async function(request, response, rinfo) {
    switch (request.questions[0].type) {
      case 'A':
      {
        const fullname = request.questions[0].name.toLowerCase();
        let name = null;
        const sname = fullname.split('.');
        if (sname.length === 1 && !this._domainName) {
          name = sname[0];
        }
        else if (sname.length === 2 && sname[1] === this._domainName) {
          name = sname[0];
        }
        else if (sname.length === 3 && Config.GLOBALDOMAIN === `.${sname[1]}.${sname[2]}`) {
          name = fullname;
        }
        if (name) {
          const ip = this._hostname2ip4[name];
          if (ip) {
            response.answers.push(
              { name: fullname,  ttl: this._ttl, type: 'A', class: 'IN', data: ip }
            );
            const ip6 = this._hostname2ip6[name];
            if (ip6) {
              response.additionals.push(
                { name: fullname, ttl: this._ttl, type: 'AAAA', class: 'IN', data: ip6 }
              );
            }
            if (this._soa) {
              response.authorities.push({ name: fullname, ttl: this._ttl, type: 'SOA', class: 'IN', data: this._soa });
            }
            response.flags |= DnsPkt.AUTHORITATIVE_ANSWER;
            return NOERROR;
          }
          LogError('Failed to find private name', name);
        }
        break;
      }
      case 'AAAA':
      {
        const fullname = request.questions[0].name.toLowerCase();
        let name = null;
        const sname = fullname.split('.');
        if (sname.length === 1 && !this._domainName) {
          name = sname[0];
        }
        else if (sname.length === 2 && sname[1] === this._domainName) {
          name = sname[0];
        }
        else if (sname.length === 3 && Config.GLOBALDOMAIN === `.${sname[1]}.${sname[2]}`) {
          name = fullname;
        }
        if (name) {
          const ip6 = this._hostname2ip6[name];
          if (ip6) {
            response.answers.push(
              { name: fullname, ttl: this._ttl, type: 'AAAA', class: 'IN', data: ip6 }
            );
            const ip = this._hostname2ip4[name];
            if (ip) {
              response.additionals.push(
                { name: fullname, ttl: this._ttl, type: 'A', class: 'IN', data: ip }
              );
            }if (this._soa) {
              response.authorities.push({ name: fullname, ttl: this._ttl, type: 'SOA', class: 'IN', data: this._soa });
            }
            response.flags |= DnsPkt.AUTHORITATIVE_ANSWER;
            return NOERROR;
          }
        }
        break;
      }
      case 'PTR':
      {
        const name = request.questions[0].name;
        if (name.endsWith('.in-addr.arpa')) {
          const m4 = name.split('.');
          const ip = `${m4[3]}.${m4[2]}.${m4[1]}.${m4[0]}`;
          const localname = this._ip2localname[ip];
          if (localname) {
            response.answers.push(
              { name: name, ttl: this._ttl, type: 'PTR', class: 'IN', data: `${localname}${this._domainName ? '.' + this._domainName : ''}` }
            );
            if (this._soa) {
              response.authorities.push({ name: name, ttl: this._ttl, type: 'SOA', class: 'IN', data: this._soa });
            }
            response.flags |= DnsPkt.AUTHORITATIVE_ANSWER;
            return NOERROR;
          }
        }
        else if (name.endsWith('.ip6.arpa')) {
          const m6 = name.split('.');
          const ip6 = `${m6[31]}${m6[30]}${m6[29]}${m6[28]}:${m6[27]}${m6[26]}${m6[25]}${m6[24]}:${m6[23]}${m6[22]}${m6[21]}${m6[20]}:${m6[19]}${m6[18]}${m6[17]}${m6[16]}:${m6[15]}${m6[14]}${m6[13]}${m6[12]}:${m6[11]}${m6[10]}${m6[9]}${m6[8]}:${m6[7]}${m6[6]}${m6[5]}${m6[4]}:${m6[3]}${m6[2]}${m6[1]}${m6[0]}`;
          const localname = this._ip2localname[ip6];
          if (localname) {
            response.answers.push(
              { name: name, ttl: this._ttl, type: 'PTR', class: 'IN', data: `${localname}${this._domainName ? '.' + this._domainName : ''}` }
            );
            if (this._soa) {
              response.authorities.push({ name: name, ttl: this._ttl, type: 'SOA', class: 'IN', data: this._soa });
            }
            response.flags |= DnsPkt.AUTHORITATIVE_ANSWER;
            return NOERROR;
          }
        }
        break;
      }
      case 'SOA':
      {
        const fullname = request.questions[0].name;
        const name = fullname.split('.');
        if (this._soa && ((name.length === 2 && name[1].toLowerCase() === this._domainName)  || (name.length === 1 && !this._domainName))) {
          response.answers.push({ name: fullname, ttl: this._ttl, type: 'SOA', class: 'IN', data: this._soa });
          response.flags |= DnsPkt.AUTHORITATIVE_ANSWER;
          return NOERROR;
        }
        break;
      }
      default:
        break;
    }
    return NOTFOUND;
  },

  setDomainName: function(name) {
    this._domainName = name.toLowerCase();
    this._soa = {
      mname: this._domainName,
      rname: `dns-admin.${this._domainName}`,
      serial: 1,
      refresh: this._ttl, // Time before secondary should refresh
      retry: this._ttl, // Time before secondary should retry
      expire: this._ttl * 2, // Time secondary should consider its copy authorative
      minimum: Math.floor(this._ttl / 10) // Time to cache a negative lookup
    };
  },

  registerHost: function(localname, globalname, ip, ip6) {
    const kLocalname = localname.toLowerCase();
    this._hostname2ip4[kLocalname] = ip;
    this._ip2localname[ip] = localname;
    if (ip6) {
      this._hostname2ip6[kLocalname] = ip6;
      this._ip2localname[ip6] = localname;
    }
    if (globalname) {
      const kGlobalname = globalname.toLowerCase();
      this._hostname2ip4[kGlobalname] = ip;
      this._ip2globalname[ip] = globalname;
      if (ip6) {
        this._hostname2ip6[kGlobalname] = ip6;
        this._ip2globalname[ip6] = globalname;
      }
    }
  },

  unregisterHost: function(localname) {
    const kLocalname = localname.toLowerCase();
    const ip = this._hostname2ip4[kLocalname];
    const ip6 = this._hostname2ip6[kLocalname];
    const kGlobalname = (this._ip2localname[ip] || '').toLowerCase();

    delete this._hostname2ip4[kLocalname];
    delete this._hostname2ip4[kGlobalname];
    delete this._hostname2ip6[kLocalname];
    delete this._hostname2ip6[kGlobalname];
    delete this._ip2localname[ip];
    delete this._ip2globalname[ip];
    delete this._ip2localname[ip6];
    delete this._ip2globalname[ip6];
  },

  lookupLocalnameIP: function(localname) {
    return this._hostname2ip4[localname.toLowerCase()];
  }
};

module.exports = PrivateDNS;

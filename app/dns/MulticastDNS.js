const DnsPacket = require('dns-packet');
const Dgram = require('dgram');
const Log = require('debug')('mdns');

const MCAST_ADDRESS = '224.0.0.251';
const PORT = 5353;
const SERVICES = '_services._dns-sd._udp';
const DEFAULT_TTL = 120; // 2 minutes
const MINKEBOX_PTR = '_minkebox._tcp.local';
const DEFAULT_QUERY_INTERVAL = 60 * 1000; // 1 minute

const REGEXP_PTR_ZC = /^(b|lb|db)\._dns-sd\._udp\.(.*)$/;

const NOERROR = 0;
const NOTFOUND = 3;

function eq(a, b) {
  return a.localeCompare(b, 'en', { sensitivity: 'base' }) == 0;
}

const MulticastDNS = {

  _records: [],
  _A: [],
  _minkeHosts: {},

  start: async function(config) {
    await this._create(config.ipaddress);

    // Query for other MinkeBoxes
    this._timer = setInterval(() => {
      this._query({ name: MINKEBOX_PTR, type: 'PTR' });
    }, DEFAULT_QUERY_INTERVAL);
    this._query({ name: MINKEBOX_PTR, type: 'PTR' });
  },

  stop: async function() {
    if (this._timer) {
      clearInterval(this._timer);
      this._timer = null;
    }
    if (this._socket) {
      await Promise.all(this._records.map((rec) => {
        return this.removeRecord(rec);
      }));
      this._socket.close();
      this._socket = null;
    }
  },

  addRecord: async function(rec) {
    const idx = this._records.findIndex(r => eq(r.hostname, rec.hostname) && eq(r.service, rec.service));
    if (idx !== -1) {
      const orec = this._records.splice(idx, 1);
      await this._unannounce(orec[0]);
    }
    this._records.push(rec);
    await this._announce(rec);
    return rec;
  },

  removeRecord: async function(rec) {
    const idx = this._records.findIndex(r => eq(r.hostname, rec.hostname) && eq(r.service, rec.service));
    if (idx !== -1) {
      const orec = this._records.splice(idx, 1);
      await this._unannounce(orec[0]);
    }
  },

  getAddrByHostname: function(hostname) {
    hostname = hostname.toLowerCase();
    const rec = this._records.find(r => eq(r.hostname, hostname));
    if (rec) {
      return rec.ip;
    }
    const answer = this._A[hostname];
    if (answer) {
      if (answer.expires > Date.now() / 1000) {
        return answer.ip;
      }
      delete this._A[hostname];
    }
    return null;
  },

  _create: async function(ip) {
    return new Promise(resolve => {
      this._socket = Dgram.createSocket({
        type: 'udp4',
        reuseAddr: true
      }, (msg, rinfo) => {
        this._incoming(msg);
      });
      this._socket.bind(PORT, MCAST_ADDRESS, () => {
        this._socket.setMulticastTTL(255);
        this._socket.setMulticastLoopback(false);
        this._socket.addMembership(MCAST_ADDRESS, ip);
        this._socket.setMulticastInterface(ip);
        resolve();
      });
    });
  },

  _incoming: function(msg) {
    const pkt = DnsPacket.decode(msg);
    if (pkt.type === 'query') {
      pkt.questions.forEach(q => {
        if (q.name.toLowerCase().startsWith(SERVICES)) {
          const domain = q.name.split('.').slice(-1);
          this._answer(Object.values(this._records.reduce((acc, rec) => {
            if (eq(rec.domainname, domain)) {
              acc[`${rec.service}.${rec.domainname}`] = { name: `${SERVICES}.${domain}`, type: 'PTR', ttl: 4500, data: `${rec.service}.${rec.domainname}` };
            }
            return acc;
          }, {})))
        }
        else {
          const srec = this._records.find(rec => eq(q.name, `${rec.hostname}.${rec.service}.${rec.domainname}`));
          if (srec) {
            this._answer([
              { name: q.name, type: 'SRV', data: { priority: 0, weight: 0, port: srec.port, target: `${srec.hostname}.${srec.domainname}` }},
              { name: q.name, type: 'TXT', data: srec.txt || [] }
            ]);
          }
          else {
            const arec = this._records.find(rec => eq(q.name, `${rec.hostname}.${rec.domainname}`));
            if (arec) {
              this._answer([{ name: q.name, type: 'A', data: arec.ip }]);
            }
            else {
              const panswers = this._records.reduce((acc, rec) => {
                if (eq(q.name, `${rec.service}.${rec.domainname}`)) {
                  acc.push({ name: `${rec.service}.${rec.domainname}`, type: 'PTR', data: `${rec.hostname}.${rec.service}.${rec.domainname}` });
                }
                return acc;
              }, []);
              if (panswers.length) {
                this._answer(panswers);
              }
            }
          }
        }
      });
    }
    else if (pkt.type === 'response') {
      const now = Math.floor(Date.now() / 1000);
      pkt.answers.forEach(answer => {
        Log('response:', answer);
        switch (answer.type) {
          case 'A':
            const name = answer.name.split('.');
            if (name.length === 2 && name[1] === 'local') {
              this._A[name[0].toLowerCase()] = { name: answer.name, ip: answer.data, expires: (answer.ttl || DEFAULT_TTL) + now };
              if (this._minkeHosts[name[0]]) {
                this._minkeHosts[name[0]] = this._A[name[0].toLowerCase()];
                this._updateMinkboxRecords();
              }
            }
            break;
          case 'PTR':
            if (answer.name === MINKEBOX_PTR) {
              const hostname = answer.data.split('.')[0];
              if (!this._minkeHosts[hostname]) {
                this._minkeHosts[hostname] = { expires: 0 };
              }
              this._query({ name: `${hostname}.local`, type: 'A' });
              this._updateMinkboxRecords();
            }
            break;
          default:
            break;
        }
      });
    }
  },

  _announce: async function(rec) {
    await this._answer([
      { name: `${rec.service}.${rec.domainname}`,                 type: 'PTR', data: `${rec.hostname}.${rec.service}.${rec.domainname}` },
      { name: `${rec.hostname}.${rec.service}.${rec.domainname}`, type: 'SRV', data: { priority: 0, weight: 0, port: rec.port, target: `${rec.hostname}.${rec.domainname}` }},
      { name: `${rec.hostname}.${rec.service}.${rec.domainname}`, type: 'TXT', data: rec.txt },
      { name: `${rec.hostname}.${rec.domainname}`,                type: 'A',   data: rec.ip }
    ]);
  },

  _unannounce: async function(rec) {
    await this._answer([
      { name: `${rec.service}.${rec.domainname}`,                 ttl: 0, type: 'PTR', data: `${rec.hostname}.${rec.service}.${rec.domainname}` },
      { name: `${rec.hostname}.${rec.service}.${rec.domainname}`, ttl: 0, type: 'SRV', data: { priority: 0, weight: 0, port: rec.port, target: `${rec.hostname}.${rec.domainname}` }},
      { name: `${rec.hostname}.${rec.service}.${rec.domainname}`, ttl: 0, type: 'TXT', data: rec.txt },
      { name: `${rec.hostname}.${rec.domainname}`,                ttl: 0, type: 'A',   data: rec.ip }
    ]);
  },

  _answer: async function(answers) {
    Log('_answer:', answers);
    if (answers.length && this._socket) {
      return new Promise((resolve) => {
        const msg = DnsPacket.encode({
          type: 'response',
          questions: [],
          authorities: [],
          additionals: [],
          answers: answers.map(answer => Object.assign({ class: 'IN', flush: false, ttl: 120 }, answer))
        });
        this._socket.send(msg, 0, msg.length, PORT, MCAST_ADDRESS, () => {
          resolve(true);
        });
      });
    }
    else {
      return false;
    }
  },

  _query: async function(query) {
    Log('_query:', query);
    if (this._socket) {
      return new Promise((resolve) => {
        const msg = DnsPacket.encode({
          type: 'query',
          questions: [ query ],
          authorities: [],
          additionals: [],
          answers: []
        });
        this._socket.send(msg, 0, msg.length, PORT, MCAST_ADDRESS, () => {
          resolve(true);
        });
      });
    }
    else {
      return false;
    }
  },

  _updateMinkboxRecords: function() {
    const now = Math.floor(Date.now() / 1000);
    const hosts = [];
    for (let key in this._minkeHosts) {
      const A = this._minkeHosts[key];
      if (A.expires > now) {
        hosts.push({ name: key, ip: A.ip });
      }
    }
    Root.emit('cluster.hosts.update', hosts);
  },

  // -----
  // MDNS query manager for DNS system
  // -----

  _defaultTTL: 60, // 1 minute

  query: async function(request, response, rinfo) {
    const question = request.questions[0];
    switch (question.type) {
      case 'A':
      {
        const name = question.name.split('.');
        if (name[name.length - 1].toLowerCase() === 'local') {
          const ip = this.getAddrByHostname(name[0]);
          if (ip && name.length === 2) {
            response.answers.push({ name: question.name, type: 'A', ttl: this._defaultTTL, class: 'IN', data: ip });
          }
          // Return true regardless of a match to stop the query process. We don't look for 'local' anywhere else.
          return NOERROR;
        }
        break;
      }
      case 'PTR':
      {
        const match = question.name.match(REGEXP_PTR_ZC);
        if (match) {
          response.flags = (response.flags & 0xFFF0) | NOTFOUND;
          return NOERROR;
        }
        break;
      }
      default:
        break;
    }
    return NOTFOUND;
  }

};

module.exports = MulticastDNS;

#! /usr/bin/node

global.DEBUG = !!process.env.DEBUG;
global.SYSTEM = !!process.env.SYSTEM;

const Koa = require('koa');
const Router = require('koa-router');
const Websockify = require('koa-websocket');
const CacheControl = require('koa-cache-control');
const Docker = require('dockerode');
const EventEmitter = require('events');
const Config = require('./Config');
const Events = require('./utils/Events');

// More listeners
EventEmitter.defaultMaxListeners = 50;

global.Root = new Events(); // System events

const Pages = require('./pages/pages');
const MinkeSetup = require('./app/MinkeSetup');
const UPNP = require('./sys/UPNP');
const Startup = require('./app/Startup');
const ClusterManager = require('./cluster/Manager');

const RESTART_REASON = `${Config.ROOT}/minke-restart-reason`;
const PORT = Config.WEB_PORT;

global.koaApp = Websockify(new Koa());
global.docker = new Docker({socketPath: '/var/run/docker.sock'});

koaApp.on('error', (err) => {
  console.error(err);
});

koaApp.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  const ip = MinkeSetup.getIPAddress();
  const domainname = MinkeSetup.getLocalDomainName();
  const domainsrc = domainname ? `http://*.${domainname} http://*.${domainname}:* https://*.${domainname}` : '';
  const cluster = ClusterManager.getHosts().map(host => `http://${host.ip} http://${host.ip}:*`).join(' ');
  ctx.set('Content-Security-Policy',
    "default-src 'self';" +
    "script-src 'self' 'unsafe-inline' 'unsafe-eval';" +
    "style-src 'self' 'unsafe-inline';" +
    "img-src 'self' data:;" +
    `frame-src ${domainsrc} ${cluster} http://${ip} http://${ctx.request.header.host}:* https://*.minkebox.net;` +
    `connect-src 'self' ws://${ctx.headers.host};` +
    "font-src 'none';" +
    "object-src 'none';" +
    "media-src 'none';"
  );
  ctx.set('X-Response-Time', `${ms}ms`);
});

koaApp.use(CacheControl({ noCache: true }));

const root = Router();
const wsroot = Router();

Pages.register(root, wsroot);
UPNP.register(root, wsroot);
ClusterManager.register(root, wsroot);

koaApp.use(root.middleware());
koaApp.ws.use(wsroot.middleware());
koaApp.ws.use(async (ctx, next) => {
  await next(ctx);
  if (ctx.websocket.listenerCount('message') === 0) {
    ctx.websocket.close();
  }
});

let restartReason = 'restart';
try {
  restartReason = FS.readFileSync(RESTART_REASON, { encoding: 'utf8' }).trim();
}
catch (_) {
}
try {
  FS.writeFileSync(RESTART_REASON, 'exit');
}
catch (_) {
}

Startup.start({ inherit: restartReason === 'restart' || restartReason === 'update', port: PORT });

process.on('uncaughtException', (e) => {
  console.error(e)
});
process.on('SIGINT', async () => {
  await MinkeSetup.getSingleton().systemRestart('halt');
});
process.on('SIGTERM', async () => {
  await MinkeSetup.getSingleton().systemRestart('exit');
});
process.on('SIGUSR1', async() => {
  await MinkeSetup.getSingleton().systemRestart('restart');
});

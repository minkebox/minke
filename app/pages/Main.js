const FS = require('fs');
const Config = require('../Config');
const Handlebars = require('./HB');
const MinkeSetup = require('../app/MinkeSetup');
const Images = require('../images/Images');
const Applications = require('../app/Applications');
const Build = require('../Build');

const NRTAGS = 20;

function _strhash(str) {
  let hash = 5381;
  const bytes = Buffer.from(str, 'utf8');
  for (let i = 0; i < bytes.length; i++) {
    hash = (hash << 5) - hash + bytes[i];
  }
  return hash & 0x7fffffff;
}

function tagColor(tag) {
  return _strhash(tag.toLowerCase()) % NRTAGS
}

function tagsToMap(tags) {
  return tags.map(tag => { return { name: tag, color: tagColor(tag) } });
}

function genApp(app) {
  const link = app.getWebLink('tab');
  return {
    _app: app,
    _id: app._id,
    cluster: app._cluster,
    name: app._name,
    status: app._status,
    ip: app._status !== 'running' ? null : app._defaultIP,
    link: link.url,
    linktarget: link.target,
    tags: app._tags,
    tagcolor: tagColor(app._tags[0].toLowerCase()),
    position: app._position.tab,
    networks: [
      app._networks.primary.name === 'host' ? 'home' : app._networks.primary.name,
      app._networks.secondary.name === 'host' ? 'home' : app._networks.secondary.name
    ]
  }
}

function genAppStatus(acc, app) {
  if (app._monitor.cmd) {
    const link = app.getWebLink('widget');
    acc.push({
      _app: app,
      _id: app._id,
      cluster: app._cluster,
      name: app._name,
      init: app._statusMonitor && app._statusMonitor.init,
      link: link.url,
      linktarget: link.target,
      running: app._status === 'running',
      tags: app._tags,
      tagcolor: tagColor(app._tags[0]),
      position: app._position.widget,
      networks: [
        app._networks.primary.name === 'host' ? 'home' : app._networks.primary.name,
        app._networks.secondary.name === 'host' ? 'home' : app._networks.secondary.name
      ]
    });
  }
  return acc;
}

function posSort(a, b) {
  return a.position - b.position;
}

function sortAndRenumber(list, tag) {
  list.sort(posSort);
  for (let i = 0; i < list.length; i++) {
    list[i]._position = i;
    list[i]._app._position[tag] = i;
  }
}

let mainTemplate;
let appTemplate;
let appStatusTemplate;
let tagsTemplate;
let networksTemplate;
let hostsTemplate;
function registerTemplates() {
  const partials = [
    'App',
    'AppStatus',
    'Tags',
    'Networks',
    'Hosts'
  ];
  partials.forEach((partial) => {
    Handlebars.registerPartial(partial, FS.readFileSync(`${__dirname}/html/partials/${partial}.html`, { encoding: 'utf8' }));
  });
  mainTemplate = Handlebars.compile(FS.readFileSync(`${__dirname}/html/Main.html`, { encoding: 'utf8' }));
  appTemplate = Handlebars.compile('{{> App}}');
  appStatusTemplate = Handlebars.compile('{{> AppStatus}}');
  tagsTemplate = Handlebars.compile('{{> Tags}}');
  networksTemplate = Handlebars.compile('{{> Networks}}');
  hostsTemplate = Handlebars.compile('{{> Hosts}}');
}
if (!DEBUG) {
  registerTemplates();
}

async function MainPageHTML(ctx) {

  if (DEBUG) {
    registerTemplates();
  }

  const tags = Applications.getTags();
  const networks = Applications.getNetworks(true);
  const hosts = Applications.getHosts();
  const apps = Applications.getAll().map(app => genApp(app));
  const statuses = Applications.getAll().reduce((acc, app) => genAppStatus(acc, app), []);
  sortAndRenumber(apps, 'tab');
  sortAndRenumber(statuses, 'widget');
  ctx.body = mainTemplate({
    configName: Config.CONFIG_NAME === 'Production' ? null : Config.CONFIG_NAME,
    Build: Build,
    Advanced: MinkeSetup.getAdvancedMode(),
    DarkMode: MinkeSetup.getDarkMode(),
    tags: tagsToMap(tags),
    networks: networks,
    hosts: hosts,
    apps: apps,
    statuses: statuses
  });
  ctx.type = 'text/html';
}

async function MainPageWS(ctx) {

  const onlines = {};
  Applications.getAll().forEach(app => onlines[app._id] = app._status);

  function send(msg) {
    try {
      ctx.websocket.send(JSON.stringify(msg));
    }
    catch (_) {
    }
  }

  function updateStatus(event) {
    if (event.status !== onlines[event.app._id] || event.app._mainMinkeApp) {
      const html = appTemplate(genApp(event.app));
      send({
        type: 'html.replace',
        selector: `.application-${event.app._id}`,
        html: html
      });
      const appstatus = genAppStatus([], event.app);
      if (appstatus.length) {
        send({
          type: 'html.replace',
          selector: `.application-status-${event.app._id}`,
          html: appStatusTemplate(appstatus[0])
        });
      }
      send({
        type: 'html.update',
        selector: '#tag-insertion-point',
        html: tagsTemplate({ tags: tagsToMap(Applications.getTags()) })
      });
      onlines[event.app._id] = event.status;
    }
  }

  function createApp(status) {
    const html = appTemplate(genApp(status.app));
    send({
      type: 'html.append',
      selector: `#app-insertion-point`,
      html: html
    });
    const appstatus = genAppStatus([], status.app);
    if (appstatus.length) {
      send({
        type: 'html.append',
        selector: `#appstatus-insertion-point`,
        html: appStatusTemplate(appstatus[0])
      });
    }
    send({
      type: 'html.update',
      selector: '#tag-insertion-point',
      html: tagsTemplate({ tags: tagsToMap(Applications.getTags()) })
    });

    // Appending app to tabs, so will get the last position.
    // App has already been added to the list.
    const tabs = Applications.getAll();
    status.app._position.tab = tabs.length - 1;
    if (appstatus.length) {
      // Appending to the widgets, so we have to calculate that position as not all apps have widgets
      const count = tabs.reduce((acc, app) => acc += (app._monitor.cmd ? 1 : 0), 0);
      status.app._position.widget = count - 1;
    }
  }

  function removeApp(status) {
    send({
      type: 'html.remove',
      selector: `.application-${status.app._id}`
    });
    send({
      type: 'html.remove',
      selector: `.application-status-${status.app._id}`
    });
    send({
      type: 'html.update',
      selector: '#tag-insertion-point',
      html: tagsTemplate({ tags: tagsToMap(Applications.getTags()) })
    });

    // Remove app from tabs.
    // App has already been removed.
    const tabs = Applications.getAll();
    tabs.forEach(app => {
      if (app._position.tab >= status.app._position.tab) {
        app._position.tab--;
      }
    });
    const widgets = Applications.getAll().reduce((acc, app) => ((app._monitor.cmd ? acc.push(app) : false), acc), []);
    widgets.forEach(app => {
      if (app._position.tab >= status.app._position.widget) {
        app._position.widget--;
      }
    });
  }

  function updateNetworks() {
    const networks = Applications.getNetworks(true);
    send({
      type: 'html.update',
      selector: '#network-insertion-point',
      html: networksTemplate({ networks: networks })
    });
  }

  function updateHosts() {
    const hosts = Applications.getHosts();
    send({
      type: 'html.update',
      selector: '#host-insertion-point',
      html: hostsTemplate({ hosts: hosts })
    });
  }

  function updateOperational(data) {
    send({
      type: 'html.update',
      selector: '.main .operational',
      html: `CPU: ${data.cpuLoad}%&nbsp;&nbsp;&nbsp;Memory: ${data.memoryUsed}%`
    });
  }

  function openCaptcha(data) {
    send({ type: 'system.captcha', url: `${Config.CAPTCH_QUESTION}?darkmode=${MinkeSetup.getDarkMode()}` });
    Root.emit('system.captcha.token', { token: 'maybe' });
  }

  ctx.websocket.on('message', async (msg) => {
    try {
      msg = JSON.parse(msg);
      switch (msg.type) {
        case 'monitor.request':
        {
          const app = Applications.getById(msg.value.id);
          if (app && app._statusMonitor) {
            send({
              type: 'monitor.reply',
              id: app._id,
              reply: await app._statusMonitor.update(msg.value.timeout)
            });
          }
          break;
        }
        case 'app.move.tab':
        {
          const move = Applications.getById(msg.value.id);
          const from = msg.value.from;
          const to = msg.value.to;
          if (move && from !== to) {
            const tosave = [ move ];
            const tabs = Applications.getAll();
            if (from > to) {
              tabs.forEach(app => {
                if (app._position.tab >= to && app._position.tab <= from) {
                  app._position.tab++;
                  tosave.push(app);
                }
              });
            }
            else {
              tabs.forEach(app => {
                if (app._position.tab >= from && app._position.tab <= to) {
                  app._position.tab--;
                  tosave.push(app);
                }
              });
            }
            move._position.tab = to;
            tosave.forEach(app => app.save().then(_ => {}));
            Root.emit('apps.tabs.reorder');
          }
          break;
        }
        case 'app.move.widget':
        {
          const move = Applications.getById(msg.value.id);
          const from = msg.value.from;
          const to = msg.value.to;
          if (move && move._monitor.cmd && from !== to) {
            const tosave = [ move ];
            const widgets = Applications.getAll().reduce((acc, app) => ((app._monitor.cmd ? acc.push(app) : false), acc), []);
            if (from > to) {
              widgets.forEach(app => {
                if (app._position.widget >= to && app._position.widget <= from) {
                  app._position.widget++;
                  tosave.push(app);
                }
              });
            }
            else {
              widgets.forEach(app => {
                if (app._position.widget >= from && app._position.widget <= to) {
                  app._position.widget--;
                  tosave.push(app);
                }
              });
            }
            move._position.widget = to;
            tosave.forEach(app => app.save().then(_ => {}));
            Root.emit('apps.widgets.reorder');
          }
          break;
        }
        case 'system.captcha.token':
          Root.emit('system.captcha.token', { token: msg.value });
          break;
        default:
          break;
      }
    }
    catch (_) {
    }
  });

  ctx.websocket.on('close', () => {
    Root.off('app.status.update', updateStatus);
    Root.off('app.create', createApp);
    Root.off('app.remove', removeApp);
    Root.off('net.create', updateNetworks);
    Root.off('net.remove', updateNetworks);
    Root.off('system.stats', updateOperational);
    Root.off('system.captcha', openCaptcha);

    Root.off('cluster.app.status.update', updateStatus);
    Root.off('cluster.app.create', createApp);
    Root.off('cluster.app.remove', removeApp);
    Root.off('cluster.net.create', updateNetworks);
    Root.off('cluster.net.remove', updateNetworks);
    Root.off('cluster.apps.updated', updateHosts);
  });

  ctx.websocket.on('error', () => {
    ctx.websocket.close();
  });

  Root.on('app.status.update', updateStatus);
  Root.on('app.create', createApp);
  Root.on('app.remove', removeApp);
  Root.on('net.create', updateNetworks);
  Root.on('net.remove', updateNetworks);
  Root.on('system.stats', updateOperational);
  Root.on('system.captcha', openCaptcha);

  Root.on('cluster.app.status.update', updateStatus);
  Root.on('cluster.app.create', createApp);
  Root.on('cluster.app.remove', removeApp);
  Root.on('cluster.net.create', updateNetworks);
  Root.on('cluster.net.remove', updateNetworks);
  Root.on('cluster.apps.updated', updateHosts);
}

module.exports = {
  HTML: MainPageHTML,
  WS: MainPageWS
};

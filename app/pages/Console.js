const FS = require('fs');
const Handlebars = require('./HB');
const MinkeSetup = require('../app/MinkeSetup');
const Applications = require('../app/Applications');
const ClusterManager = require('../cluster/Manager');

const consoleTemplate = Handlebars.compile(FS.readFileSync(`${__dirname}/html/Console.html`, { encoding: 'utf8' }));
const consoleScreenTemplate = Handlebars.compile(FS.readFileSync(`${__dirname}/html/ConsoleScreen.html`, { encoding: 'utf8' }));

async function PageHTML(ctx) {
  if (ctx.query.s) {
    ctx.type = 'text/html';
    ctx.body = consoleScreenTemplate({
      DarkMode: MinkeSetup.getDarkMode()
    });
  }
  else {
    const tab = [];
    const app = Applications.getById(ctx.params.id);
    if (app == MinkeSetup.getSingleton()) {
      tab.push({ name: 'Local', ref: `/console/${app._id}/?s=1` });
      ClusterManager.getHosts().forEach(host => {
        tab.push({ name: host.name, ref: `http://${host.ip}/console/${app._id}/?s=1` });
      });
    }
    else {
      tab.push({ name: 'Main', ref: `/console/${app._id}/?c=m&s=1` });
      if (app._networks.primary.name !== 'host') {
        tab.push({ name: 'Helper', ref: `/console/${app._id}/?c=h&s=1` });
      }
      app._secondary.forEach((_, idx) => tab.push({ name: `#${idx}`, ref: `/console/${app._id}/?c=${idx}&s=1` }));
    }
    ctx.type = 'text/html';
    ctx.body = consoleTemplate({
      title: app._name,
      tab: tab,
      DarkMode: MinkeSetup.getDarkMode()
    });
  }
}

async function PageWS(ctx) {
  const app = Applications.getById(ctx.params.id);
  if (!app) {
    console.log(`Missing app ${ctx.params.id}`);
    return;
  }
  let container = null;
  switch (ctx.query.c || 'm') {
    case 'm':
      container = app._container;
      break;
    case 'h':
      container = app._helperContainer;
      break;
    default:
      if (app._secondaryContainers) {
        container = app._secondaryContainers[ctx.query.c];
      }
      break;
  }
  if (!container) {
    console.log(`Missing container ${ctx.query.c || 'm'} for ${app._name}`);
    return;
  }
  const exec = await container.exec({
    AttachStdin: true,
    AttachStdout: true,
    AttachStderr: true,
    Tty: true,
    Cmd: [ 'sh', '-c', 'test -x /bin/bash && exec /bin/bash; exec sh' ]
  });
  const stream = await exec.start({
    stdin: true
  });

  ctx.websocket.on('message', msg => {
    try {
      msg = JSON.parse(msg);
      switch (msg.type) {
        case 'console.from':
          stream.write(msg.value);
          break;
        default:
          break;
      }
    }
    catch (e) {
      console.log(e);
    }
  });
  ctx.websocket.on('error', () => {
    ctx.websocket.close();
    stream.destroy();
  });
  ctx.websocket.on('close', () => {
    stream.destroy();
  });

  function write(data) {
    try {
      ctx.websocket.send(JSON.stringify({ type: 'console.to', data: data.toString('utf8') }));
    }
    catch (_) {
      stream.destroy();
    }
  }
  app._docker.modem.demuxStream(stream, { write: write }, { write: write });

  stream.on('end', () => {
    try {
      ctx.websocket.send(JSON.stringify({ parent: true, type: 'window.close' }));
    }
    catch (_) {
    }
    ctx.websocket.close();
  });
}

module.exports = {
  HTML: PageHTML,
  WS: PageWS
};

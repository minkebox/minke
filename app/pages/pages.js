const FS = require('fs');
const Path = require('path');
const Config = require('../Config');

const CACHE_MAXAGE = 24 * 60 * 60; // 24 hours
const debug = (Config.CONFIG_NAME !== 'Production');

const Pages = {
  '/':                          require('./Main'),
  '/new/application':           require('./Applications'),
  '/configure/:id':             require('./Configure'),
  '/minkebox.config':           require('../config/ConfigBackup'),
  '/console/:id':               require('./Console'),
  '/log/:id':                   require('./Log'),
  '/js/ace.js':                 { JS: `${__dirname}/../node_modules/ace-builds/${debug ? 'src' : 'src-min'}/ace.js` },
  '/js/theme-twilight.js':      { JS: `${__dirname}/../node_modules/ace-builds/${debug ? 'src' : 'src-min'}/theme-twilight.js` },
  '/js/chart.js':               { JS: `${__dirname}/../node_modules/chart.js/dist/${debug ? 'Chart.js' : 'Chart.min.js'}` },
  '/js/sortable.js':            { JS: `${__dirname}/../node_modules/sortablejs/Sortable.min.js` },
  '/js/xterm.js':               { JS: `${__dirname}/../node_modules/xterm/lib/xterm.js` },
  '/js/xterm.js.map':           { JS: `${__dirname}/../node_modules/xterm/lib/xterm.js.map` },
  '/js/xterm-addon-fit.js':     { JS: `${__dirname}/../node_modules/xterm-addon-fit/lib/xterm-addon-fit.js` },
  '/js/xterm-addon-fit.js.map': { JS: `${__dirname}/../node_modules/xterm-addon-fit/lib/xterm-addon-fit.js.map` },
  '/js/script.js':              { JS: `${__dirname}/script/script.js` },
  '/js/qrcode.js':              { JS: `${__dirname}/script/qrcode.js` },
  '/css/pure.css':              { CSS: `${__dirname}/../node_modules/purecss/build/pure-min.css` },
  '/css/xterm.css':             { CSS: `${__dirname}/../node_modules/xterm/css/xterm.css` },
  '/css/apps.css':              { CSS: `${__dirname}/css/apps.css` },
  '/css/colors-dark.css':       { CSS: `${__dirname}/css/colors-dark.css` },
  '/css/colors-light.css':      { CSS: `${__dirname}/css/colors-light.css` },
  '/css/configure.css':         { CSS: `${__dirname}/css/configure.css` },
  '/css/console.css':           { CSS: `${__dirname}/css/console.css` },
  '/css/main.css':              { CSS: `${__dirname}/css/main.css` },
  '/img/:img':                  { FN: async (ctx) => {
                                        const img = ctx.params.img;
                                        if (Path.dirname(img) === '.') {
                                          ctx.body = FS.readFileSync(`${__dirname}/img/${img}`);
                                        }
                                        ctx.type = 'image/png';
                                        if (!debug) {
                                          ctx.cacheControl = { maxAge: CACHE_MAXAGE };
                                        }
                                      }
                                }
};

function pages(root, wsroot) {

  for (let key in Pages) {
    const page = Pages[key];
    if (page.JS) {
      const body = FS.readFileSync(page.JS, { encoding: 'utf8' });
      root.get(key, async (ctx) => {
        ctx.body = body;
        ctx.type = 'text/javascript';
        if (!debug) {
          ctx.cacheControl = { maxAge: CACHE_MAXAGE };
        }
      });
    }
    if (page.CSS) {
      const body = FS.readFileSync(page.CSS, { encoding: 'utf8' });
      root.get(key, async (ctx) => {
        ctx.body = body;
        ctx.type = 'text/css';
        if (!debug) {
          ctx.cacheControl = { maxAge: CACHE_MAXAGE };
        }
      });
    }
    if (page.HTML) {
      root.get(key, page.HTML);
    }
    if (page.WS) {
      wsroot.get(Path.normalize(`${key}/ws`), page.WS);
    }
    if (page.FN) {
      root.get(key, page.FN);
    }
  }
}

module.exports = {
  register: pages
};

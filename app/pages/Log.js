const FS = require('fs');
const Handlebars = require('./HB');
const Applications = require('../app/Applications');
const Filesystem = require('../sys/Filesystem');
const MinkeSetup = require('../app/MinkeSetup');
const ClusterManager = require('../cluster/Manager');

const logTemplate = Handlebars.compile(FS.readFileSync(`${__dirname}/html/Log.html`, { encoding: 'utf8' }));
const logScreenTemplate = Handlebars.compile(FS.readFileSync(`${__dirname}/html/LogScreen.html`, { encoding: 'utf8' }));

async function PageHTML(ctx) {
  if (ctx.query.s) {
    ctx.type = 'text/html';
    ctx.body = logScreenTemplate({});
  }
  else {
    const tab = [];
    const app = Applications.getById(ctx.params.id);
    if (app == MinkeSetup.getSingleton()) {
      tab.push({ name: 'Local', ref: `/log/${app._id}/?s=1` });
      ClusterManager.getHosts().forEach(host => {
        tab.push({ name: host.name, ref: `http://${host.ip}/log/${app._id}/?s=1` });
      });
    }
    else {
      tab.push({ name: 'Main', ref: `/log/${app._id}/?c=m&s=1` });
      if (app._networks.primary.name !== 'host') {
        tab.push({ name: 'Helper', ref: `/log/${app._id}/?c=h&s=1` });
      }
      app._secondary.forEach((_, idx) => tab.push({ name: `#${idx}`, ref: `/log/${app._id}/?c=${idx}&s=1` }));
    }

    ctx.type = 'text/html';
    ctx.body = logTemplate({
      title: `Logs for ${app._name}`,
      tab: tab
    });
  }
}

async function PageWS(ctx) {
  const app = Applications.getById(ctx.params.id);
  if (!app) {
    console.log(`Missing app ${ctx.params.id}`);
    return;
  }

  let logs = { destroy: function() {} };

  ctx.websocket.on('error', () => {
    ctx.websocket.close();
    logs.destroy();
  });
  ctx.websocket.on('close', () => {
    logs.destroy();
  });

  function write(prefix, data) {
    try {
      ctx.websocket.send(JSON.stringify({ type: 'console.to', data: `${prefix}${data.toString('utf8')}` }));
    }
    catch (_) {
    }
  }

  if (app.isRunning()) {
    let container = null;
    switch (ctx.query.c || 'm') {
      case 'm':
        container = app._container;
        break;
      case 'h':
        container = app._helperContainer;
        break;
      default:
        if (app._secondaryContainers) {
          container = app._secondaryContainers[ctx.query.c];
        }
        break;
    }
    if (!container) {
      console.log(`Missing container ${ctx.query.c || 'm'} for ${app._name}`);
      return;
    }

    logs = await container.logs({
      follow: true,
      stdout: true,
      stderr: true
    });
    logs.on('close', () => {
      ctx.websocket.close();
    });
    app._docker.modem.demuxStream(logs, { write: data => write('\033[37m', data) }, { write: data => write('\033[33m', data) });
  }
  else {
    const fs = Filesystem.create(app);
    let ext = '';
    switch (ctx.query.c || 'm') {
      case 'm':
        break;
      case 'h':
        ext = '_helper';
        break;
      default:
        ext = `_${ctx.query.c}`;
        break;
    }
    const logs = fs.getLogs(ext);
    write('\033[37m', '[STDOUT]\n');
    write('\033[37m', logs.stdout);
    write('\033[33m', '\n[STDERR]\n');
    write('\033[33m', logs.stderr);
    write('\033[31m', '\n[TERMINATED]\n');
  }
}

module.exports = {
  HTML: PageHTML,
  WS: PageWS
};

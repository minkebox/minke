const FS = require('fs');
const Path = require('path');
const UUID = require('uuid/v4');
const Handlebars = require('./HB');
const MinkeSetup = require('../app/MinkeSetup');
const Applications = require('../app/Applications');
const Skeletons = require('../images/Skeletons');
const Network = require('../sys/Network');
const Disks = require('../sys/Disks');
const Human = require('../sys/Human');
const ConfigBackup = require('../config/ConfigBackup');
const UPNP = require('../sys/UPNP');
const Filesystem = require('../sys/Filesystem');
const Database = require('../config/Database');
const ClusterManager = require('../cluster/Manager');

const MinkeBoxConfiguration = 'minke'; // MinkeSetup._id
const SKELETON_ERROR = {
  type: 'error',
  skeleton: {
    name: 'Missing Skeleton',
    description: 'Missing Skeleton',
    actions: [],
    properties: []
  }
};

let template;
let downloadTemplate;
let websitesTemplate;
function registerTemplates() {
  const partials = [
    'EditTable',
    'ShowTable',
    'SelectDirectory',
    'SelectShares',
    'SelectWebsites',
    'Disks',
    'BackupAndRestore',
    'DownloadFile',
    'SelectBackups'
  ];
  partials.forEach((partial) => {
    Handlebars.registerPartial(partial, Handlebars.compile(
      FS.readFileSync(`${__dirname}/html/partials/${partial}.html`, { encoding: 'utf8' }), { preventIndent: true }));
  });
  template = Handlebars.compile(FS.readFileSync(`${__dirname}/html/Configure.html`, { encoding: 'utf8' }), { preventIndent: true });
  downloadTemplate = Handlebars.compile('{{> DownloadFile}}', { preventIndent: true });
  websitesTemplate = Handlebars.compile('{{> SelectWebsites}}', { preventIndent: true });
}
if (!DEBUG) {
  registerTemplates();
}

async function uninstallApp(app) {
  Applications.remove(app);
  // Can't delete during startup - so wait for that to be done
  while (app.isStarting()) {
    await new Promise(resolve => setTimeout(resolve, 1000));
  }
  if (app.isRunning()) {
    await app.stop();
  }

  app._fs.uninstall();

  await Database.removeApp(app._id);

  Root.emit('app.remove', { app: app });
  if (app._willCreateNetwork()) {
    Root.emit('net.remove', { network: { _id: app._id, name: app._name } });
  }
}

async function ConfigurePageHTML(ctx) {

  if (DEBUG) {
    registerTemplates();
  }

  const app = Applications.getById(ctx.params.id);
  if (!app) {
    throw new Error(`Missing app: ${ctx.params.id}`);
  }
  const skel = (await Skeletons.loadSkeleton(app.skeletonId(), true)) || SKELETON_ERROR;
  if (skel.type === 'error') {
    console.error(`Failed to load skeleton: ${app._image}`);
  }
  const skeleton = skel.skeleton;
  const fs = app._fs;

  async function expandString(text) {
    return await app.eval.expandString(text);
  }

  let nextid = 100;
  const firstUse = (app._bootcount === 0);
  const visibles = {};
  const enabled = {};
  const properties = {
    Advanced: MinkeSetup.getAdvancedMode(),
    FirstUse: firstUse,
    WifiAvailable: app._mainMinkeApp && SYSTEM ? (await Network.wifiAvailable()) : false,
    UPnPAvailable: UPNP.available(),
    NoSystemControl: !SYSTEM
  };
  const vars = app.eval.allVariables();
  let help = false;
  const navbuttons = [];
  const nskeleton = {
    id: app._id,
    name: skeleton.name,
    value: app._name,
    description: await expandString(skeleton.description),
    actions: await Promise.all(skeleton.actions.map(async action => {
      if ('visible' in action || 'enabled' in action) {
        const id = action.id || `x${++nextid}`;
        if ('visible' in action) {
          visibles[id] = action.visible;
          action = Object.assign({ id: id }, action);
        }
        if ('enabled' in action) {
          enabled[id] = action.enabled;
          action = Object.assign({ id: id }, action);
        }
      }
      switch (action.type) {
        case 'Text':
          {
            return Object.assign({}, action, { text: await expandString(action.text) });
          }
        case 'Help':
          {
            help = true;
            return Object.assign({}, action, { text: await expandString(action.text) });
          }
        case 'NavButton':
          {
            navbuttons.push({
              name: action.name,
              link: await expandString(action.url),
              linktarget: '_blank'
            });
            return action;
          }
        case 'EditEnvironment':
          {
            let value = '';
            let placeholder = '';
            if (vars[action.name]) {
              value = vars[action.name].value;
              placeholder = await expandString(vars[action.name].defaultValue);
            }
            properties[`${action.type}#${action.name}`] = value;
            return Object.assign({ action: `window.action('${action.type}#${action.name}',this.value)`, value: value, placeholder: placeholder, options: action.options }, action, { description: await expandString(action.description) });
          }
        case 'EditEnvironmentAsCheckbox':
          {
            let value = false;
            if (vars[action.name]) {
              value = vars[action.name].value;
            }
            properties[`${action.type}#${action.name}`] = value;
            return Object.assign({ action: `window.action('${action.type}#${action.name}',this.checked)`, value: value }, action, { description: await expandString(action.description) });
          }
        case 'EditEnvironmentAsTable':
          {
            let value = [];
            if (vars[action.name] && vars[action.name].value) {
              value = vars[action.name].value;
              // Fix up the data so we have at least enough for to match the headers
              // (otherwise it looks bad when we display it)
              const hlen = action.headers.length;
              value.forEach(v => {
                while (v.length < hlen) {
                  v.push('');
                }
              });
            }
            return Object.assign({ action: `${action.type}#${action.name}`, value: value, controls: true }, action, { description: await expandString(action.description) });
          }
        case 'SelectWebsites':
          {
            let currentSites = [];
            if (vars[action.name] && vars[action.name].value) {
              currentSites = vars[action.name].value;
            }

            // Select websites on the secondary network if we're using it, otherwise use the primary
            // Generally we're sending traffic from the secondary to the primary network (hence secondary is checked first).
            // We track temporary network name changes because they effect what websites we can offer
            this._tempnetworks = {
              primary: app._networks.primary.name,
              secondary: app._networks.secondary.name
            };
            const networkName = this._tempnetworks.secondary !== 'none' ? this._tempnetworks.secondary : this._tempnetworks.primary;
            const websites = (await Applications.getWebsites(networkName, true)).map(site => {
              const match = currentSites.find(cs => cs[0] === site.app._id);
              let ip = networkName === site.app._networks.primary.name ? site.app._defaultIP : site.app._secondaryIP;
              return {
                appid: site.app._id,
                name: site.app._name,
                hostname: site.app._safeName(),
                ip: ip,
                ip6: site.app.getSLAACAddress(),
                port: site.port.port,
                dns: match ? match[3] : '',
                published: match ? !!match[4] : false
              };
            });
            return Object.assign({ action: `${action.type}#${action.name}`, websites: websites }, action, { description: await expandString(action.description) });
          }
        case 'SelectNetwork':
          {
            let networks;
            let network;
            if (action.name === 'primary') {
              network = app._networks.primary.name || 'home';
              networks = Applications.getNetworks(false);
            }
            else {
              network = app._networks.secondary.name || 'none';
              networks = [{ _id: 'none', name: 'none' }].concat(Applications.getNetworks(false));
            }
            if (app._networks[action.name].canCreate && !networks.find(network => network._id === app._id)) {
              networks.push({ _id: app._id, name: 'Create network' });
            }
            properties[`${action.type}#${action.name}`] = network;
            // If we chance the networks then the websites we can select will also change.
            let reload = '';
            if (skeleton.actions.find(action => action.type === 'SelectWebsites')) {
              reload = `;window.cmd('app.update-websites',['${action.name}',this.value])`;
            }
            return Object.assign({
              action: `window.action('${action.type}#${action.name}',this.value)` + reload,
              networks: networks,
              value: network
            }, action, { description: await expandString(action.description) });
          }
        case 'ShowFileAsTable':
          {
            app.eval.setVariable(action.name, await fs.readFromFile(action.name));
            const data = vars[action.name].value;
            return Object.assign({ value: data }, action, { description: await expandString(action.description) });
          }
        case 'ShowFile':
          {
            app.eval.setVariable(action.name, await fs.readFromFile(action.name));
            const data = vars[action.name].value;
            return Object.assign({ value: data }, action, { description: await expandString(action.description) });
          }
        case 'DownloadFile':
        case 'EditFile':
          {
            const fdata = await fs.readFromFile(action.name);
            if (fdata) {
              app.eval.setVariable(action.name, fdata);
            }
            const data = vars[action.name].value;
            return Object.assign({ action: `window.action('${action.type}#${action.name}',this.value)`, value: data, filename: Path.basename(action.name) }, action, { description: await expandString(action.description) });
          }
        case 'EditFileAsTable':
          {
            let value = [];
            if (vars[action.name] && vars[action.name].value) {
              value = vars[action.name].value;
              // Fix up the data so we have at least enough for to match the headers
              // (otherwise it looks bad when we display it)
              const hlen = action.headers.length;
              value.forEach(v => {
                while (v.length < hlen) {
                  v.push('');
                }
              });
            }
            return Object.assign({ action: `${action.type}#${action.name}`, value: value, controls: true }, action, { description: await expandString(action.description) });
          }
        case 'SelectDirectory':
          {
            let selected = '';
            if (vars[action.name] && vars[action.name].value) {
              selected = vars[action.name].value;
            }

            const shareables = [];
            let found = false;

            // Generate a list of all shareables (making the currently selected one as necessary)
            const allShares = await Applications.getShareables(app);
            allShares.sort((a, b) => a.app._name < b.app._name ? -1 : a.app._name > b.app._name ? 1 : 0);

            for (let i = 0; i < allShares.length; i++) {
              const shareable = allShares[i];
              const shares = [];
              for (let j = 0; j < shareable.shares.length; j++) {
                const bind = shareable.shares[j];
                for (let k = 0; k < bind.shares.length; k++) {
                  const share = bind.shares[k];
                  const name = Path.normalize(`${shareable.app._name}/${await shareable.app.eval.expandString(bind.target)}/${share.name}/`).slice(0, -1).replace(/\//g, '.');
                  const src = Path.normalize(`${await shareable.app.eval.expandPath(bind.src)}/${share.name}/`).slice(0, -1);
                  const value = src == selected;
                  found |= value;
                  shares.push({
                    name: name,
                    src: src,
                    description: await expandString(share.description),
                    value: value
                  });
                }
              }
              if (shares.length) {
                shareables.push({
                  app: shareable.app,
                  shares: shares
                });
              }
            }

            // Add native shareables
            const nativedirs = Filesystem.getNativeDirectories();
            if (nativedirs.length) {
              shareables.push({
                app: {
                  _id: 'native',
                  _name: 'Native'
                },
                shares: nativedirs.map(dir => {
                  const value = dir.src == selected;
                  found |= value;
                  return {
                    name: Path.basename(dir.src),
                    src: dir.src,
                    description: dir.src,
                    value: value
                  };
                })
              });
            }

            // Local fallback
            shareables.unshift({
              app: app,
              shares: [{
                name: 'Local',
                src: Filesystem.getNativePath(app._id, 'store', `/vol/${action.name}`),
                description: 'Local',
                value: !found
              }]
            });

            return Object.assign({ action: `window.action('${action.type}#${action.name}',event.target.value)`, shareables: shareables }, action, { description: await expandString(action.description) });
          }
        case 'SelectShares':
          {
            let selected = [];
            if (vars[action.name] && vars[action.name].value) {
              selected = vars[action.name].value;
            }

            const shareables = [];

            // Generate a list of all shareables (making the currently selected ones)
            const allShares = await Applications.getShareables(app);
            allShares.sort((a, b) => a.app._name < b.app._name ? -1 : a.app._name > b.app._name ? 1 : 0);

            for (let i = 0; i < allShares.length; i++) {
              const shareable = allShares[i];
              const shares = [];
              for (let j = 0; j < shareable.shares.length; j++) {
                const bind = shareable.shares[j];
                for (let k = 0; k < bind.shares.length; k++) {
                  const share = bind.shares[k];
                  const name = Path.normalize(`${shareable.app._name}/${await shareable.app.eval.expandString(bind.target)}/${share.name}/`).slice(0, -1).replace(/\//g, '.');
                  const src = Path.normalize(`${await shareable.app.eval.expandPath(bind.src)}/${share.name}/`).slice(0, -1);
                  const isShared = selected.find(select => select.src === src);
                  shares.push({
                    name: name,
                    altname: !isShared || isShared.name == name ? null : isShared.name,
                    description: await expandString(share.description),
                    action: `window.share('${action.type}#${shareable.app._id}#${src}#${action.name}#${name}',this)`,
                    value: !!isShared
                  });
                }
              }
              if (shares.length) {
                shareables.push({
                  app: shareable.app,
                  shares: shares
                });
              }
            }

            // Add native shareables
            const nativedirs = Filesystem.getNativeDirectories();
            if (nativedirs.length) {
              shareables.push({
                app: {
                  _id: 'native',
                  _name: 'Native'
                },
                shares: nativedirs.map(dir => {
                  const name = Path.basename(dir.src);
                  const isShared = selected.find(select => select.src === dir.src);
                  return {
                    name: name,
                    altname: !isShared || isShared.name == name ? null : isShared.name,
                    description: dir.src,
                    action: `window.share('${action.type}#native#${dir.src}#${action.name}#${name}',this)`,
                    value: !!isShared
                  };
                })
              });
            }

            return Object.assign({ shareables: shareables }, action, { description: await expandString(action.description) })
          }
        case 'SelectBackups':
          {
            let selected = [];
            if (vars[action.name] && vars[action.name].value) {
              selected = vars[action.name].value;
            }

            const allBackups = Applications.getBackups();
            allBackups.sort((a, b) => a.app._name < b.app._name ? -1 : a.app._name > b.app._name ? 1 : 0);

            const backups = [];
            allBackups.forEach(backup => {
              if (backup.app._id === MinkeBoxConfiguration) {
                backups.unshift({
                  id: MinkeBoxConfiguration,
                  name: `${backup.app._name} (Configuration)`,
                  action: `window.backup('${action.type}#${MinkeBoxConfiguration}#${action.name}',this)`,
                  value: !!selected.find(select => select.appid === MinkeBoxConfiguration)
                });
              }
              else {
                backups.push({
                  id: backup.app._id,
                  name: backup.app._name,
                  action: `window.backup('${action.type}#${backup.app._id}#${action.name}',this)`,
                  value: !!selected.find(select => select.appid === backup.app._id)
                });
              }
            });

            return Object.assign({ backups: backups }, action, { description: await expandString(action.description) });
          }
        case '__Disks':
          {
            if (!app._mainMinkeApp) {
              return {};
            }
            const diskinfo = Object.values((await Disks.getAllDisks()).diskinfo);
            const havestore = diskinfo.find(info => info.root === '/mnt/store');
            return {
              type: '__Disks',
              disks: diskinfo.map(disk => {
                return {
                  name: disk.root === '/minke' ? 'boot' : disk.root === '/mnt/store' ? 'store' : disk.name,
                  size: (disk.size / (1000 * 1000 * 1000)).toFixed(2) + 'GB',
                  percentage: disk.size === 0 ? 0 : (disk.used / disk.size * 100).toFixed(1),
                  tenth: parseInt(disk.size === 0 ? 0 : (disk.used / disk.size * 10)),
                  status: disk.status,
                  format: SYSTEM && !havestore && disk.root !== '/minke'
                }
              })
            };
          }
        case '__Captcha':
          {
            if (!app._mainMinkeApp) {
              return {};
            }
            const valid = Human.isVerified();
            return {
              type: '__Captcha',
              label: valid ? 'Verified' : 'Unverified: Click to verify',
              enabled: !valid
            };
          }
        case 'Header':
        case 'Script':
        case 'Argument':
        case 'SetEnvironment':
        default:
          return action;
      }
    }))
  }
  const advanced = MinkeSetup.getAdvancedMode();
  ctx.body = template({
    minkeConfig: app._mainMinkeApp,
    Advanced: advanced,
    DarkMode: MinkeSetup.getDarkMode(),
    skeleton: nskeleton,
    skeletonType: !app._mainMinkeApp && skel.type === 'local' ? 'Personal' : null,
    properties: JSON.stringify(properties),
    skeletonAsText: Skeletons.toString(skeleton),
    navbuttons: navbuttons,
    firstUse: properties.FirstUse,
    NoSystemControl: properties.NoSystemControl,
    help: help,
    changes: '[' + Object.keys(visibles).map((key) => {
      return `function(){try{const c=document.getElementById("${key}").classList;if(${visibles[key]}){c.remove("invisible")}else{c.add("invisible")}}catch(_){}}`;
    }).concat(Object.keys(enabled).map((key) => {
      return `function(){try{const v=(${enabled[key]});document.querySelectorAll("#${key}.can-disable,#${key} .can-disable").forEach((e)=>{e.disabled=(v?'':'disabled');if(v){e.classList.remove("disabled")}else{e.classList.add("disabled")}})}catch(_){}}`
    })).join(',') + ']',
    cluster: ClusterManager.getHosts()
  });
  ctx.type = 'text/html'
}

async function ConfigurePageWS(ctx) {

  function send(msg) {
    try {
      ctx.websocket.send(JSON.stringify(msg));
    }
    catch (_) {
    }
  }

  let app = Applications.getById(ctx.params.id);

  const NOCHANGE = 0;
  const APPCHANGE = 1;
  const SKELCHANGE = 2;
  const SHARECHANGE = 4;
  const BACKUPCHANGE = 8;

  const patterns = [
    {
      p: /^Name$/, f: async (value, match) => {
        if (app._name != value) {
          app._name = value;
          return APPCHANGE;
        }
        return NOCHANGE;
      }
    },
    {
      p: /^(EditEnvironment|SelectDirectory|EditFile)#(.+)$/, f: async (value, match) => {
        const key = match[2];
        return app.eval.setVariable(key, value) ? APPCHANGE : NOCHANGE;
      }
    },
    {
      p: /^EditEnvironmentAsCheckbox#(.+)$/, f: async (value, match) => {
        const key = match[1];
        return app.eval.setVariable(key, !!value) ? APPCHANGE : NOCHANGE;
      }
    },
    {
      p: /^(EditEnvironmentAsTable|SelectWebsites|EditFileAsTable)#(.+)$/, f: async (value, match) => {
        const key = match[2];
        return app.eval.setVariable(key, value) ? APPCHANGE : NOCHANGE;
      }
    },
    {
      p: /^SelectShares#(.*)#(.*)#(.*)#(.*)$/, f: async (value, match) => {
        const sharesrc = match[2]; // Absolute path of directory we're sharing
        const target = match[3]; // Path of the parent directory we're sharing onto
        const name = value.target.replace(/\//, '.') || match[4]; // The directory name in the parent
        const tvar = app.eval.getVariable(target);
        if (tvar) {
          const current = [].concat(tvar.value);
          const idx = current.findIndex(curr => curr.src === sharesrc);
          if (value.shared && idx === -1) {
            current.push({
              src: sharesrc,
              name: name
            });
          }
          else if (value.shared && idx !== -1 && current[idx].name !== name) {
            current[idx].name = name;
          }
          else if (!value.shared && idx !== -1) {
            current.splice(idx, 1);
          }
          return app.eval.setVariable(target, current) ? SHARECHANGE : NOCHANGE;
        }

        return NOCHANGE;
      }
    },
    {
      p: /^SelectBackups#(.+)#(.*)/, f: async (value, match) => {
        const appid = match[1];
        const target = match[2];
        const svar = app.eval.getVariable(target);
        if (tvar) {
          const current = tvar.value;
          const idx = current.findIndex(curr => curr.appid === appid);
          if (value.backup && idx === -1) {
            current.push({
              appid: appid,
              target: target
            });
            return BACKUPCHANGE;
          }
          else if (!value.backup && idx !== -1) {
            current.splice(idx, 1);
            return BACKUPCHANGE;
          }
        }
        return BACKUPCHANGE
      }
    },
    {
      p: /^SelectNetwork#(.+)$/, f: async (value, match) => {
        const network = match[1];
        const ovalue = app._networks[network].name;
        if ((network in app._networks) && ovalue !== value) {
          app._networks[network].name = value;
          return APPCHANGE;
        }
        return NOCHANGE;
      }
    },
    {
      p: /^__EditSkeleton$/, f: async (value, match) => {
        const skel = Skeletons.parse(value);
        if (skel) {
          // Make sure skeleton has unique id
          if (!skel.uuid) {
            skel.uuid = UUID().toUpperCase();
          }
          else {
            // If skeleton exists, we can update in-place if it's already local. Otherwise
            // we assigned it a new id.
            const existing = Skeletons.loadSkeleton(skel.uuid, false);
            if (existing && existing.type !== 'local') {
              skel.uuid = UUID().toUpperCase();
            }
          }
          Skeletons.saveLocalSkeleton(skel);
          // Make sure app points to the correct skeleton
          app._skeletonId = skel.uuid;
          return SKELCHANGE;
        }
        return NOCHANGE;
      }
    }
  ];

  let changes = {};
  async function save(forceRestart) {
    try {
      let changed = 0;
      for (let property in changes) {
        for (let i = 0; i < patterns.length; i++) {
          const match = property.match(patterns[i].p);
          if (match) {
            changed |= await patterns[i].f(changes[property], match);
          }
        }
      }
      changes = {};

      if (changed || app._status === 'stopped' || forceRestart) {
        const uapp = app;
        if ((changed & SKELCHANGE) !== 0) {
          await uapp.updateFromSkeleton(Skeletons.loadSkeleton(uapp.skeletonId(), false).skeleton, uapp.toJSON());
          await ConfigBackup.save();
          app = null;
          send({
            type: 'page.reload'
          });
        }

        if (!forceRestart) {
          await uapp.restart();
        }
        else {
          // Force skeleton update on explicity restart
          const skel = Skeletons.loadSkeleton(uapp.skeletonId(), false);
          if (skel) {
            await uapp.updateFromSkeleton(skel.skeleton, uapp.toJSON());
          }
          await uapp.restart('restart');
        }
      }
    }
    catch (e) {
      console.log(e);
    }
  }

  ctx.websocket.on('message', async (msg) => {
    try {
      msg = JSON.parse(msg);
      switch (msg.type) {
        case 'action.change':
          changes[msg.property] = msg.value;
          break;
        case 'app.restart':
          if (app) {
            await save(true);
          }
          break;
        case 'app.save':
          if (app) {
            await save();
          }
          break;
        case 'app.reboot':
          if (msg.value === 'local') {
            if (app) {
              await app.restart('reboot');
            }
          }
          else if (msg.value === 'all') {
            ClusterManager.getHosts().forEach(host => {
              ClusterManager.getHost(host.name).reboot().catch(e => Log(e));
            });
            if (app) {
              await app.restart('reboot');
            }
          }
          else {
            ClusterManager.getHost(msg.value).reboot().catch(e => Log(e));
          }
          break;
        case 'app.halt':
          if (msg.value === 'local') {
            if (app) {
              await app.restart('halt');
            }
          }
          else if (msg.value === 'all') {
            ClusterManager.getHosts().forEach(host => {
              ClusterManager.getHost(host.name).halt().catch(e => Log(e));
            });
            if (app) {
              await app.restart('halt');
            }
          }
          else {
            ClusterManager.getHost(msg.value).halt().catch(e => Log(e));
          }
          break;
        case 'app.rerun':
          if (msg.value === 'local') {
            if (app) {
              await save(true);
            }
          }
          else if (msg.value === 'all') {
            ClusterManager.getHosts().forEach(host => {
              ClusterManager.getHost(host.name).restart().catch(e => Log(e));
            });
            if (app) {
              await save(true);
            }
          }
          else {
            ClusterManager.getHost(msg.value).restart().catch(e => Log(e));
          }
          break;
        case 'app.update':
          if (msg.value === 'local') {
            if (app) {
              app.updateAll();
            }
          }
          else if (msg.value === 'all') {
            ClusterManager.getHosts().forEach(host => {
              ClusterManager.getHost(host.name).update().catch(e => Log(e));
            });
            if (app) {
              app.updateAll();
            }
          }
          else {
            ClusterManager.getHost(msg.value).update().catch(e => Log(e));
          }
          break;
        case 'app.delete':
          changes = {};
          const tapp = app;
          app = null;
          await uninstallApp(tapp);
          await ConfigBackup.save();
          break;
        case 'app.format-disk':
          if (app._mainMinkeApp) {
            Disks.format(msg.value, () => {
              send({
                type: 'page.reload'
              });
            });
          }
          break;
        case 'app.restore-all':
          if (app._mainMinkeApp) {
            ConfigBackup.restore(msg.value);
          }
          break;
        case 'app.open-captcha':
          Root.emit('human.verify', { force: true });
          break;
        case 'app.update-download':
          {
            const path = msg.value;
            const value = await fs.readFromFile(path);
            send({
              type: 'html.replace',
              selector: `#${path.replace(/[./]/g, '\\$&')}`,
              html: downloadTemplate({
                name: path,
                value: value,
                filename: Path.basename(path)
              })
            });
            break;
          }
        case 'app.update-websites':
          {
            this._tempnetworks[msg.value[0]] = msg.value[1];
            const networkName = this._tempnetworks.secondary !== 'none' ? this._tempnetworks.secondary : this._tempnetworks.primary;
            const websites = (await Applications.getWebsites(networkName, true)).map(site => {
              let ip = networkName === site.app._networks.primary.name ? site.app._defaultIP : site.app._secondaryIP;
              return {
                appid: site.app._id,
                name: site.app._name,
                hostname: site.app._safeName(),
                ip: ip,
                ip6: site.app.getSLAACAddress(),
                port: site.port.port,
                dns: '',
                published: false
              };
            });
            send({
              type: 'html.update',
              selector: '.websites',
              html: websitesTemplate({
                action: '',
                websites: websites
              })
            });
            break;
          }
        default:
          break;
      }
    }
    catch (e) {
      console.error(e);
    }
  });

  ctx.websocket.on('close', () => {
    if (app) {
      save();
    }
  });

  ctx.websocket.on('error', () => {
    ctx.websocket.close();
  });

}

function tab(t) {
  let s = '';
  for (; t > 0; t--) {
    s += '  ';
  }
  return s;
}
function toText(o, t) {
  if (Array.isArray(o)) {
    let r = "[";
    for (let i = 0; i < o.length; i++) {
      r += `${i === 0 ? '' : ','}\n${tab(t + 1)}${toText(o[i], t + 1)}`;
    }
    r += `\n${tab(t)}]`;
    return r;
  }
  else switch (typeof o) {
    case 'string':
      return "`" + o + "`";
      break;
    case 'number':
    case 'boolean':
    case 'undefined':
      return o;
    case 'object':
      if (o === null) {
        return o;
      }
      let r = '{';
      const k = Object.keys(o);
      for (let i = 0; i < k.length; i++) {
        r += `${i === 0 ? '' : ','}\n${tab(t + 1)}${k[i]}: ${toText(o[k[i]], t + 1)}`;
      }
      r += `\n${tab(t)}}`;
      return r;
      break;
    default:
      break;
  }
  return '';
}

module.exports = {
  HTML: ConfigurePageHTML,
  WS: ConfigurePageWS
};


const ClusterManager = require('./Manager');
const MinkeRemote = require('./MinkeRemote');
const Applications = require('../app/Applications');
const MinkeApp = require('../app/MinkeApp');
const Database = require('../config/Database');
const Human = require('../sys/Human');
const Barrier = require('../utils/Barrier');
const Log = require('debug')('cluster:apps');

const ClusterApplications = {

  start() {
    this._clusterOnline = this._clusterOnline.bind(this);
    this._clusterAppsUpdate = this._clusterAppsUpdate.bind(this);
    this._removeClusterHostApps = this._removeClusterHostApps.bind(this);
    this._localAppUpdate = this._localAppUpdate.bind(this);
    this._masterMode = this._masterMode.bind(this);

    Root.on('app.status.update', this._localAppUpdate);
    Root.on('app.create', this._localAppUpdate);
    Root.on('app.remove', this._localAppUpdate);
    Root.on('cluster.host.online', this._clusterOnline);
    Root.on('cluster.apps.update', this._clusterAppsUpdate);
    Root.on('cluster.master.mode', this._masterMode);
  },

  _localAppUpdate(evt) {
    ClusterManager.interrupt('cluster.apps.update');
  },

  async _clusterOnline(evt) {
    Log('_clusterOnline:', evt);
    if (evt.online) {
      try {
        const setup = require('../app/MinkeSetup').getSingleton();
        const config = {};

        if (Human.isVerified()) {
          config.human = { id: Human.getId() };
        }

        config.dns = { ip: MinkeApp._network.network.ip_address };
        config.updatetime = { updatetime: setup.eval.expandVariable('UPDATETIME') };
        config.timezone = { timezone: setup.eval.expandVariable('TIMEZONE') };
        config.domain = { localdomain: setup.eval.expandVariable('LOCALDOMAIN') };

        await ClusterManager.getHost(evt.host).setConfig(config);
      }
      catch (e) {
        Log(e);
      }
      await this._updateClusterHostApps(evt.host);
    }
    else {
      await this._removeClusterHostApps(evt.host, false);
    }
  },

  _clusterAppsUpdate: Barrier(async function (evt) {
    await this._updateClusterHostApps(evt.host);
  }),

  async _updateClusterHostApps(host) {
    Log('_updateClusterHostApps:', host);
    const node = ClusterManager.getHost(host);
    if (!node) {
      return;
    }
    Applications.addHost(host);
    let lost = Applications.getForHost(host);
    let remote;
    try {
      remote = await node.getAllApplications();
    }
    catch (e) {
      // Failed to retrieve apps - do no harm
      lost = [];
      remote = [];
    }
    for (let i = 0; i < remote.length; i++) {
      const config = remote[i];
      const idx = lost.findIndex(a => a._id === config._id);
      if (idx === -1) {
        const rapp = new MinkeRemote(node);
        await rapp.createFromJson(config);
        Applications.add(rapp, host);
        if (rapp.isRemoteRunning()) {
          rapp.start();
        }
      }
      else {
        const rapp = lost[idx];
        rapp.updateFromJson(config);
        lost.splice(idx, 1);
        if (rapp.isRemoteRunning()) {
          rapp.start();
        }
        else {
          rapp.stop();
        }
      }
    }
    for (let i = 0; i < lost.length; i++) {
      const rapp = lost[i];
      Applications.remove(rapp);
      await rapp.remove();
      //await Database.removeApp(rapp._id);
    }
    Root.emit('cluster.apps.updated');
  },

  async _removeClusterHostApps(host, clean) {
    Log('_removeClusterHostApps:', host, clean);
    const apps = Applications.getForHost(host);
    for (let j = 0; j < apps.length; j++) {
      const app = apps[j];
      Applications.remove(app);
      await app.stop();
      if (clean) {
        await Database.removeApp(app._id);
      }
    }
    Applications.removeHostIfEmpty(host);
    Root.emit('cluster.apps.updated');
  },

  async _masterMode(evt) {
    Log('_masterMode', evt);
    if (evt.enable) {
      return;
    }
    const hosts = Applications.getHosts();
    for (let i = 0; i < hosts.length; i++) {
      if (hosts[i] !== 'local') {
        await this._removeClusterHostApps(hosts[i], true);
      }
    }
  }

};

module.exports = ClusterApplications;

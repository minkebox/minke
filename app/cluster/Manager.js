
const WebSocket = require('ws');
const Applications = require('../app/Applications');
const MinkeRemote = require('./MinkeRemote');
const Human = require('../sys/Human');
const DNS = require('../dns/DNS');
const Updater = require('../images/Updater');
const Log = require('debug')('cluster:manager');
let MinkeSetup;

const RECONNECT_TIMEOUT = 10 * 1000; // 10 seconds

const ClusterManager = {

  _hosts: {},
  _incomings: [],
  _enabled: false,

  register: function(_, wsroot) {
    MinkeSetup = require('../app/MinkeSetup');

    this._hostsUpdated = this._hostsUpdated.bind(this);

    wsroot.get('/cluster', this._incoming.bind(this));
    Root.on('cluster.hosts.update', this._hostsUpdated);
  },

  enableMaster: function(enable) {
    if (this._enabled != enable) {
      this._enabled = enable;
      if (this._enabled) {
        for (let name in this._hosts) {
          Log('creating connection:', name);
          this._hosts[name]._open();
        }
      }
      else {
        for (let name in this._hosts) {
          Log('destroying connection:', name);
          this._hosts[name]._close();
        }
      }
      Root.emit('cluster.master.mode', { enable: this._enabled });
    }
  },

  _hostsUpdated: function(hosts) {
    const lost = Object.assign({}, this._hosts);

    hosts.forEach(host => {
      if (host.name in lost) {
        delete lost[host.name];
      }
      else {
        Log('creating connection:', host);
        const client = this._createCommandClient(host);
        this._hosts[host.name] = client;
        // Generate methods
        for (let cmd in this.commands) {
          client[cmd] = async function(msg) {
            return await client._send(cmd, msg);
          }
        }
        if (this._enabled) {
          client._open();
        }
      }
    });

    // Hosts have been lost
    for (let name in lost) {
      if (this._enabled) {
        lost[name]._close();
      }
      delete this._hosts[name];
    }
  },

  getHosts: function() {
    return this._enabled ? Object.values(this._hosts).filter(conn => conn._isActive()).map(conn => conn._host) : [];
  },

  getHost: function(host) {
    return this._hosts[host];
  },

  interrupt: function(cmd, msg) {
    Log('interrupt:', cmd, msg);
    const payload = JSON.stringify({ id: 0, cmd: cmd, msg: msg });
    this._incomings.forEach(ctx => {
      try {
        ctx.websocket.send(payload);
      }
      catch (e) {
        Log(e);
      }
    });
  },

  _incoming: function(ctx) {
    Log('new incoming connection');
    this._incomings.push(ctx);
    ctx.websocket.on('message', async pkt => {
      Log('message', pkt);
      try {
        pkt = JSON.parse(pkt);
        try {
          Log('_incoming:', pkt);
          const fn = this.commands[pkt.cmd];
          if (fn) {
            ctx.websocket.send(JSON.stringify({ id: pkt.id, cmd: '__reply__', msg: await fn.call(this.commands, pkt.msg) }));
          }
        }
        catch (e) {
          ctx.websocket.send(JSON.stringify({ id: pkt.id, cmd: '__error__', msg: e.stack }));
        }
      }
      catch (e) {
        Log(e);
      }
    });
    ctx.websocket.on('close', () => {
      Log('close:');
      const idx = this._incomings.indexOf(ctx);
      if (idx !== -1) {
        this._incomings.splice(idx, 1);
      }
    });
    ctx.websocket.on('error', e => {
      Log('error:', e);
      ctx.websocket.close();
    });
  },

  _createCommandClient(host) {
    let ws = { readyState: 3 }; // Start closed
    let id = 1;
    let pending = {};
    let enabled = false;
    const connect = () => {
      if (!enabled) {
        return;
      }
      // If we somehow have an open socket, close it.
      try {
        if (ws.readyState === 0 || ws.readyState === 1) {
          ws.close();
        }
      }
      catch (_) {
      }
      // Open a new connection
      ws = new WebSocket(`ws://${host.ip}/cluster`);
      ws.once('open', () => {
        Log('online', host);
        Root.emit('cluster.host.online', { online: true, host: host.name });
      });
      const closeAndOpen = () => {
        ws.off('close', closeAndOpen); // Errors dont make close do this again
        for (let id in pending) {
          const p = pending[id];
          delete pending[id];
          p[1](new Error('closed'));
        }
        // If we error and the socket is open, close it
        try {
          if (ws.readyState === 0 || ws.readyState === 1) {
            ws.close();
          }
        }
        catch (_) {
        }
        if (!enabled) {
          Root.emit('cluster.host.online', { online: false, host: host.name });
        }
        else {
          setTimeout(() => {
            connect();
          }, RECONNECT_TIMEOUT);
        }
      };
      ws.once('error', closeAndOpen);
      ws.once('close', closeAndOpen);
      ws.on('message', data => {
        const pkt = JSON.parse(data);
        Log('reply:', data);
        const p = pending[pkt.id];
        if (p) {
          delete pending[pkt.id];
          switch (pkt.cmd) {
            case '__reply__':
              p[0](pkt.msg);
              break;
            case '__error__':
              p[1](new Error('remote error:\n' + pkt.msg));
              break;
            default:
              p[1](new Error('unknown pkt cmd'));
              break;
          }
        }
        else if (pkt.id === 0) {
          Root.emit(pkt.cmd, { host: host.name, msg: pkt.msg });
        }
        else {
          Log('unexpected pkt:', pkt);
        }
      });
    };
    return {
      _host: host,
      _open: () => {
        if (!enabled) {
          enabled = true;
          connect();
        }
      },
      _send: (cmd, msg) => {
        Log('send:', cmd, msg);
        if (!enabled || ws.readyState !== 1) {
          throw new Error('inactive');
        }
        const mid = id++;
        const p = new Promise((resolve, reject) => pending[mid] = [ resolve, reject ]);
        try {
          ws.send(JSON.stringify({ id: mid, cmd: cmd, msg: msg }));
        }
        catch (_) {
          this._close();
        }
        return p;
      },
      _isActive: () => enabled && ws.readyState === 1,
      _close: () => {
        try {
          enabled = false;
          ws.close();
        }
        catch (_) {
          // May already be closed.
          switch (ws.readyState) {
            case 0: // Connecting
            case 1: // Open
            case 2: // Closing
            case 3: // Closed
            default:
              break;
          }
        }
      }
    };
  },

  commands: {

    async getAllApplications() {
      const setup = MinkeSetup.getSingleton();
      return Promise.all(Applications.getLocal().filter(app => app !== setup).map(async app => await MinkeRemote.toRemoteJSON(app)));
    },

    async statusMonitorUpdate(msg) {
      return await Applications.getById(msg.id)._statusMonitor.update(msg.timeout);
    },

    async update() {
      MinkeSetup.getSingleton().updateAll();
    },

    async reboot() {
      MinkeSetup.getSingleton().restart('reboot');
    },

    async halt() {
      MinkeSetup.getSingleton().restart('halt');
    },

    async restart() {
      MinkeSetup.getSingleton().restart('restart');
    },

    async setConfig(msg) {
      const setup = MinkeSetup.getSingleton();
      if (msg.human) {
        Human.setVerificationId(msg.human.id);
      }
      if (msg.dns) {
        DNS.setClusterDNSAddress(msg.dns.ip);
      }
      if (msg.updatetime) {
        setup.eval.setVariable('UPDATETIME', msg.updatetime.updatetime);
        setup.setUpdateTime();
      }
      if (msg.timezone) {
        setup.eval.setVariable('TIMEZONE', msg.timezone.timezone);
        setup.setTimezone();
      }
      if (msg.domain) {
        setup.eval.setVariable('LOCALDOMAIN', msg.domain.localdomain);
      }
    }

  }

};

module.exports = ClusterManager;

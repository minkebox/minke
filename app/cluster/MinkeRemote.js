const Config = require('../Config');
const Database = require('../config/Database');
const DNS = require('../dns/DNS');

const GLOBALDOMAIN = Config.GLOBALDOMAIN;

let ClusterManager;

class MinkeRemote {

  constructor(node) {
    if (!ClusterManager) {
      ClusterManager = require('./Manager');
    }
    this._cluster = node._host;
    this.eval = {
      expandPort: async port => port
    }
  }

  async createFromJson(app) {
    this._id = app._id;
    this._globalId = app.globalId;
    const state = await this.getApp();
    this._type = 'remote';
    this._name = app.name;
    this._status = 'stopped';
    this._rstatus = app.status;
    this._tags = app.tags;
    this._skeletonId = app.skeletonId;
    this._defaultIP = app.ip;
    this._homeIP = app.hip;
    this._position = state ? state.position : { tab: 0, widget: 0 },
    this._networks = app.networks;
    this._widgetOpen = app.widgetOpen;
    this._tabOpen = app.tabOpen;
    this._monitor = app.monitor;
    if (this._monitor.cmd) {
      this._statusMonitor = {
        init: this._monitor.init.replace(/{{ID}}/g, this._id),
        update: async (timeout) => await ClusterManager.getHost(this._cluster.name).statusMonitorUpdate({ id: app._id, timeout: timeout })
      }
    }

    this._ports = app.ports || [];
    this._dnsPort = app.dnsPort;
    Root.emit('cluster.app.create', { app: this });
  }

  updateFromJson(app) {
    this._name = app.name;
    this._rstatus = app.status;
    this._tags = app.tags;
    this._defaultIP = app.ip;
    this._homeIP = app.hip;
    this._networks = app.networks;
    this._widgetOpen = app.widgetOpen;
    this._tabOpen = app.tabOpen;
    this._dnsPort = app.dnsPort;
    this._ports = app.ports || [];
    Root.emit('cluster.app.status.update', { status: this._status, app: this });
  }

  toJSON() {
    return null;
  }

  async start() {
    if (this._status === 'running') {
      return;
    }
    if (this._willCreateNetwork()) {
      Root.emit('cluster.net.create', { app: this });
    }
    if (this._dnsPort) {
      this._dns = DNS.addDNSServer(this, {
        port: this._dnsPort, dnsNetwork: false
      });
    }
    if (this._homeIP) {
      DNS.registerHost(this._safeName(), `${this._globalId}${GLOBALDOMAIN}`, this._homeIP, null);
    }
    else if (this._defaultIP) {
      DNS.registerHost(this._safeName(), null, this._defaultIP, null);
    }
    this._status = 'running';
    Root.emit('cluster.app.status.update', { status: this._status, app: this });
  }

  async stop() {
    if (this._status !== 'running') {
      return;
    }
    DNS.unregisterHost(this._safeName());
    if (this._willCreateNetwork()) {
      Root.emit('cluster.net.remove', { app: this });
    }
    if (this._dns) {
      DNS.removeDNSServer(this._dns);
      this._dns = null;
    }
    this._status = 'stopped';
    Root.emit('cluster.app.status.update', { status: this._status, app: this });
  }

  async remove() {
    await this.stop();
    Root.emit('cluster.app.remove', { app: this });
  }

  async save() {
    await Database.saveApp({
      _id: this._id,
      type: 'remote',
      position: this._position
    });
  }

  isRunning() {
    return this._status === 'running';
  }

  isRemoteRunning() {
    return this._rstatus === 'running';
  }

  async getApp() {
    return await Database.getApp(this._id);
  }

  skeletonId() {
    return this._skeletonId;
  }

  getWebLink(type) {
    switch (type) {
      case 'config':
      default:
        if (this._tabOpen || this._widgetOpen) {
          return {
            url: (this._tabOpen || this._widgetOpen).url,
            target: '_blank'
          };
        }
      case 'tab':
        if (this._tabOpen) {
          return {
            url: this._tabOpen.url,
            target: this._tabOpen.target
          };
        }
        break;
      case 'widget':
        if (this._widgetOpen) {
          return {
            url: this._widgetOpen.url,
            target: this._widgetOpen.target
          };
        }
        break;
    }
    return {};
  }

  getSLAACAddress() {
    return null;
  }

  _willCreateNetwork() {
    return (this._networks.primary.name === this._id || this._networks.secondary.name === this._id);
  }

  _safeName() {
    return this._name.replace(/[^a-zA-Z0-9]/g, '');
  }

}

MinkeRemote.toRemoteJSON = async function(app) {
  return {
    _id: app._id,
    globalId: app._globalId,
    name: app._name,
    status: app._status,
    tags: app._tags,
    skeletonId: app._skeletonId,
    ip: app._defaultIP,
    hip: app._homeIP,
    networks: app._networks,
    monitor: app._monitor,
    widgetOpen: app._widgetOpen && {
      url: app._widgetOpen.url,
      target: app._widgetOpen.target
    },
    tabOpen: app._tabOpen && {
      url: app._tabOpen.url,
      target: app._tabOpen.target
    },
    ports: await Promise.all(app._ports.map(async port => await app.eval.expandPort(port))),
    dnsPort: app._dns ? app._dns.port : null
  };
}

module.exports = MinkeRemote;
